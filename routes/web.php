<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'revalidate'], function(){
    Route::post('cms/home', ['uses'=>'loginController@_loginAdminConfirm','as'=>'_loginAdminConfirm']);
    /* ---------- ADMIN ---------- */
        /*Admin Dashboard - After Admin Login Successfully*/
            Route::get('cms/admin/dashboard/managedp', ['uses'=>'adminController@_adminDashboardManageDP','as'=>'_adminDashboardManageDP']);
            /* Manage School */
                Route::get('cms/admin/dashboard/manageschool', ['uses'=>'adminController@_adminDashboardManageSchool','as'=>'_adminDashboardManageSchool']);
                /* if table has no data */
                Route::get('cms/admin/dashboard/manageschool/add', ['uses'=>'adminController@_adminDashboardManageSchoolAdd','as'=>'_adminDashboardManageSchoolAdd']);
                    /* Dropping a School */
                    Route::post('_adminDropSchool', ['uses'=>'adminController@_adminDropSchool','as'=>'_adminDropSchool']);
                    /* Adding  a School Modal */
                    Route::post('_adminInsertSchool', ['uses'=>'adminController@_adminInsertSchool','as'=>'_adminInsertSchool']); 
                    /* Updating  a School Modal */
                    Route::post('_adminUpdateSchool', ['uses'=>'adminController@_adminUpdateSchool','as'=>'_adminUpdateSchool']);
                /* Archive page */
                Route::get('cms/admin/dashboard/manageschool/archive', ['uses'=>'adminController@_adminDashboardManageSchoolArchive','as'=>'_adminDashboardManageSchoolArchive']);        
                    /* Delete School */
                    Route::post('_adminDashboardManageSchoolDelete', ['uses'=>'adminController@_adminDashboardManageSchoolDelete','as'=>'_adminDashboardManageSchoolDelete']);
                    /* Restore School */
                    Route::post('_adminDashboardManageSchoolRestore', ['uses'=>'adminController@_adminDashboardManageSchoolRestore','as'=>'_adminDashboardManageSchoolRestore']);
            /* End of manage school */
            /* Manage Principal */
                Route::get('cms/admin/dashboard/manageprincipal', ['uses'=>'adminController@_adminDashboardManagePrincipal','as'=>'_adminDashboardManagePrincipal']);
                    /* Get all school  */
                    Route::post('_adminDashboardPrincipalGetSchool', ['uses'=>'adminController@_adminDashboardPrincipalGetSchool','as'=>'_adminDashboardPrincipalGetSchool']); 
                        /* Insert Principal Modal */
                        Route::post('_adminInsertPrincipal', ['uses'=>'adminController@_adminInsertPrincipal','as'=>'_adminInsertPrincipal']); 
                        /* Edit Principal Modal */
                        Route::post('_adminUpdatePrincipal', ['uses'=>'adminController@_adminUpdatePrincipal','as'=>'_adminUpdatePrincipal']);
                        /* Resetting a Principal */
                        Route::post('_adminResetPrincipal', ['uses'=>'adminController@_adminResetPrincipal','as'=>'_adminResetPrincipal']);
                        /* Dropping a Principal */
                        Route::post('_adminDropPrincipal', ['uses'=>'adminController@_adminDropPrincipal','as'=>'_adminDropPrincipal']); 
                    /* Archive page */
                    Route::get('cms/admin/dashboard/manageprincipal/archive', ['uses'=>'adminController@_adminDashboardManagePrincipalArchive','as'=>'_adminDashboardManagePrincipalArchive']);        
                        /* Delete Principal */
                        Route::post('_adminDashboardManagePrincipalDelete', ['uses'=>'adminController@_adminDashboardManagePrincipalDelete','as'=>'_adminDashboardManagePrincipalDelete']);
                        /* Restore Principal */
                        Route::post('_adminDashboardManagePrincipalRestore', ['uses'=>'adminController@_adminDashboardManagePrincipalRestore','as'=>'_adminDashboardManagePrincipalRestore']);
            /* End of manage principal */
            /* Manage Division Personnel */
                    /* Adding page */
                    Route::get('cms/admin/dashboard/managedp/add', ['uses'=>'adminController@_adminDashboardManageDPAdd','as'=>'_adminDashboardManageDPAdd']);
                        /* Inserting a Division Personnel */
                        Route::post('_adminInsertDivisionPersonnel', ['uses'=>'adminController@_adminInsertDivisionPersonnel','as'=>'_adminInsertDivisionPersonnel']); 
                        /* Resetting a Division Personnel */
                        Route::post('_adminResetDivisionPersonnel', ['uses'=>'adminController@_adminResetDivisionPersonnel','as'=>'_adminResetDivisionPersonnel']);
                        /* Dropping a Division Personnel */
                        Route::post('_adminDropDivisionPersonnel', ['uses'=>'adminController@_adminDropDivisionPersonnel','as'=>'_adminDropDivisionPersonnel']); 
                        /* Selecting * Office */
                        Route::post('_adminInsertOfficeAssignment', ['uses'=>'adminController@_adminInsertOfficeAssignment','as'=>'_adminInsertOfficeAssignment']);  
                        /* Selecting * Program */
                        Route::post('_adminGetPrograms', ['uses'=>'adminController@_adminGetPrograms','as'=>'_adminGetPrograms']);
                        /* Inserting a Program */
                        Route::post('_adminInsertProgram', ['uses'=>'adminController@_adminInsertProgram','as'=>'_adminInsertProgram']);   
                    /* Archive page */
                    Route::get('cms/admin/dashboard/managedp/archive', ['uses'=>'adminController@_adminDashboardManageDPArchive','as'=>'_adminDashboardManageDPArchive']);        
                        /* Delete Division Personnel */
                        Route::post('_adminDashboardManageDPDelete', ['uses'=>'adminController@_adminDashboardManageDPDelete','as'=>'_adminDashboardManageDPDelete']);
                        /* Restore Division Personnel */
                        Route::post('_adminDashboardManageDPRestore', ['uses'=>'adminController@_adminDashboardManageDPRestore','as'=>'_adminDashboardManageDPRestore']);
                    /* Updating page */
                    Route::get('cms/admin/dashboard/managedp/edit/{id}', ['uses'=>'adminController@_adminDashboardManageDPEdit','as'=>'_adminDashboardManageDPEdit']);
                        /* Updating a Division Personnel */
                        Route::post('_adminUpdateDivisionPersonnel', ['uses'=>'adminController@_adminUpdateDivisionPersonnel','as'=>'_adminUpdateDivisionPersonnel']); 
            /* End of manage division personnel */
            /* Miscellaneous */
                /* Office */
                    Route::get('cms/admin/dashboard/miscellaneous/office', ['uses'=>'adminController@_adminDashboardManageMiscOffice','as'=>'_adminDashboardManageMiscOffice']);
                        /* Adding an Office Modal */
                        Route::post('_adminInsertOffice', ['uses'=>'adminController@_adminInsertOffice','as'=>'_adminInsertOffice']); 
                        /* Updating  an Office Modal */
                        Route::post('_adminUpdateOffice', ['uses'=>'adminController@_adminUpdateOffice','as'=>'_adminUpdateOffice']);
                        /* Dropping an Office Modal */
                        Route::post('_adminDropOffice', ['uses'=>'adminController@_adminDropOffice','as'=>'_adminDropOffice']);
                    /* Archive page */
                    Route::get('cms/admin/dashboard/miscellaneous/office/archive', ['uses'=>'adminController@_adminDashboardManageOfficeArchive','as'=>'_adminDashboardManageOfficeArchive']);        
                        /* Delete an Office Modal */
                        Route::post('_adminDashboardManageOfficeDelete', ['uses'=>'adminController@_adminDashboardManageOfficeDelete','as'=>'_adminDashboardManageOfficeDelete']);
                        /* Restore an Office Modal */
                        Route::post('_adminDashboardManageOfficeRestore', ['uses'=>'adminController@_adminDashboardManageOfficeRestore','as'=>'_adminDashboardManageOfficeRestore']);
                    /* End of office */
                /* Program */
                    Route::get('cms/admin/dashboard/miscellaneous/program', ['uses'=>'adminController@_adminDashboardManageMiscProgram','as'=>'_adminDashboardManageMiscProgram']);
                        /* Adding an Office Modal */
                        Route::post('_adminInsertProgram', ['uses'=>'adminController@_adminInsertProgram','as'=>'_adminInsertProgram']); 
                        /* Updating  an Office Modal */
                        Route::post('_adminUpdateProgram', ['uses'=>'adminController@_adminUpdateProgram','as'=>'_adminUpdateProgram']);
                        /* Dropping an Office Modal */
                        Route::post('_adminDropProgram', ['uses'=>'adminController@_adminDropProgram','as'=>'_adminDropProgram']);
                    /* Archive page */
                    Route::get('cms/admin/dashboard/miscellaneous/program/archive', ['uses'=>'adminController@_adminDashboardManageProgramArchive','as'=>'_adminDashboardManageProgramArchive']);        
                        /* Delete an Office Modal */
                        Route::post('_adminDashboardManageProgramDelete', ['uses'=>'adminController@_adminDashboardManageProgramDelete','as'=>'_adminDashboardManageProgramDelete']);
                        /* Restore an Office Modal */
                        Route::post('_adminDashboardManageProgramRestore', ['uses'=>'adminController@_adminDashboardManageProgramRestore','as'=>'_adminDashboardManageProgramRestore']);
                /* End of program */
            /* End of miscellaneous */
    /* ---------- End of ADMIN ---------- */
    /* ---------- DPC ---------- */
        /*Division Personnel Dashboard - After Division Personnel Login Successfully*/
        Route::get('cms/dp/dashboard/managespc', ['uses'=>'divisionPersonnelController@_divisionPersonnelDashboardManageSPC','as'=>'_divisionPersonnelDashboardManageSPC']);
            /* Resetting a School Personnel */
            Route::post('_divisionPersonnelResetSPC', ['uses'=>'divisionPersonnelController@_divisionPersonnelResetSPC','as'=>'_divisionPersonnelResetSPC']);
            /* Dropping a School Personnel */
            Route::post('_divisionPersonnelDropSPC', ['uses'=>'divisionPersonnelController@_divisionPersonnelDropSPC','as'=>'_divisionPersonnelDropSPC']);
            /* Adding page */
             Route::get('cms/dp/dashboard/managespc/add', ['uses'=>'divisionPersonnelController@_divisionPersonnelDashboardManageSPCAdd','as'=>'_divisionPersonnelDashboardManageSPCAdd']);
                /* Ajax for Program */
                Route::post('_divisionPersonnelGetCompareProgramSPC', ['uses'=>'divisionPersonnelController@_divisionPersonnelGetCompareProgramSPC','as'=>'_divisionPersonnelGetCompareProgramSPC']);
                /* Inserting SPC */
                Route::post('_divisionPersonnelInsertSPC', ['uses'=>'divisionPersonnelController@_divisionPersonnelInsertSPC','as'=>'_divisionPersonnelInsertSPC']); 
            /* Updating page */
            Route::get('cms/dp/dashboard/managespc/edit/{id}', ['uses'=>'divisionPersonnelController@_divisionPersonnelDashboardManageSPCEdit','as'=>'_divisionPersonnelDashboardManageSPCEdit']);
                /* Updating a SPC */
                Route::post('_divisionPersonnelUpdateSPC', ['uses'=>'divisionPersonnelController@_divisionPersonnelUpdateSPC','as'=>'_divisionPersonnelUpdateSPC']); 
            /* Archive page */
            Route::get('cms/dp/dashboard/managespc/archive', ['uses'=>'divisionPersonnelController@_divisionPersonnelDashboardManageSPCArchive','as'=>'_divisionPersonnelDashboardManageSPCArchive']);        
                /* Delete School Personnel */
                Route::post('_divisionPersonnelDeleteSPC', ['uses'=>'divisionPersonnelController@_divisionPersonnelDeleteSPC','as'=>'_divisionPersonnelDeleteSPC']);
                /* Restore School Personnel */
                Route::post('_divisionPersonnelRestoreSPC', ['uses'=>'divisionPersonnelController@_divisionPersonnelRestoreSPC','as'=>'_divisionPersonnelRestoreSPC']);
            /* Specific Program Report ex:ICT*/
                Route::get('/cms/dp/dashboard/reports/{program}', ['uses'=>'divisionPersonnelController@_divisionPersonnelProgramReports','as'=>'_divisionPersonnelProgramReports']);
                /* Ajax */
                    /* Active Report */
                    Route::post('_divisionPersonnelGetActiveReport', ['uses'=>'divisionPersonnelController@_divisionPersonnelGetActiveReport','as'=>'_divisionPersonnelGetActiveReport']);
                        /* Hide or Show filter->Level */
                        Route::post('_divisionPersonnelShowHideReport', ['uses'=>'divisionPersonnelController@_divisionPersonnelShowHideReport','as'=>'_divisionPersonnelShowHideReport']);
                        /* Count Schools Compliance Total */
                        Route::post('_divisionPersonnelShowCountSchoolsCompliance', ['uses'=>'divisionPersonnelController@_divisionPersonnelShowCountSchoolsCompliance','as'=>'_divisionPersonnelShowCountSchoolsCompliance']);
                        /* Delete Active Report */
                        Route::post('_dpcDeleteActiveReport', ['uses'=>'divisionPersonnelController@_dpcDeleteActiveReport','as'=>'_dpcDeleteActiveReport']);
                    /* Complied Report */
                    Route::post('_divisionPersonnelGetCompliedReport', ['uses'=>'divisionPersonnelController@_divisionPersonnelGetCompliedReport','as'=>'_divisionPersonnelGetCompliedReport']);
            /* Adding Reports Page */
                Route::get('/cms/dp/dashboard/reports/{program}/compose', ['uses'=>'divisionPersonnelController@_divisionPersonnelProgramReportsCompose','as'=>'_divisionPersonnelProgramReportsCompose']);
                /* Create a Post */
                    Route::post('_divisionPersonnelCompose', ['uses'=>'divisionPersonnelController@_divisionPersonnelCompose','as'=>'_divisionPersonnelCompose']);
            /* Editing Reports Page */
                Route::get('/cms/dp/dashboard/reports/{program}/edit/{id}', ['uses'=>'divisionPersonnelController@_divisionPersonnelComposeEdit','as'=>'_divisionPersonnelComposeEdit']);
                /* Editing a Post */
                    Route::post('_divisionPersonnelEditReport', ['uses'=>'divisionPersonnelController@_divisionPersonnelEditReport','as'=>'_divisionPersonnelEditReport']);
    /* ---------- End of DPC ---------- */
    /* ---------- Principal ---------- */
        /*Principal - After Principal Login Successfully*/
        Route::get('cms/principal/home', ['uses'=>'principalController@_principalHome','as'=>'_principalHome']);
    /* ---------- End of Principal ---------- */
    /* ---------- SPC ---------- */
        /*School Personnel - After School Personnel Login Successfully*/
        Route::get('cms/sp/home', ['uses'=>'schoolPersonnelController@_schoolPersonnelHome','as'=>'_schoolPersonnelHome']);
            /* Get specific program page */
            Route::get('/cms/spc/dashboard/reports/{program}',['uses'=>'schoolPersonnelController@_schoolPersonnelGetSpecificProgram','as'=>'_schoolPersonnelGetSpecificProgram']);
            /* Ajax for getting active reports*/
            Route::post('_schoolPersonnelProgramGetFilteredReport', ['uses'=>'schoolPersonnelController@_schoolPersonnelProgramGetFilteredReport','as'=>'_schoolPersonnelProgramGetFilteredReport']);
            /* Ajax for downloading attachment */
            Route::post('_schoolPersonnelProgramDownloadAttachment', ['uses'=>'schoolPersonnelController@_schoolPersonnelProgramDownloadAttachment','as'=>'_schoolPersonnelProgramDownloadAttachment']);
    /* ---------- End of SPC ---------- */
});
/*Login*/
Route::get('cms/login', ['uses'=>'loginController@_login','as'=>'_login']);
/*Logout*/
Route::get('cms/logout/{alert}', ['uses'=>'logoutController@_logout','as'=>'_logout']);