@extends('layouts.divisionpersonnel')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- MODALS -->
<!-- Modal for add ? -->
<div class="modal-header" style="font-weight:bold;">
Add School Personnel
<!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="{{route('_divisionPersonnelInsertSPC')}}" method="post">
                    {!! csrf_field() !!}
                    <b>First Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">First Name</label>
                        <input type="text" name="new-DPC-FirstName" placeholder="First Name" class="form-username form-control" id="dpc-SPC-FirstName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Middle Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Middle Name</label>
                        <input type="text" name="new-DPC-MiddleName" placeholder="Middle Name" class="form-username form-control" id="dpc-SPC-MiddleName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Surname:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Surname</label>
                        <input type="text" name="new-DPC-Surname" placeholder="Surname" class="form-username form-control" id="dpc-SPC-Surname" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Email:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Email</label>
                        <input type="email" name="new-DPC-Email" placeholder="Email" class="form-username form-control" id="dpc-SPC-Email" pattern="[a-zA-Z0-9\s(),'@.]+" required>
                    </div>
                    <div id="dpcNoSchool" class="alert alert-warning" role="alert">
                        Admin haven't yet created a school.</p>
                    </div>
                    <b>School:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">School</label>
                        <select name="new-DPC-School" placeholder="School" class="form-username form-control" id="dpc-SPC-GetSchool">
                            @foreach($dpcschool as $data)
                                <option id="dpc-SPC-GetSchoolOption">{{$data['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- Program -->
                    <b>Program:</b>
                    <div class="container">
                        <div class="card">
                            <div class="card-body">
                                <div id="dpcNoProgram" class="alert alert-warning" role="alert">
                                    Admin haven't yet created a program / All programs were assigned to a School Personnel already.</p>
                                </div>
                                Active Program/s:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="form-username" name="new-DPC-Program1" placeholder="Program" class="form-username form-control" id="dpc-SPC-GetPrograms" size="7">
                                        <!--foreach($dpcprograms as $data)
                                            <option id="dpc-SPC-GetProgramsOption">{$data}</option>
                                        endforeach-->
                                    </select>
                                </div>
                                <div style="text-align:center;padding-bottom:15px;">
                                        <button type="button" class="btn btn-info" id="dpc-SPC-GetPrograms-Button1">Add</button>
                                </div>
                                Program/s to be added:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="new-DPC-Program" placeholder="Program" class="form-username form-control" id="dpc-SPC-GetPrograms-Transferred" size="7">
                                    </select>
                                </div>
                                <!-- Hidden input -->
                                <div class="form-group">
                                    <input name="new-DPC-Programs" type="hidden" id="dpc-SPC-GetPrograms-Hidden">
                                </div>
                                <!-- end of hidden input -->
                                <div style="text-align:center;">
                                        <button type="button" class="btn btn-danger" id="dpc-SPC-GetPrograms-Button2">Remove All</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Program -->
                    <b>Contact Number:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Contact Number</label>
                        <input type="text" onkeydown="return event.keyCode !== 69" name="new-DPC-ContactNumber" placeholder="Contact Number" class="form-username form-control" id="dpc-SPC-ContactNumber" pattern="[0-9\s()]+" required>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="dpc-SPC-ApplyA">Submit</button>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<!-- end of Modal add -->
<script>
function _getProgramCompared(){
var selected_school = $("#dpc-SPC-GetSchool").val();
    //alert(selected_school);
    $.ajax({
            type:"POST",
            url:"{{route('_divisionPersonnelGetCompareProgramSPC')}}",
            data:{_token:"{!! csrf_token() !!}",newDPCSchool:selected_school},
            success:function(data){
                $("#dpc-SPC-GetPrograms").html("");
                for (i=0; i < data.length; i++){
                    $("#dpc-SPC-GetPrograms").append('<option class="dpc-SPC-GetProgramsOption">'+data[i]+'</option>');
                }
                if(data["error"]==1){
                        $("#dpcNoProgram").show();
                        $("#dpc-SPC-GetPrograms").attr('disabled', 'disabled');
                        $("#dpc-SPC-GetPrograms-Transferred").attr('disabled', 'disabled');
                        $("#dpc-SPC-GetPrograms-Button1").attr('disabled', 'disabled');
                        $("#dpc-SPC-GetPrograms-Button2").attr('disabled', 'disabled');
                        $("#dpc-SPC-ApplyA").attr('disabled', 'disabled');
                    }
                else{
                    $("#dpcNoProgram").hide();
                    $("#dpc-SPC-GetPrograms").prop('disabled', false);
                    $("#dpc-SPC-GetPrograms-Transferred").prop('disabled', false);
                    $("#dpc-SPC-GetPrograms-Button1").prop('disabled', false);
                    $("#dpc-SPC-GetPrograms-Button2").prop('disabled', false);
                    $("#dpc-SPC-ApplyA").prop('disabled', false);
                }
            }
        });
}
$(function(){
    _getProgramCompared();
    $("#dpc-SPC-GetSchool").on("change",_getProgramCompared);
    if($("#dpc-SPC-GetSchoolOption").html()==""){
        $("#dpcNoSchool").show();
        $("#dpc-SPC-GetSchool").attr('disabled', 'disabled');
        $("#dpc-SPC-ApplyA").attr('disabled', 'disabled');
    }
    else{
        $("#dpcNoSchool").hide();
    }
});
/* ADD */
    $("#dpc-SPC-GetPrograms-Button1").click(function(){
        $("#dpc-SPC-GetPrograms > option:selected").each(function(){
            $(this).remove().appendTo("#dpc-SPC-GetPrograms-Transferred");
        });
        /* Separator */
        var selectedval = $("#dpc-SPC-GetPrograms-Hidden").val();
        selectedval += $("#dpc-SPC-GetPrograms-Transferred").val() + ", ";
        $("#dpc-SPC-GetPrograms-Hidden").val(selectedval);
        /* end of separator */
    });
    $("#dpc-SPC-GetPrograms-Button2").click(function(){
        $('#dpc-SPC-GetPrograms-Transferred > option').remove().appendTo("#dpc-SPC-GetPrograms");
        $('#dpc-SPC-GetPrograms-Hidden').val('');
    });
    /* Remove comma */
    $("#dpc-SPC-ApplyA").on("click", function(){
        var removedComma = $("#dpc-SPC-GetPrograms-Hidden").val();
        $('#dpc-SPC-GetPrograms-Hidden').val(removedComma.substring(0,removedComma.length - 2));
    });
    /* end of remove comma */
/* ADD */
</script>
@endsection