@extends('layouts.divisionpersonnel')
@section('content')
<div class="modal-header" style="font-weight:bold;font-size:20px;">
    Create a Post
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-dpcComposeFile" role="tabpanel" aria-labelledby="pills-dpcComposeFile-tab">
                <form role="form" action="{{route('_divisionPersonnelCompose')}}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <b>Title:</b>
                <div class="form-group">
                    <label class="sr-only" for="form-username">Title</label>
                    <input type="text" name="new-DPCPost-Title" placeholder="Title" class="form-username form-control" id="dpc-Post-Title" pattern="[a-zA-Z0-9\s(),']+" required>
                </div>
                <b>Description:</b>
                <div class="form-group">
                    <label class="sr-only" for="form-username">Description</label>
                    <textarea name="new-DPCPost-Description" placeholder="Description" class="form-username form-control" id="dpc-Post-Description" pattern="[a-zA-Z0-9\s(),'.?]+" required></textarea>
                </div>
                <b>Attachment:</b>
                <div class="container row div-fileAttachment1">
                    <input name="new-fileAttachment1" id="fileAttachment1" type="file"  accept=".doc, .docm, .docx, .dot, .dotm, .dotx, .htm, .html, .odt, .pdf, .rtf, .txt, .wps, .xml, .xps, .csv, .dbf, .dif, .mht, .mhtml, .ods, .prn, .slk, .xla, .xlam, .xls, .xlsb, .xlsm, .xlsx, .xlt, .xltm, .xltx, .xlw, .odp, .pot, .potm, .potx, .ppa, .ppam, .pps, .ppsm, .ppsx, .ppt, .pptx, .rtf"/>
                </div>
                <div style="padding-top:15px;">
                    <b>Deadline:</b>
                </div>
                <div style="padding-bottom:5px;">
                    <input type="date" name="new-dpcDate" id="dpc-dpcDate" required>
                </div>
                <hr>
                <b>Mode of Reply:</b>
                <div>
                    <input type="radio" name="dpcRadio" value="File" required>File
                </div>
                <div>
                    <input type="radio" name="dpcRadio" value="Text">Text
                </div>
                <div>
                    <input type="radio" name="dpcRadio" value="Url">Url
                </div> 
                <div class="dpcFileSelect" style="padding-top:10px;">
                <b>File Quantity:</b><br>
                    <select id="dpcFilesToSelect">
                        <option value=""></option>
                    </select>
                </div>
                <!--
                <div class="container">
                    <div class="row div-fileUpload1">
                        <input name="new-fileUpload1" id="fileUpload1" type="file"  accept=".doc, .docm, .docx, .dot, .dotm, .dotx, .htm, .html, .odt, .pdf, .rtf, .txt, .wps, .xml, .xps, .csv, .dbf, .dif, .mht, .mhtml, .ods, .prn, .slk, .xla, .xlam, .xls, .xlsb, .xlsm, .xlsx, .xlt, .xltm, .xltx, .xlw, .odp, .pot, .potm, .potx, .ppa, .ppam, .pps, .ppsm, .ppsx, .ppt, .pptx, .rtf" required />
                    </div>
                    <div class="row div-fileUpload2" style="margin-top:10px;">
                        <input name="new-fileUpload2" id="fileUpload2" type="file"  accept=".doc, .docm, .docx, .dot, .dotm, .dotx, .htm, .html, .odt, .pdf, .rtf, .txt, .wps, .xml, .xps, .csv, .dbf, .dif, .mht, .mhtml, .ods, .prn, .slk, .xla, .xlam, .xls, .xlsb, .xlsm, .xlsx, .xlt, .xltm, .xltx, .xlw, .odp, .pot, .potm, .potx, .ppa, .ppam, .pps, .ppsm, .ppsx, .ppt, .pptx, .rtf" />
                    </div>
                    <div class="row div-fileUpload3" style="margin-top:10px;">
                        <input name="new-fileUpload3" id="fileUpload3" type="file"  accept=".doc, .docm, .docx, .dot, .dotm, .dotx, .htm, .html, .odt, .pdf, .rtf, .txt, .wps, .xml, .xps, .csv, .dbf, .dif, .mht, .mhtml, .ods, .prn, .slk, .xla, .xlam, .xls, .xlsb, .xlsm, .xlsx, .xlt, .xltm, .xltx, .xlw, .odp, .pot, .potm, .potx, .ppa, .ppam, .pps, .ppsm, .ppsx, .ppt, .pptx, .rtf" />
                    </div>
                </div>
                -->
                <!-- Hidden Program -->
                <input type="hidden" name="new-fileUploadProgram" value="{{session('dcpprogramcompose')}}"/>
                <!-- Hidden DateTime -->
                <input type="hidden" name="new-fileUploadDateTime" value="{{date('YmdHis')}}"/>
                <!-- Hidden Deadline -->
                <input type="hidden" name="new-Deadline" id="dpc-dpcDateHidden"/>
                <!-- Hidden Mode of Reply -->
                <input type="hidden" name="new-fileUploadModeofReply" id="dpcModeofReply"/>
                <!-- Hidden File Quantity -->
                <input type="hidden" name="new-fileUploadQuantity" id="dpc-FileQuantity"/>
                <div>
                    <input type="submit" value="Compose" class="btn btn-info float-right"/>
                </div>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<script>
//Document ready hide all + for loop
$(function(){
    $(".dpcFileSelect").hide();
    /*$(".div-fileUpload1").hide();
    $(".div-fileUpload2").hide();
    $(".div-fileUpload3").hide();*/
    for(i = 1; i <= 3; i++) {
        $("#dpcFilesToSelect").append("<option value="+ i +">"+ i +"</option>");
    }
});
$("#dpc-dpcDate").on('change',function(){
    $("#dpc-dpcDateHidden").val($(this).val());
});
//Radio to Select.change
$("input[name=dpcRadio]").change(function(){
    if($("input[name=dpcRadio]:checked").val() == "File"){
        $(".dpcFileSelect").show();
        $("#dpcFilesToSelect").attr("required",true);
        $("#dpcModeofReply").val($("input[name=dpcRadio]:checked").val());
        $("#dpc-FileQuantity").val($("#dpcFilesToSelect").val());
        //Files to Select.change
        $("#dpcFilesToSelect").on('change',function(){
            $("#dpc-FileQuantity").val($("#dpcFilesToSelect").val());
        });
        /*$("#dpcFilesToSelect").change(function(){
            if($("#dpcFilesToSelect").val() == ""){ 
                $(".div-fileUpload1").hide();
                $(".div-fileUpload2").hide();
                $(".div-fileUpload3").hide();
            }
            else if($("#dpcFilesToSelect").val() == 1){
               $(".div-fileUpload1").show(); 
            }
            else if($("#dpcFilesToSelect").val() == 2){
               $(".div-fileUpload1").show();
               $(".div-fileUpload2").show();
            }
            else if($("#dpcFilesToSelect").val() == 3){
               $(".div-fileUpload1").show();
               $(".div-fileUpload2").show();
               $(".div-fileUpload3").show();
            }
        });*/
    }
    else{
        $(".dpcFileSelect").hide();
        $("#dpcFilesToSelect").attr("required",false);
        $("#dpcModeofReply").val($("input[name=dpcRadio]:checked").val());
        $("#dpc-FileQuantity").val('Empty');
        /*$(".div-fileUpload1").hide();
        $(".div-fileUpload2").hide();
        $(".div-fileUpload3").hide();*/
    }
});
</script>
@endsection