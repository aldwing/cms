@extends('layouts.divisionpersonnel')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- MODALS -->
<!-- Modal for edit ? -->
<div class="modal-header" style="font-weight:bold;">
    Edit School Personnel
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="{{route('_divisionPersonnelUpdateSPC')}}" method="post">
                    {!! csrf_field() !!}
                    @foreach($dpceditspc as $data)
                    <b>First Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">First Name</label>
                        <input type="text" name="new-DPC-FirstNameEdit" placeholder="First Name" value="{{$data['first_name']}}" class="form-username form-control" id="dpc-SPC-FirstName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Middle Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Middle Name</label>
                        <input type="text" name="new-DPC-MiddleNameEdit" placeholder="Middle Name" value="{{$data['middle_name']}}" class="form-username form-control" id="dpc-SPC-MiddleName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Surname:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Surname</label>
                        <input type="text" name="new-DPC-SurnameEdit" placeholder="Surname" value="{{$data['surname']}}" class="form-username form-control" id="dpc-SPC-Surname" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Email:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Email</label>
                        <input type="email" name="new-DPC-EmailEdit" placeholder="Email" value="{{$data['email']}}" class="form-username form-control" id="dpc-SPC-Email" pattern="[a-zA-Z0-9\s(),'@.]+" required>
                    </div>
                    <div id="dpcNoSchool" class="alert alert-warning" role="alert">
                        Admin haven't yet created a school.</p>
                    </div>
                    <b>School:</b> ({{$data['office']}})
                    <div class="form-group">
                        <label class="sr-only" for="form-username">School</label>
                        <select name="new-DPC-SchoolEdit" placeholder="School" class="form-username form-control" id="dpc-SPC-GetSchoolEdit">
                            @foreach($dpcnewschool as $data1)
                                <option id="dpc-SPC-GetSchoolOption">{{$data1['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- Program -->
                    <b>Program:</b> ({{$data['program']}})
                    <div class="container">
                        <div class="card">
                            <div class="card-body">
                                <div id="dpcNoProgram" class="alert alert-warning" role="alert">
                                Admin haven't yet created a program / All programs were assigned to a School Personnel already.</p>
                                </div>
                                Active Program/s:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="form-program" placeholder="Program" class="form-username form-control" id="dpc-SPC-GetPrograms" size="7">
                                        <!--foreach($dpcprograms as $data2)
                                            <option id="dpc-SPC-GetProgramsOption">{$data2}</option>
                                        endforeach -->
                                    </select>
                                </div>
                                <div style="text-align:center;padding-bottom:15px;">
                                        <button type="button" class="btn btn-info" id="dpc-SPC-GetPrograms-Button1">Add</button>
                                </div>
                                Program/s to be added:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="new-DPC-ProgramEdit" placeholder="Program" class="form-username form-control" id="dpc-SPC-GetPrograms-Transferred" size="7">
                                    </select>
                                </div>
                                <!-- Hidden input -->
                                <div class="form-group">
                                    <input name="new-DPC-ProgramsEdit" type="hidden" id="dpc-SPC-GetPrograms-Hidden">
                                </div>
                                <!-- end of hidden input -->
                                <div style="text-align:center;">
                                        <button type="button" class="btn btn-danger" id="dpc-SPC-GetPrograms-Button2">Remove All</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Program -->
                    <b>Contact Number:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Contact Number</label>
                        <input type="text" onkeydown="return event.keyCode !== 69" name="new-DPC-ContactNumberEdit" placeholder="Contact Number" value="{{$data['contact_number']}}" class="form-username form-control" id="dpc-SPC-ContactNumber" pattern="[0-9\s()]+" required>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="dpc-SPC-ApplyA">Submit</button>
                    @endforeach
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<!-- end of Modal edit -->
<script>
function _getProgramCompared(){
var selected_schooledit = $("#dpc-SPC-GetSchoolEdit").val();
    //alert(selected_school);
    $.ajax({
            type:"POST",
            url:"{{route('_divisionPersonnelGetCompareProgramSPC')}}",
            data:{_token:"{!! csrf_token() !!}",newDPCSchool:selected_schooledit},
            success:function(data){
                $("#dpc-SPC-GetPrograms").html("");
                for (i=0; i < data.length; i++){
                    $("#dpc-SPC-GetPrograms").append('<option class="dpc-SPC-GetProgramsOption">'+data[i]+'</option>');
                }
                if(data["error"]==1){
                        $("#dpcNoProgram").show();
                        $("#dpc-SPC-GetPrograms").attr('disabled', 'disabled');
                        $("#dpc-SPC-GetPrograms-Transferred").attr('disabled', 'disabled');
                        $("#dpc-SPC-GetPrograms-Button1").attr('disabled', 'disabled');
                        $("#dpc-SPC-GetPrograms-Button2").attr('disabled', 'disabled');
                        $("#dpc-SPC-ApplyA").attr('disabled', 'disabled');
                    }
                else{
                    $("#dpcNoProgram").hide();
                    $("#dpc-SPC-GetPrograms").prop('disabled', false);
                    $("#dpc-SPC-GetPrograms-Transferred").prop('disabled', false);
                    $("#dpc-SPC-GetPrograms-Button1").prop('disabled', false);
                    $("#dpc-SPC-GetPrograms-Button2").prop('disabled', false);
                    $("#dpc-SPC-ApplyA").prop('disabled', false);
                }
            }
        });
}
$(function(){
    _getProgramCompared();
    $("#dpc-SPC-GetSchoolEdit").on("change",_getProgramCompared);
    if($("#dpc-SPC-GetSchoolOption").html()==""){
        $("#dpcNoSchool").show();
        $("#dpc-SPC-GetSchool").attr('disabled', 'disabled');
        $("#dpc-SPC-ApplyA").attr('disabled', 'disabled');
    }
    else{
        $("#dpcNoSchool").hide();
    }
})
/* ADD */
    $("#dpc-SPC-GetPrograms-Button1").click(function(){
        $("#dpc-SPC-GetPrograms > option:selected").each(function(){
            $(this).remove().appendTo("#dpc-SPC-GetPrograms-Transferred");
        });
        /* Separator */
        var selectedval = $("#dpc-SPC-GetPrograms-Hidden").val();
        selectedval += $("#dpc-SPC-GetPrograms-Transferred").val() + ", ";
        $("#dpc-SPC-GetPrograms-Hidden").val(selectedval);
        /* end of separator */
    });
    $("#dpc-SPC-GetPrograms-Button2").click(function(){
        $('#dpc-SPC-GetPrograms-Transferred > option').remove().appendTo("#dpc-SPC-GetPrograms");
        $('#dpc-SPC-GetPrograms-Hidden').val('');
    });
    /* Remove comma */
    $("#dpc-SPC-ApplyA").on("click", function(){
        var removedComma = $("#dpc-SPC-GetPrograms-Hidden").val();
        $('#dpc-SPC-GetPrograms-Hidden').val(removedComma.substring(0,removedComma.length - 2));
    });
    /* end of remove comma */
/* ADD */
</script>
@endsection