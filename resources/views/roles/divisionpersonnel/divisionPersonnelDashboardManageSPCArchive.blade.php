@extends('layouts.divisionpersonnel')
@section('title')
CMS | DPC
@endsection
@section('content')
<style>
.errorAdminManageDPArchive{ 
    border: 3px solid rgba(255, 0, 0, .6);
}
.successAdminManageDPArchive{
    border: 3px solid rgba(128, 189, 255, .6);
}
</style>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Office</th>
            <th>Role</th>
            <th>Program</th>
            <th>Contact Number</th>
            <th>Restore</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getspcarchive as $data)
        <tr>
            <td>{{$data['Name']}}</td>
            <td>{{$data['email']}}</td>
            <td>{{$data['office']}}</td>
            <td>{{$data['role']}}</td>
            <td>{{$data['program']}}</td>
            <td>{{$data['contact_number']}}</td>
            <!-- Button trigger modal for restore and delete-->
            <td><a href="#" class="dpc-SPC-ArchiveRestore" data-dpcarchiverestorespcid="{{$data['id']}}" data-toggle="modal" data-target="#dpc-SPC-ArchiveRestore"><i class="fas fa-undo" style="color:blue;"></i></a></td>
            <td><a href="#" class="dpc-SPC-ArchiveDelete" data-dpcarchivedeletespcemail="{{$data['email']}}" data-toggle="modal" data-target="#dpc-SPC-ArchiveDelete"><i class="fas fa-trash-alt" style="color:red;"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Restore -->
<div class="modal fade" id="dpc-SPC-ArchiveRestore" tabindex="-1" role="dialog" aria-labelledby="dpc-SPC-ArchiveRestore" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_divisionPersonnelRestoreSPC')}}" method="post">
            {!!csrf_field()!!}
                The School Personnel will be restored.
                <input type="hidden" name="new-DPC-ArchiveRestoreID" class="form-username form-control" id="dpc-SPC-ArchiveRestoreID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Restore</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" id="dpc-SPC-ArchiveDelete" tabindex="-1" role="dialog" aria-labelledby="dpc-SPC-ArchiveDelete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_divisionPersonnelDeleteSPC')}}" method="post" class="submitDPCArchive">
                {!! csrf_field() !!}
                Deleting an archived personnel cannot be recovered. <!--Click <b style="color:red;">Delete</b> <b style="color:red;" id="admin-DP-ArchiveBold" name="admin-DP-ArchiveEmailName"></b> if you want to delete this archived user.-->
                <input type="hidden" name="new-DPC-ArchiveEmail" class="form-username form-control" id="dpc-SPC-ArchiveEmail" required>
                <input type="hidden" name="admin-DPC-ArchiveHiddenEmail" id="dpc-SPC-ArchiveHidden" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary dpc-SPC-ArchiveSubmit" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.dpc-SPC-ArchiveDelete', function () {
    /* Bold
    $("#admin-DP-ArchiveBold").html($(this).data("managedparchivesessionemail"));
    */
    /* input exag */
    $("#dpc-SPC-ArchiveEmail").val($(this).data("dpcarchivedeletespcemail"));
    $("#dpc-SPC-ArchiveHidden").val($(this).data("dpcarchivedeletespcemail"));
})
$(".adminDashboardManageDP tbody").on('click', '.dpc-SPC-ArchiveRestore', function () {
    $("#dpc-SPC-ArchiveRestoreID").val($(this).data("dpcarchiverestorespcid"));
})
$(document).ready(function(){ 
    $(".submitDPCArchive").submit(function(event){ 
        if ( $('#dpc-SPC-ArchiveHidden').val() !== $("#dpc-SPC-ArchiveEmail").val() ) {
            $('#dpc-SPC-ArchiveEmail').addClass("errorAdminManageDPArchive");
            event.preventDefault(); 
        }
        else{
            $('#dpc-SPC-ArchiveEmail').addClass("successAdminManageDPArchive");
        }
    }); 
});
</script>
@endsection