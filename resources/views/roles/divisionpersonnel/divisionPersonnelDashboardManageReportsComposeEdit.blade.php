@extends('layouts.divisionpersonnel')
@section('content')
<div class="modal-header" style="font-weight:bold;font-size:20px;">
    Edit a Post
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-dpcComposeFile" role="tabpanel" aria-labelledby="pills-dpcComposeFile-tab">
                <form role="form" action="{{route('_divisionPersonnelEditReport')}}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <b>Title:</b>
                <div class="form-group">
                    <label class="sr-only" for="form-username">Title</label>
                <input type="text" name="new-DPCPost-TitleEdit" value="{{$dpcreport['title']}}" placeholder="Title" class="form-username form-control" id="dpc-Post-Title" pattern="[a-zA-Z0-9\s(),']+" required>
                </div>
                <b>Description:</b>
                <div class="form-group">
                    <label class="sr-only" for="form-username">Description</label>
                <textarea name="new-DPCPost-DescriptionEdit" placeholder="Description" class="form-username form-control" id="dpc-Post-Description" pattern="[a-zA-Z0-9\s(),'.?]+" required>{{$dpcreport['description']}}</textarea>
                </div>
                <div style="padding-top:15px;">
                    <b>Deadline:</b>
                </div>
                <div style="padding-bottom:5px;">
                    <input type="date" name="new-dpcDate" value="{{$dpcreport['date_deadline']}}" id="dpc-dpcDate" required>
                </div>
                <hr>
                <b>Mode of Reply:</b> ({{$dpcreport['modeofreply']}} {{$dpcreport['filequantity']}})
                <div>
                    <input type="radio" name="dpcRadio" value="File" required>File
                </div>
                <div>
                    <input type="radio" name="dpcRadio" value="Text">Text
                </div>
                <div>
                    <input type="radio" name="dpcRadio" value="Url">Url
                </div> 
                <div class="dpcFileSelect" style="padding-top:10px;">
                <b>File Quantity:</b><br>
                    <select id="dpcFilesToSelect">
                        <option value=""></option>
                    </select>
                </div>
                <!-- Hidden Program -->
                <input type="hidden" name="new-fileUploadProgramEdit" value="{{session('dcpprogramcompose')}}"/>
                <!-- Hidden Deadline -->
                <input type="hidden" value="{{$dpcreport['date_deadline']}}" name="new-DeadlineEdit" id="dpc-dpcDateHidden"/>
                <!-- Hidden Mode of Reply -->
                <input type="hidden" name="new-fileUploadModeofReplyEdit" id="dpcModeofReply"/>
                <!-- Hidden File Quantity -->
                <input type="hidden" name="new-fileUploadQuantityEdit" id="dpc-FileQuantity"/>
                <!-- Hidden DateTime -->
                <input type="hidden" name="new-ReportDateUpdated" value="{{date('YmdHis')}}"/>
                <!-- Hidden ID -->
                <input type="hidden" name="new-ReportIDEdit" value="{{$dpcreport['id']}}"/>
                <div>
                    <input type="submit" value="Compose" class="btn btn-info float-right"/>
                </div>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
</div>
<script>
//Document ready hide all + for loop
$(function(){
    $(".dpcFileSelect").hide();
    for(i = 1; i <= 3; i++) {
        $("#dpcFilesToSelect").append("<option value="+ i +">"+ i +"</option>");
    }
});
$("#dpc-dpcDate").on('change',function(){
    $("#dpc-dpcDateHidden").val($(this).val());
});
//Radio to Select.change
$("input[name=dpcRadio]").change(function(){
    if($("input[name=dpcRadio]:checked").val() == "File"){
        $(".dpcFileSelect").show();
        $("#dpcFilesToSelect").attr("required",true);
        $("#dpcModeofReply").val($("input[name=dpcRadio]:checked").val());
        $("#dpc-FileQuantity").val($("#dpcFilesToSelect").val());
        //Files to Select.change
        $("#dpcFilesToSelect").on('change',function(){
            $("#dpc-FileQuantity").val($("#dpcFilesToSelect").val());
        });
    }
    else{
        $(".dpcFileSelect").hide();
        $("#dpcFilesToSelect").attr("required",false);
        $("#dpcModeofReply").val($("input[name=dpcRadio]:checked").val());
        $("#dpc-FileQuantity").val('Empty');
        /*$(".div-fileUpload1").hide();
        $(".div-fileUpload2").hide();
        $(".div-fileUpload3").hide();*/
    }
});
</script>
@endsection