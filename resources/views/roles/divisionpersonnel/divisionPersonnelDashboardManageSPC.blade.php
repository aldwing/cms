@extends('layouts.divisionpersonnel')
@section('title')
CMS | DPC
@endsection
@section('program')
    @foreach($dpcprograms as $data1)
        <a class="dropdown-item-move" href="/cms/dp/dashboard/reports/{{$data1}}"><i class="fas fa-copy" style="padding-right:10px;"></i> {{$data1}}</a>
    @endforeach
@endsection
@section('content')
<a href="{{route('_divisionPersonnelDashboardManageSPCAdd')}}" id="dpc-SPC-ModalAdd"><button class="btn btn-primary" style="margin-bottom:10px"><i class="fas fa-plus-square"></i> Add</button></a>
<a href="{{route('_divisionPersonnelDashboardManageSPCArchive')}}" id="dpc-SPC-Archive"><button class="btn btn-info" style="margin-bottom:10px"><i class="fas fa-user-times"></i> View Archive</button></a>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>School</th>
            <th>Role</th>
            <th>Program</th>
            <th>Contact Number</th>
            <th>Reset</th>
            <th>Edit</th>
            <th>Drop</th>
        </tr>
    </thead>
    <tbody>
        @foreach($dpcgetspc as $data)
        <tr>
            <td>{{$data['Name']}}</td>
            <td>{{$data['email']}}</td>
            <td>{{$data['office']}}</td>
            <td>{{$data['role']}}</td>
            <td>{{$data['program']}}</td>
            <td>{{$data['contact_number']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="dpc-SPC-ModalReset" data-dpcresetspcemail="{{$data['email']}}" data-dpcresetspcid="{{$data['id']}}" data-toggle="modal" data-target="#dpc-SPC-Reset"><i class="fas fa-sync" style="color:black;"></i></a></td>
            <td><a href="/cms/dp/dashboard/managespc/edit/{{$data['id']}}"><button class="btn"><i class="fas fa-user-edit"></i></button></a></td>
            <td><a href="#" class="dpc-SPC-ModalDrop" data-dpcdropspcid="{{$data['id']}}" data-toggle="modal" data-target="#dpc-SPC-Drop"><button class="btn"><i class="fas fa-trash-alt" style="color:red;"></i></button></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Reset -->
<div class="modal fade" id="dpc-SPC-Reset" tabindex="-1" role="dialog" aria-labelledby="dpc-SPC-Reset" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_divisionPersonnelResetSPC')}}" method="post">
            {!!csrf_field()!!}
                The School Personnel's password will be reset.
                <input type="hidden" name="new-DPC-ResetEmail" class="form-username form-control" id="dpc-SPC-ResetEmail" required>
                <input type="hidden" name="new-DPC-ResetID" id="dpc-SPC-ResetID" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Reset</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Drop -->
<div class="modal fade" id="dpc-SPC-Drop" tabindex="-1" role="dialog" aria-labelledby="dpc-SPC-Drop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_divisionPersonnelDropSPC')}}" method="post">
            {!!csrf_field()!!}
                The School Personnel will be sent to archive.
                <input type="hidden" name="new-DPC-DropID" class="form-username form-control" id="dpc-SPC-DropID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Drop</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.dpc-SPC-ModalReset', function () {
    $("#dpc-SPC-ResetEmail").val($(this).data("dpcresetspcemail"));
    $("#dpc-SPC-ResetID").val($(this).data("dpcresetspcid"));
});
$(".adminDashboardManageDP tbody").on('click', '.dpc-SPC-ModalDrop', function () {
    $("#dpc-SPC-DropID").val($(this).data("dpcdropspcid"));
});
</script>
@endsection