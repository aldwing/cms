@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- Modal for add ? -->
<div class="modal-header" style="font-weight:bold;">
    Add Office
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="{{route('_adminInsertOffice')}}" method="post">
                    {!! csrf_field() !!}
                    <b>Office:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Office</label>
                        <input type="text" name="new-Office-OfficeName" placeholder="Office" class="form-username form-control" id="admin-Office-OfficeName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="admin-Office-ApplyA">Submit</button>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
@endsection
