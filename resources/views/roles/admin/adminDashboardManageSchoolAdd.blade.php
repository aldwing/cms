@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<div class="modal-header" style="font-weight:bold;">
    Add School
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="{{route('_adminInsertSchool')}}" method="post">
                    {!! csrf_field() !!}
                    <b>School Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">School Name</label>
                        <input type="text" name="new-School-Name" placeholder="School Name" class="form-username form-control" id="admin-School-Name" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Address:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Address</label>
                        <input type="text" name="new-School-Address" placeholder="Address" class="form-username form-control" id="admin-School-Address" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Contact Number:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Contact Number</label>
                        <input type="text" onkeydown="return event.keyCode !== 69" name="new-School-ContactNumber" placeholder="Contact Number" class="form-username form-control" id="admin-School-ContactNumber" pattern="[0-9\s()]+" required>
                    </div>
                    <b>School Level:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">School Level</label>
                        <select name="new-School-SchoolLevel" class="form-username form-control" id="admin-School-SchoolLevel" required>
                            <option value="">Choose the School's Level</option>
                            <option value="Primary">Primary</option>
                            <option value="Secondary">Secondary</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="admin-School-ApplyA">Submit</button>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<!-- end of Modal add -->
@endsection
