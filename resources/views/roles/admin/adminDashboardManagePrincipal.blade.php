@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- New Personnel/New Office/ New Program -->
<button class="btn btn-primary admin-Principal-ModalAdd" style="margin-bottom:10px" data-toggle="modal" data-target="#adminPrincipalAdd"><i class="fas fa-plus-square"></i> Add</button>
<a href="{{route('_adminDashboardManagePrincipalArchive')}}" id="admin-DP-Archive"><button class="btn btn-info" style="margin-bottom:10px"><i class="fas fa-user-times"></i> View Archive</button></a>
{!!csrf_field()!!}
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>School</th>
            <th>Role</th>
            <th>Contact Number</th>
            <th>Reset</th>
            <th>Edit</th>
            <th>Drop</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getprincipal as $data)
        <tr>
            <td>{{$data['Name']}}</td>
            <td>{{$data['email']}}</td>
            <td>{{$data['office']}}</td>
            <td>{{$data['role']}}</td>
            <td>{{$data['contact_number']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="admin-Principal-ModalReset" data-principalresetemail="{{$data['email']}}" data-principalresetid="{{$data['id']}}" data-toggle="modal" data-target="#admin-Principal-Reset"><i class="fas fa-sync" style="color:black;"></i></a></td>
            <td><button class="btn admin-Principal-ModalEdit" data-principalfirstname="{{$data['first_name']}}" data-principalmiddlename="{{$data['middle_name']}}" data-principalsurname="{{$data['surname']}}" data-principalemail="{{$data['email']}}" data-principalid="{{$data['id']}}" data-principaloffice="{{$data['office']}}" data-principalcontactnumber="{{$data['contact_number']}}" data-toggle="modal" data-target="#adminPrincipalEdit"><i class="fas fa-user-edit"></i></button></td>
            <td><a href="#" class="admin-Principal-ModalDrop" data-principaldropid="{{$data['id']}}" data-toggle="modal" data-target="#admin-Principal-Drop"><i class="fas fa-trash-alt" style="color:red;"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- MODALS -->
<!-- Modal for add ? -->
<div class="modal fade" id="adminPrincipalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Add Principal
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="{{route('_adminInsertPrincipal')}}" method="post">
                                {!! csrf_field() !!}
                                <b>First Name:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">First Name</label>
                                    <input type="text" name="new-Principal-FirstName" placeholder="First Name" class="form-username form-control" id="admin-Principal-FirstName" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Middle Name:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Middle Name</label>
                                    <input type="text" name="new-Principal-MiddleName" placeholder="Middle Name" class="form-username form-control" id="admin-Principal-MiddleName" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Surname:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Surname</label>
                                    <input type="text" name="new-Principal-Surname" placeholder="Surname" class="form-username form-control" id="admin-Principal-Surname" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Email:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Email</label>
                                    <input type="email" name="new-Principal-Email" placeholder="Email" class="form-username form-control" id="admin-Principal-Email" pattern="[a-zA-Z0-9\s(),'@.]+" required>
                                </div>
                                <div id="adminNoSchools" class="alert alert-warning" role="alert">
                                    There are no schools, please create a school first. Click the link below!</p>
                                    <a href="{{route('_adminDashboardManageSchoolAdd')}}" style="color:blue;"><i class="fas fa-arrow-right"></i> Add new</a>
                                </div>
                                <b>School:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">School</label>
                                    <select name="new-Principal-Office" placeholder="School" class="form-username form-control" id="admin-Principal-Office">
                                        <option id="admin-Principal-GetSchoolOption"></option>
                                    </select>
                                </div>
                                <b>Contact Number:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Contact Number</label>
                                    <input type="text" onkeydown="return event.keyCode !== 69" name="new-Principal-ContactNumber" placeholder="Contact Number" class="form-username form-control" id="admin-Principal-ContactNumber" pattern="[0-9\s()]+" required>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-Principal-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- end of Modal add -->
<!-- Modal for edit ? -->
<div class="modal fade" id="adminPrincipalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Edit Principal
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="{{route('_adminUpdatePrincipal')}}" method="post">
                                {!! csrf_field() !!}
                                <b>First Name:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">First Name</label>
                                    <input type="text" name="new-Principal-FirstNameEdit" placeholder="First Name" class="form-username form-control" id="admin-Principal-FirstNameEdit" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Middle Name:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Middle Name</label>
                                    <input type="text" name="new-Principal-MiddleNameEdit" placeholder="Middle Name" class="form-username form-control" id="admin-Principal-MiddleNameEdit" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Surname:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Surname</label>
                                    <input type="text" name="new-Principal-SurnameEdit" placeholder="Surname" class="form-username form-control" id="admin-Principal-SurnameEdit" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Email:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Email</label>
                                    <input type="email" name="new-Principal-EmailEdit" placeholder="Email" class="form-username form-control" id="admin-Principal-EmailEdit" pattern="[a-zA-Z0-9\s(),'@.]+" required>
                                </div>
                                <b>School:</b> <b id="admin-Principal-BoldEdit"></b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">School</label>
                                    <select name="new-Principal-OfficeEdit" placeholder="School" class="form-username form-control" id="admin-Principal-OfficeEdit">
                                    </select>
                                </div>
                                <b>Contact Number:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Contact Number</label>
                                    <input type="text" onkeydown="return event.keyCode !== 69" name="new-Principal-ContactNumberEdit" placeholder="Contact Number" class="form-username form-control" id="admin-Principal-ContactNumberEdit" pattern="[0-9\s()]+" required>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="new-Principal-IDEdit" placeholder="ID" class="form-username form-control" id="admin-Principal-IDEdit" required>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-Principal-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- end of Modal edit -->
<!-- Modal Reset -->
<div class="modal fade" id="admin-Principal-Reset" tabindex="-1" role="dialog" aria-labelledby="admin-Principal-Reset" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminResetPrincipal')}}" method="post">
            {!!csrf_field()!!}
                The Principal's password will be reset.
                <input type="hidden" name="new-Principal-ResetEmail" class="form-username form-control" id="admin-Principal-ResetEmail" required>
                <input type="hidden" name="new-Principal-ResetID" id="admin-Principal-ResetID" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Reset</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Drop -->
<div class="modal fade" id="admin-Principal-Drop" tabindex="-1" role="dialog" aria-labelledby="admin-Principal-Drop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDropPrincipal')}}" method="post">
            {!!csrf_field()!!}
                The Principal will be sent to archive.
                <input type="hidden" name="new-Principal-DropID" class="form-username form-control" id="admin-Principal-DropID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Drop</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.admin-Principal-ModalEdit', function () {
    _adminGetPrincipalSchoolEdit();
    $("#admin-Principal-FirstNameEdit").val($(this).data("principalfirstname"));
    $("#admin-Principal-MiddleNameEdit").val($(this).data("principalmiddlename"));
    $("#admin-Principal-SurnameEdit").val($(this).data("principalsurname"));
    $("#admin-Principal-EmailEdit").val($(this).data("principalemail"));
    $("#admin-Principal-ContactNumberEdit").val($(this).data("principalcontactnumber"));
    $("#admin-Principal-IDEdit").val($(this).data("principalid"));
    $("#admin-Principal-BoldEdit").text('( '+$(this).data("principaloffice")+' )');
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Principal-ModalReset', function () {
    $("#admin-Principal-ResetEmail").val($(this).data("principalresetemail"));
    $("#admin-Principal-ResetID").val($(this).data("principalresetid"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Principal-ModalDrop', function () {
    $("#admin-Principal-DropID").val($(this).data("principaldropid"));
})
/* School */
$(".admin-Principal-ModalAdd").click(function(){
    _adminGetPrincipalSchoolAdd();
})
/* Selecting * School */
function _adminGetPrincipalSchoolAdd(){
    $.ajax({
        type: "POST",
        url: "{{route ('_adminDashboardPrincipalGetSchool')}}",
        data: {_token:"{!! csrf_token() !!}"},
        success: function(data){
            $("#admin-Principal-Office").html("");
            for (i=0; i < data.length; i++){
                $("#admin-Principal-Office").append('<option>' + data[i]["name"] + '</option>');
            }
            if(data[0]["name"]==""){
                $("#adminNoSchools").show();
                $("#admin-Principal-Office").attr('disabled', 'disabled');
                $("#admin-Principal-ApplyA").attr('disabled', 'disabled');
            }
            else{
                $("#adminNoSchools").hide();
            }
        }
    });
}
/* Selecting * School */
function _adminGetPrincipalSchoolEdit(){
    $.ajax({
        type: "POST",
        url: "{{route ('_adminDashboardPrincipalGetSchool')}}",
        data: {_token:"{!! csrf_token() !!}"},
        success: function(data){
            $("#admin-Principal-OfficeEdit").html("");
            for (i1=0; i1 < data.length; i1++){
                $("#admin-Principal-OfficeEdit").append('<option>' + data[i1]["name"] + '</option>');
            }
        }
    });
}
/* End of School */
</script>
@endsection
