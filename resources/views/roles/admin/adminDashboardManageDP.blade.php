@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<a href="{{route('_adminDashboardManageDPAdd')}}" id="admin-DP-ModalAdd"><button class="btn btn-primary" style="margin-bottom:10px"><i class="fas fa-plus-square"></i> Add</button></a>
<a href="{{route('_adminDashboardManageDPArchive')}}" id="admin-DP-Archive"><button class="btn btn-info" style="margin-bottom:10px"><i class="fas fa-user-times"></i> View Archive</button></a>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Office</th>
            <th>Role</th>
            <th>Program</th>
            <th>Contact Number</th>
            <th>Reset</th>
            <th>Edit</th>
            <th>Drop</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getdp as $data)
        <tr>
            <td>{{$data['Name']}}</td>
            <td>{{$data['email']}}</td>
            <td>{{$data['office']}}</td>
            <td>{{$data['role']}}</td>
            <td>{{$data['program']}}</td>
            <td>{{$data['contact_number']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="admin-DP-ModalReset" data-dpresetemail="{{$data['email']}}" data-dpresetid="{{$data['id']}}" data-toggle="modal" data-target="#admin-DP-Reset"><i class="fas fa-sync" style="color:black;"></i></a></td>
            <td><a href="/cms/admin/dashboard/managedp/edit/{{$data['id']}}"><button class="btn"><i class="fas fa-user-edit"></i></button></a></td>
            <td><a href="#" class="admin-DP-ModalDrop" data-dpdropid="{{$data['id']}}" data-toggle="modal" data-target="#admin-DP-Drop"><button class="btn"><i class="fas fa-trash-alt" style="color:red;"></i></button></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Reset -->
<div class="modal fade" id="admin-DP-Reset" tabindex="-1" role="dialog" aria-labelledby="admin-DP-Reset" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminResetDivisionPersonnel')}}" method="post">
            {!!csrf_field()!!}
                The Division Personnel's password will be reset.
                <input type="hidden" name="new-DP-ResetEmail" class="form-username form-control" id="admin-DP-ResetEmail" required>
                <input type="hidden" name="new-DP-ResetID" id="admin-DP-ResetID" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Reset</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Drop -->
<div class="modal fade" id="admin-DP-Drop" tabindex="-1" role="dialog" aria-labelledby="admin-DP-Drop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDropDivisionPersonnel')}}" method="post">
            {!!csrf_field()!!}
                The Division Personnel will be sent to archive.
                <input type="hidden" name="new-DP-DropID" class="form-username form-control" id="admin-DP-DropID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Drop</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.admin-DP-ModalReset', function () {
    $("#admin-DP-ResetEmail").val($(this).data("dpresetemail"));
    $("#admin-DP-ResetID").val($(this).data("dpresetid"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-DP-ModalDrop', function () {
    $("#admin-DP-DropID").val($(this).data("dpdropid"));
})
</script>
@endsection
