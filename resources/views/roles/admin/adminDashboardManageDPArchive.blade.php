@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- New Personnel/New Office/ New Program -->
{!!csrf_field()!!}
<style>
.errorAdminManageDPArchive{ 
    border: 3px solid rgba(255, 0, 0, .6);
}
.successAdminManageDPArchive{
    border: 3px solid rgba(128, 189, 255, .6);
}
</style>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Office</th>
            <th>Role</th>
            <th>Program</th>
            <th>Contact Number</th>
            <th>Restore</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getdparchive as $data)
        <tr>
            <td>{{$data['Name']}}</td>
            <td>{{$data['email']}}</td>
            <td>{{$data['office']}}</td>
            <td>{{$data['role']}}</td>
            <td>{{$data['program']}}</td>
            <td>{{$data['contact_number']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="admin-DP-ArchiveRestore" data-dparchiverestoreid="{{$data['id']}}" data-toggle="modal" data-target="#admin-DP-ArchiveRestore"><i class="fas fa-undo" style="color:blue;"></i></a></td>
            <td><a href="#" class="admin-DP-ArchiveSessionEmail" data-managedparchivesessionemail="{{$data['email']}}" data-toggle="modal" data-target="#admin-DP-Delete"><i class="fas fa-trash-alt" style="color:red;"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Restore -->
<div class="modal fade" id="admin-DP-ArchiveRestore" tabindex="-1" role="dialog" aria-labelledby="admin-DP-ArchiveRestore" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageDPRestore')}}" method="post">
            {!!csrf_field()!!}
                The Division Personnel will be restored.
                <input type="hidden" name="new-DP-ArchiveRestoreID" class="form-username form-control" id="admin-DP-ArchiveRestoreID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Restore</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" id="admin-DP-Delete" tabindex="-1" role="dialog" aria-labelledby="admin-DP-Delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageDPDelete')}}" method="post" class="submitDPArchive">
                {!! csrf_field() !!}
                Deleting an archived personnel cannot be recovered. <!--Click <b style="color:red;">Delete</b> <b style="color:red;" id="admin-DP-ArchiveBold" name="admin-DP-ArchiveEmailName"></b> if you want to delete this archived user.-->
                <input type="hidden" name="new-DP-ArchiveEmail" class="form-username form-control" id="admin-DP-ArchiveEmail" required>
                <input type="hidden" name="admin-DP-ArchiveHiddenEmail" id="admin-DP-ArchiveHidden" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary admin-DP-ArchiveSubmit" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.admin-DP-ArchiveSessionEmail', function () {
    /* Bold
    $("#admin-DP-ArchiveBold").html($(this).data("managedparchivesessionemail"));
    */
    /* input exag */
    $("#admin-DP-ArchiveEmail").val($(this).data("managedparchivesessionemail"));
    $("#admin-DP-ArchiveHidden").val($(this).data("managedparchivesessionemail"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-DP-ArchiveRestore', function () {
    $("#admin-DP-ArchiveRestoreID").val($(this).data("dparchiverestoreid"));
})
$(document).ready(function(){ 
    $(".submitDPArchive").submit(function(event){ 
        if ( $('#admin-DP-ArchiveHidden').val() !== $("#admin-DP-ArchiveEmail").val() ) {
            $('#admin-DP-ArchiveEmail').addClass("errorAdminManageDPArchive");
            event.preventDefault(); 
        }
        else{
            $('#admin-DP-ArchiveEmail').addClass("successAdminManageDPArchive");
        }
    }); 
});
</script>
@endsection