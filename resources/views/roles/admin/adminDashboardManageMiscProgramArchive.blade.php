@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<style>
.errorAdminManageDPArchive{ 
    border: 3px solid rgba(255, 0, 0, .6);
}
.successAdminManageDPArchive{
    border: 3px solid rgba(128, 189, 255, .6);
}
</style>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Program</th>
            <th>Description</th>
            <th>Restore</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getprogramarchive as $data)
        <tr>
            <td>{{$data['program']}}</td>
            <td>{{$data['description']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="admin-Program-ArchiveRestore" data-programarchiverestoreid="{{$data['id']}}" data-toggle="modal" data-target="#admin-Program-ArchiveRestore"><i class="fas fa-undo" style="color:blue;"></i></a></td>
            <td><a href="#" class="admin-Program-ArchiveSessionProgram" data-manageprogramarchivesessionprogram="{{$data['program']}}" data-toggle="modal" data-target="#admin-Program-Delete"><i class="fas fa-trash-alt" style="color:red;"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Restore -->
<div class="modal fade" id="admin-Program-ArchiveRestore" tabindex="-1" role="dialog" aria-labelledby="admin-Program-ArchiveRestore" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageProgramRestore')}}" method="post">
            {!!csrf_field()!!}
                The Program will be restored.
                <input type="hidden" name="new-Program-ArchiveRestoreID" class="form-username form-control" id="admin-Program-ArchiveRestoreID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Restore</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" id="admin-Program-Delete" tabindex="-1" role="dialog" aria-labelledby="admin-Program-Delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageProgramDelete')}}" method="post" class="submitSchoolArchive">
                {!! csrf_field() !!}
                Deleting an archived program cannot be recovered.<!-- Click <b style="color:red;">Delete</b> <b style="color:red;" id="admin-DP-ArchiveBold" name="admin-DP-ArchiveEmailName"></b> if you want to delete this archived user.-->
                <input type="hidden" name="new-Program-ArchiveName" class="form-username form-control" id="admin-Program-ArchiveName" required>
                <input type="hidden" name="admin-Program-ArchiveHiddenName" id="admin-Program-ArchiveHidden" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary admin-School-ArchiveSubmit" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.admin-Program-ArchiveSessionProgram', function () {
    $("#admin-Program-ArchiveName").val($(this).data("manageprogramarchivesessionprogram"));
    $("#admin-Program-ArchiveHidden").val($(this).data("manageprogramarchivesessionprogram"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Program-ArchiveRestore', function () {
    $("#admin-Program-ArchiveRestoreID").val($(this).data("programarchiverestoreid"));
})
$(document).ready(function(){ 
    $(".submitSchoolArchive").submit(function(event){ 
        if ( $('#admin-Program-ArchiveHidden').val() !== $("#admin-Program-ArchiveName").val() ) {
            $('#admin-Program-ArchiveName').addClass("errorAdminManageDPArchive");
            event.preventDefault(); 
        }
        else{
            $('#admin-Program-ArchiveName').addClass("successAdminManageDPArchive");
        }
    }); 
});
</script>
@endsection