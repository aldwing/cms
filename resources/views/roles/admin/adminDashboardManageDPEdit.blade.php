@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- New Personnel/New Office/ New Program -->
<!-- MODALS -->
<!-- Modal for edit ? -->
<div class="modal-header" style="font-weight:bold;">
    Edit Division Personnel
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="{{route('_adminUpdateDivisionPersonnel')}}" method="post">
                    {!! csrf_field() !!}
                    @foreach($admineditdp as $data)
                    <b>First Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">First Name</label>
                        <input type="text" name="new-DP-FirstName-edit" placeholder="First Name" value="{{$data['first_name']}}" class="form-username form-control" id="admin-DP-FirstName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Middle Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Middle Name</label>
                        <input type="text" name="new-DP-MiddleName-edit" placeholder="Middle Name" value="{{$data['middle_name']}}" class="form-username form-control" id="admin-DP-MiddleName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Surname:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Surname</label>
                        <input type="text" name="new-DP-Surname-edit" placeholder="Surname" value="{{$data['surname']}}" class="form-username form-control" id="admin-DP-Surname" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Email:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Email</label>
                        <input type="email" name="new-DP-Email-edit" placeholder="Email" value="{{$data['email']}}" class="form-username form-control" id="admin-DP-Email" pattern="[a-zA-Z0-9\s(),'@.]+" required>
                    </div>
                    <div id="adminNoOffice" class="alert alert-warning" role="alert">
                        There are no offices, please create an office first. Click the link below!</p>
                        <a href="{{route('_adminDashboardManageMiscOffice')}}" style="color:blue;"><i class="fas fa-arrow-right"></i> Add new</a>
                    </div>
                    <b>Office:</b> ({{$data['office']}})
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Office</label>
                        <select name="new-DP-Office-edit" placeholder="Office" class="form-username form-control" id="admin-DP-GetOfficeAssignments">
                            @foreach($adminnewoffice as $data1)
                                <option value="{{$data['office']}}">{{$data1['office']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- Program -->
                    <b>Program:</b> ({{$data['program']}})
                    <div class="container">
                        <div class="card">
                            <div class="card-body">
                                <div id="adminNoProgram" class="alert alert-warning" role="alert">
                                    There are no programs, please create a program first. Click the link below!</p>
                                    <a href="{{route('_adminDashboardManageMiscProgram')}}" style="color:blue;"><i class="fas fa-arrow-right"></i> Add new</a>
                                </div>
                                Active Program/s:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="form-username" placeholder="Program" class="form-username form-control" id="admin-DP-GetPrograms" size="7">
                                    @foreach($adminnewprogram as $data2)
                                            <option>{{$data2['program']}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div style="text-align:center;padding-bottom:15px;">
                                        <button type="button" class="btn btn-info" id="admin-DP-GetPrograms-Button1">Add</button>
                                </div>
                                Program/s to be added:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="new-DP-Program" placeholder="Program" class="form-username form-control" id="admin-DP-GetPrograms-Transferred" size="7" pattern="[a-zA-Z0-9\s(),']+" required>
                                    </select>
                                </div>
                                <!-- Hidden input -->
                                <div class="form-group">
                                    <input name="new-DP-Programs-edit" type="hidden" id="admin-DP-GetPrograms-Hidden">
                                </div>
                                <!-- end of hidden input -->
                                <div style="text-align:center;">
                                        <button type="button" class="btn btn-danger" id="admin-DP-GetPrograms-Button2">Remove All</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Program -->
                    <b>Contact Number:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Contact Number</label>
                        <input type="text" onkeydown="return event.keyCode !== 69" name="new-DP-ContactNumber-edit" placeholder="Contact Number" value="{{$data['contact_number']}}" class="form-username form-control" id="admin-DP-ContactNumber" pattern="[0-9\s()]+" required>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="admin-DP-ApplyA">Submit</button>
                    @endforeach
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<!-- end of Modal edit -->
<script>
$(function(){
    if($("#admin-DP-GetOfficeOption").html()==""){
        $("#adminNoOffice").show();
        $("#admin-DP-GetOfficeAssignments").attr('disabled', 'disabled');
        $("#admin-DP-ApplyA").attr('disabled', 'disabled');
    }
    else{
        $("#adminNoOffice").hide();
    }
    if($("#admin-DP-GetProgramsOption").html()==""){
        $("#adminNoProgram").show();
        $("#admin-DP-GetPrograms").attr('disabled', 'disabled');
        $("#admin-DP-GetPrograms-Transferred").attr('disabled', 'disabled');
        $("#admin-DP-GetPrograms-Button1").attr('disabled', 'disabled');
        $("#admin-DP-GetPrograms-Button2").attr('disabled', 'disabled');
        $("#admin-DP-ApplyA").attr('disabled', 'disabled');
    }
    else{
        $("#adminNoProgram").hide();
    }
})
/* EDIT*/
    $("#admin-DP-GetPrograms-Button1").click(function(){
        $("#admin-DP-GetPrograms > option:selected").each(function(){
            $(this).remove().appendTo("#admin-DP-GetPrograms-Transferred");
        });
        /* Separator */
        var selectedval = $("#admin-DP-GetPrograms-Hidden").val();
        selectedval += $("#admin-DP-GetPrograms-Transferred").val() + ", ";
        $("#admin-DP-GetPrograms-Hidden").val(selectedval);
        /* end of separator */
    });
    $("#admin-DP-GetPrograms-Button2").click(function(){
        $('#admin-DP-GetPrograms-Transferred > option').remove().appendTo("#admin-DP-GetPrograms");
        $('#admin-DP-GetPrograms-Hidden').val('');
    });
    /* Remove comma */
    $("#admin-DP-ApplyA").on("click", function(){
        var removedComma = $("#admin-DP-GetPrograms-Hidden").val();
        $('#admin-DP-GetPrograms-Hidden').val(removedComma.substring(0,removedComma.length - 2));
    });
    /* end of remove comma */
/* EDIT */
</script>
@endsection
