
@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- Modal for add ? -->
<div class="modal-header" style="font-weight:bold;">
    Add Principal
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="{{route('_adminInsertPrincipal')}}" method="post">
                    {!! csrf_field() !!}
                    <b>First Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">First Name</label>
                        <input type="text" name="new-Principal-FirstName" placeholder="First Name" class="form-username form-control" id="admin-Principal-FirstName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Middle Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Middle Name</label>
                        <input type="text" name="new-Principal-MiddleName" placeholder="Middle Name" class="form-username form-control" id="admin-Principal-MiddleName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Surname:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Surname</label>
                        <input type="text" name="new-Principal-Surname" placeholder="Surname" class="form-username form-control" id="admin-Principal-Surname" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Email:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Email</label>
                        <input type="email" name="new-Principal-Email" placeholder="Email" class="form-username form-control" id="admin-Principal-Email" pattern="[a-zA-Z0-9\s(),'@.]+" required>
                    </div>
                    <div id="adminNoSchool" class="alert alert-warning" role="alert">
                        There are no schools, please create a school first. Click the link below!</p>
                        <a href="{{route('_adminDashboardManageSchoolAdd')}}" style="color:blue;"><i class="fas fa-arrow-right"></i> Add new</a>
                    </div>
                    <b>School:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">School</label>
                        <select name="new-Principal-Office" placeholder="School" class="form-username form-control" id="admin-Principal-Office">
                            @foreach($getprincipal1 as $data)
                                <option id="admin-Principal-GetSchoolOption">{{$data['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <b>Contact Number:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Contact Number</label>
                        <input type="text" onkeydown="return event.keyCode !== 69" name="new-Principal-ContactNumber" placeholder="Contact Number" class="form-username form-control" id="admin-Principal-ContactNumber" pattern="[0-9\s()]+" required>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="admin-Principal-ApplyA">Submit</button>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<!-- end of Modal add -->
<script>
$(function(){
    if($("#admin-Principal-GetSchoolOption").html()==""){
        $("#adminNoSchool").show();
        $("#admin-Principal-Office").attr('disabled', 'disabled');
        $("#admin-Principal-ApplyA").attr('disabled', 'disabled');
    }
    else{
        $("#adminNoSchool").hide();
    }
})
</script>
@endsection
