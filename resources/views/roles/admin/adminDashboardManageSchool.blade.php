@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<button class="btn btn-primary" style="margin-bottom:10px" data-toggle="modal" data-target="#adminSchoolAdd"><i class="fas fa-plus-square"></i> Add</button>
<a href="{{route('_adminDashboardManageSchoolArchive')}}" id="admin-DP-Archive"><button class="btn btn-info" style="margin-bottom:10px"><i class="fas fa-user-times"></i> View Archive</button></a>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Contact Number</th>
            <th>School Level</th>
            <th>Edit</th>
            <th>Drop</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getschool as $data)
        <tr>
            <td>{{$data['name']}}</td>
            <td>{{$data['address']}}</td>
            <td>{{$data['contact_number']}}</td>
            <td>{{$data['level']}}</td>
            <!-- Button trigger modal for edit-->
            <td><button class="btn" id="admin-School-ModalEdit" data-schoolname="{{$data['name']}}" data-schooladdress="{{$data['address']}}" data-schoolcontactnumber="{{$data['contact_number']}}" data-schoolid="{{$data['id']}}" data-toggle="modal" data-target="#adminSchoolEdit"><i class="fas fa-user-edit"></i></button></td>
            <td><a href="#" class="admin-School-ModalDrop" data-schooldropid="{{$data['id']}}" data-toggle="modal" data-target="#admin-School-Drop"><button class="btn"><i class="fas fa-trash-alt" style="color:red;"></i></button></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- MODALS -->
<!-- Modal for add ? -->
<div class="modal fade" id="adminSchoolAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Add School
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="{{route('_adminInsertSchool')}}" method="post">
                                {!! csrf_field() !!}
                                <b>School Name:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">School Name</label>
                                    <input type="text" name="new-School-Name" placeholder="School Name" class="form-username form-control" id="admin-School-Name" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Address:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Address</label>
                                    <input type="text" name="new-School-Address" placeholder="Address" class="form-username form-control" id="admin-School-Address" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Contact Number:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Contact Number</label>
                                    <input type="text" onkeydown="return event.keyCode !== 69" name="new-School-ContactNumber" placeholder="Contact Number" class="form-username form-control" id="admin-School-ContactNumber" pattern="[0-9\s()]+" required>
                                </div>
                                <b>School Level:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">School Level</label>
                                    <select name="new-School-SchoolLevel" class="form-username form-control" id="admin-School-SchoolLevel" required>
                                        <option value="">Choose the School's Level</option>
                                        <option value="Primary">Primary</option>
                                        <option value="Secondary">Secondary</option>
                                    </select>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-School-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- end of Modal add -->
<!-- Modal for edit ? -->
<div class="modal fade" id="adminSchoolEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Edit School
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="{{route('_adminUpdateSchool')}}" method="post">
                                {!! csrf_field() !!}
                                <b>School Name:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">School Name</label>
                                    <input type="text" name="new-School-NameEdit" placeholder="School Name" class="form-username form-control" id="admin-School-NameEdit" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Address:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Address</label>
                                    <input type="text" name="new-School-AddressEdit" placeholder="Address" class="form-username form-control" id="admin-School-AddressEdit" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <b>Contact Number:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Contact Number</label>
                                    <input type="text" onkeydown="return event.keyCode !== 69" name="new-School-ContactNumberEdit" placeholder="Contact Number" class="form-username form-control" id="admin-School-ContactNumberEdit" pattern="[0-9\s()]+" required>
                                </div>
                                <b>School Level:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">School Level</label>
                                    <select name="new-School-SchoolLevelEdit" class="form-username form-control" id="admin-School-SchoolLevelEdit" required>
                                        <option value="">Choose the School's Level</option>
                                        <option value="Primary">Primary</option>
                                        <option value="Secondary">Secondary</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="new-School-IDEdit" placeholder="Contact Number" class="form-username form-control" id="admin-School-IDEdit" required>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-School-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- end of Modal edit -->
<!-- Modal Drop -->
<div class="modal fade" id="admin-School-Drop" tabindex="-1" role="dialog" aria-labelledby="admin-School-Drop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDropSchool')}}" method="post">
            {!!csrf_field()!!}
                The School will be sent to archive.
                <input type="hidden" name="new-School-DropID" class="form-username form-control" id="admin-School-DropID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Drop</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '#admin-School-ModalEdit', function () {
    $("#admin-School-NameEdit").val($(this).data("schoolname"));
    $("#admin-School-AddressEdit").val($(this).data("schooladdress"));
    $("#admin-School-ContactNumberEdit").val($(this).data("schoolcontactnumber"));
    $("#admin-School-IDEdit").val($(this).data("schoolid"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-School-ModalDrop', function () {
    $("#admin-School-DropID").val($(this).data("schooldropid"));
})
</script>
@endsection