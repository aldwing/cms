@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- New Personnel/New Office/ New Program -->
{!!csrf_field()!!}
<style>
.errorAdminManageDPArchive{ 
    border: 3px solid rgba(255, 0, 0, .6);
}
.successAdminManageDPArchive{
    border: 3px solid rgba(128, 189, 255, .6);
}
</style>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Contact Number</th>
            <th>School Level</th>
            <th>Restore</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getschoolarchive as $data)
        <tr>
            <td>{{$data['name']}}</td>
            <td>{{$data['address']}}</td>
            <td>{{$data['contact_number']}}</td>
            <td>{{$data['level']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="admin-School-ArchiveRestore" data-schoolarchiverestoreid="{{$data['id']}}" data-toggle="modal" data-target="#admin-School-ArchiveRestore"><i class="fas fa-undo" style="color:blue;"></i></a></td>
            <td><a href="#" class="admin-School-ArchiveSessionName" data-manageschoolarchivesessionname="{{$data['name']}}" data-toggle="modal" data-target="#admin-School-Delete"><i class="fas fa-trash-alt" style="color:red;"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Restore -->
<div class="modal fade" id="admin-School-ArchiveRestore" tabindex="-1" role="dialog" aria-labelledby="admin-School-ArchiveRestore" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageSchoolRestore')}}" method="post">
            {!!csrf_field()!!}
                The School will be restored.
                <input type="hidden" name="new-School-ArchiveRestoreID" class="form-username form-control" id="admin-School-ArchiveRestoreID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Restore</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" id="admin-School-Delete" tabindex="-1" role="dialog" aria-labelledby="admin-School-Delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageSchoolDelete')}}" method="post" class="submitSchoolArchive">
                {!! csrf_field() !!}
                Deleting an archived school cannot be recovered.<!-- Click <b style="color:red;">Delete</b> <b style="color:red;" id="admin-DP-ArchiveBold" name="admin-DP-ArchiveEmailName"></b> if you want to delete this archived user.-->
                <input type="hidden" name="new-School-ArchiveName" class="form-username form-control" id="admin-School-ArchiveName" required>
                <input type="hidden" name="admin-School-ArchiveHiddenName" id="admin-School-ArchiveHidden" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary admin-School-ArchiveSubmit" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.admin-School-ArchiveSessionName', function () {
    /* Bold
    $("#admin-DP-ArchiveBold").html($(this).data("manageschoolarchivesessionname"));
    */
    /* input exag */
    $("#admin-School-ArchiveName").val($(this).data("manageschoolarchivesessionname"));
    $("#admin-School-ArchiveHidden").val($(this).data("manageschoolarchivesessionname"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-School-ArchiveRestore', function () {
    $("#admin-School-ArchiveRestoreID").val($(this).data("schoolarchiverestoreid"));
})
$(document).ready(function(){ 
    $(".submitSchoolArchive").submit(function(event){ 
        if ( $('#admin-School-ArchiveHidden').val() !== $("#admin-School-ArchiveName").val() ) {
            $('#admin-School-ArchiveName').addClass("errorAdminManageDPArchive");
            event.preventDefault(); 
        }
        else{
            $('#admin-School-ArchiveName').addClass("successAdminManageDPArchive");
        }
    }); 
});
</script>
@endsection