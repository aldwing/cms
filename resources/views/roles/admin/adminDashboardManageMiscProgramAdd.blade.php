@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<!-- Modal for add ? -->
<div class="modal-header" style="font-weight:bold;">
    Add Program
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="{{route('_adminInsertProgram')}}" method="post">
                    {!! csrf_field() !!}
                    <b>Program:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Program</label>
                        <input type="text" name="new-Program-ProgramName" placeholder="Program" class="form-username form-control" id="admin-Program-ProgramName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Program Description:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Program Description</label>
                        <textarea name="new-Program-ProgramDescription" placeholder="Program Description" class="form-username form-control" id="admin-Program-ProgramDescription" pattern="[a-zA-Z0-9\s(),']+" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="admin-Program-ApplyA">Submit</button>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>

@endsection
