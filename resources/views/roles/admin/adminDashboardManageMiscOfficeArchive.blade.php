@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<style>
.errorAdminManageDPArchive{ 
    border: 3px solid rgba(255, 0, 0, .6);
}
.successAdminManageDPArchive{
    border: 3px solid rgba(128, 189, 255, .6);
}
</style>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Office</th>
            <th>Restore</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getofficearchive as $data)
        <tr>
            <td>{{$data['office']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="admin-Office-ArchiveRestore" data-officearchiverestoreid="{{$data['id']}}" data-toggle="modal" data-target="#admin-Office-ArchiveRestore"><i class="fas fa-undo" style="color:blue;"></i></a></td>
            <td><a href="#" class="admin-Office-ArchiveSessionOffice" data-manageofficearchivesessionoffice="{{$data['office']}}" data-toggle="modal" data-target="#admin-Office-Delete"><i class="fas fa-trash-alt" style="color:red;"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Restore -->
<div class="modal fade" id="admin-Office-ArchiveRestore" tabindex="-1" role="dialog" aria-labelledby="admin-Office-ArchiveRestore" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageOfficeRestore')}}" method="post">
            {!!csrf_field()!!}
                The Office will be restored.
                <input type="hidden" name="new-Office-ArchiveRestoreID" class="form-username form-control" id="admin-Office-ArchiveRestoreID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Restore</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" id="admin-Office-Delete" tabindex="-1" role="dialog" aria-labelledby="admin-Office-Delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManageOfficeDelete')}}" method="post" class="submitSchoolArchive">
                {!! csrf_field() !!}
                Deleting an archived office cannot be recovered.<!-- Click <b style="color:red;">Delete</b> <b style="color:red;" id="admin-DP-ArchiveBold" name="admin-DP-ArchiveEmailName"></b> if you want to delete this archived user.-->
                <input type="hidden" name="new-Office-ArchiveName" class="form-username form-control" id="admin-Office-ArchiveName" required>
                <input type="hidden" name="admin-Office-ArchiveHiddenName" id="admin-Office-ArchiveHidden" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary admin-School-ArchiveSubmit" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".adminDashboardManageDP tbody").on('click', '.admin-Office-ArchiveSessionOffice', function () {
    $("#admin-Office-ArchiveName").val($(this).data("manageofficearchivesessionoffice"));
    $("#admin-Office-ArchiveHidden").val($(this).data("manageofficearchivesessionoffice"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Office-ArchiveRestore', function () {
    $("#admin-Office-ArchiveRestoreID").val($(this).data("officearchiverestoreid"));
})
$(document).ready(function(){ 
    $(".submitSchoolArchive").submit(function(event){ 
        if ( $('#admin-Office-ArchiveHidden').val() !== $("#admin-Office-ArchiveName").val() ) {
            $('#admin-Office-ArchiveName').addClass("errorAdminManageDPArchive");
            event.preventDefault(); 
        }
        else{
            $('#admin-Office-ArchiveName').addClass("successAdminManageDPArchive");
        }
    }); 
});
</script>
@endsection