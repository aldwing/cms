@extends('layouts.admin')
@section('title')
CMS | Admin
@endsection
@section('content')
<style>
.errorAdminManageDPArchive{ 
    border: 3px solid rgba(255, 0, 0, .6);
}
.successAdminManageDPArchive{
    border: 3px solid rgba(128, 189, 255, .6);
}
</style>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Office</th>
            <th>Role</th>
            <th>Contact Number</th>
            <th>Restore</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        @foreach($getprincipalarchive as $data)
        <tr>
            <td>{{$data['Name']}}</td>
            <td>{{$data['email']}}</td>
            <td>{{$data['office']}}</td>
            <td>{{$data['role']}}</td>
            <td>{{$data['contact_number']}}</td>
            <!-- Button trigger modal for edit-->
            <td><a href="#" class="admin-Principal-ArchiveRestore" data-principalarchiverestoreid="{{$data['id']}}" data-toggle="modal" data-target="#admin-Principal-ArchiveRestore"><i class="fas fa-undo" style="color:blue;"></i></a></td>
            <td><a href="#" class="admin-Principal-ArchiveSessionEmail" data-managedparchiveprincipalsessionemail="{{$data['email']}}" data-toggle="modal" data-target="#admin-Principal-Delete"><i class="fas fa-trash-alt" style="color:red;"></i></a></td>
        </tr>
        @endforeach
    </tbody>
</table>
<!-- Modal Restore -->
<div class="modal fade" id="admin-Principal-ArchiveRestore" tabindex="-1" role="dialog" aria-labelledby="admin-Principal-ArchiveRestore" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManagePrincipalRestore')}}" method="post">
            {!!csrf_field()!!}
                The Principal will be restored.
                <input type="text" name="new-Principal-ArchiveRestoreID" class="form-username form-control" id="admin-Principal-ArchiveRestoreID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info">Restore</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="admin-Principal-Delete" tabindex="-1" role="dialog" aria-labelledby="admin-Principal-Delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="{{route('_adminDashboardManagePrincipalDelete')}}" method="post" class="submitPrincipalArchive">
                {!! csrf_field() !!}
                Deleting an archived personnel cannot be recovered. <!--Click <b style="color:red;">Delete</b> <b style="color:red;" id="admin-DP-ArchiveBold" name="admin-DP-ArchiveEmailName"></b> if you want to delete this archived user.-->
                <input type="hidden" name="new-Principal-ArchiveEmail" class="form-username form-control" id="admin-Principal-ArchiveEmail" required>
                <input type="hidden" name="admin-Principal-ArchiveHiddenEmail" id="admin-Principal-ArchiveHidden" >
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary admin-Principal-ArchiveSubmit" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            </div>
        </div>
    </div>
</div>

<script>
$(".adminDashboardManageDP tbody").on('click', '.admin-Principal-ArchiveSessionEmail', function () {
    /* Bold
    $("#admin-DP-ArchiveBold").html($(this).data("managedparchivesessionemail"));
    */
    /* input exag */
    $("#admin-Principal-ArchiveEmail").val($(this).data("managedparchiveprincipalsessionemail"));
    $("#admin-Principal-ArchiveHidden").val($(this).data("managedparchiveprincipalsessionemail"));
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Principal-ArchiveRestore', function () {
    $("#admin-Principal-ArchiveRestoreID").val($(this).data("principalarchiverestoreid"));
})
$(document).ready(function(){ 
    $(".submitPrincipalArchive").submit(function(event){ 
        if ( $('#admin-Principal-ArchiveHidden').val() !== $("#admin-Principal-ArchiveEmail").val() ) {
            $('#admin-Principal-ArchiveEmail').addClass("errorAdminManageDPArchive");
            event.preventDefault(); 
        }
        else{
            $('#admin-Principal-ArchiveEmail').addClass("successAdminManageDPArchive");
        }
    }); 
});
</script>
@endsection