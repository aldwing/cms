@extends('layouts.schoolpersonnel')
@section('content')
<style>
.reportdescription {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis; 
}
</style>
<div class="modal-header">
    <!-- h5 class="modal-title" id="exampleModalCenterTitle">Add</h5 -->
    <!-- Links for Modal Content -->
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-spcActiveReports-tab" data-toggle="pill" href="#pills-spcActiveReports" role="tab" aria-controls="pills-spcActiveReports" aria-selected="true">Active Reports</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-spcCompliedReports-tab" data-toggle="pill" href="#pills-spcCompliedReports" role="tab" aria-controls="pills-spcCompliedReports" aria-selected="false">Complied Reports</a>
        </li>
    </ul>
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">

    <img src="{{ asset('filesdir/aldwin.jpg') }}" style="width:300px;">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- Active Reports -->
            <div class="tab-pane fade show active" id="pills-spcActiveReports" role="tabpanel" aria-labelledby="pills-spcActiveReports-tab">
               <img src="{{ asset('CMSlogin/img/loadingGif.gif')}}" id="loadingGif" style="display:none;margin:auto;height:121px;width:121px;">
                <table class="table table-striped table-border" id="dataTableActiveReportSPC">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Attachment</th>
                            <th>Description</th>
                            <th>Date Posted</th>
                            <th>Deadline</th>
                            <th>Reply</th>
                        </tr>
                    </thead>
                    <tbody id="activeReportSPC">

                    </tbody>
                </table>
            </div>
            <!-- Complied Reports -->
            <div class="tab-pane fade" id="pills-spcCompliedReports" role="tabpanel" aria-labelledby="pills-spcCompliedReports-tab">
                <table class="table table-striped table-border" id="dataTableCompliedReportSPC">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Date Posted</th>
                            <th>Deadline</th>
                            <th>Date Submitted</th>
                        </tr>
                    </thead>
                    <tbody id="compliedReport">
                        <tr>
                                
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
</div>
<!-- Modal Reply -->
<div class="modal fade" id="spc-ActiveReportReply" tabindex="-1" role="dialog" aria-labelledby="spc-ActiveReportReply" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Reply:</h5>
                </div>
                <div class="modal-body">
                <form role="form" action="#" method="post">
                {!!csrf_field()!!}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info">Submit</button>
                </form>
                </div>
            </div>
        </div>
    </div>
<script>
function _getSPCActiveReport(){
    $("#loadingGif").css("display","block"); 
    $("#dataTableActiveReportSPC").css("display","none");
    $("#dataTableActiveReportSPC").DataTable().destroy();
    $("#activeReportSPC").html("");
    setTimeout(function(){$.ajax({
            type:"POST",
            url:"{{route('_schoolPersonnelProgramGetFilteredReport')}}",
            data:{_token:"{!! csrf_token() !!}"},
            success:function(data){
                for (i=0; i < data.length; i++){
                    $("#activeReportSPC").append('<tr>'
                                              +'<td>'+ data[i]["title"] +'</td>'
                                              +'<td id="unique_spcID'+[i]+'"></td><td>'
                                              + data[i]["description"] +'</td><td>'
                                              + data[i]["date_posted"] +'</td><td>'
                                              + data[i]["date_deadline"] +'</td><td>'
                                              +"<div style='text-align:center;'><a href='#' id='spc-ModalActiveReportReply' data-SPCActiveReportReplyID='"+data[i]["id"]+"' data-toggle='modal' data-target='#spc-ActiveReportReply'><i class='fas fa-reply' style='color:#007BFF;'></i></a></div>"+'</td>'
                                              +'</tr>');
                                              _getSPCActiveReportAttachment("unique_spcID"+i,data[i]["attachment"]);
                }   
                $("#loadingGif").css("display","none");
                $("#dataTableActiveReportSPC").css("display","block");
                $("#dataTableActiveReportSPC").DataTable({
                    retrieve: true,
                    responsive: true,
                    ordering: false,
                });
            }
        });},3025)
}
// function _downloadAttachment(){
//     $.ajax({
//             type:"POST",
//             url:"{{route('_schoolPersonnelProgramDownloadAttachment')}}",
//             data:{_token:"{!! csrf_token() !!}"},
//             success:function(data){
                
//             }   
//         })
// }
function _getSPCActiveReportAttachment(id,attachment){
    if(attachment=="Empty"){
        $("#"+id).html(attachment);
    }
    else{
        var sessionprogram= "{{session('spcprogram')}}";
        var xxx = JSON.stringify('{{ asset("filesdir/kikiboprogram/kikibodownload") }}');
        xxx = xxx.replace("kikiboprogram",sessionprogram);
        xxx = xxx.replace("kikibodownload",attachment);
        var cocosand = '<a download href=' + xxx + ' style="color:blue;">' + attachment + '</a>';
        // alert(cocosand);
        $("#"+id).html(cocosand);
    }
}
$(function(){
    _getSPCActiveReport();
    $("#pills-spcActiveReports-tab").on('click',_getSPCActiveReport);
    // $("#idDownload").on('click',_downloadAttachment);
});
</script>
@endsection