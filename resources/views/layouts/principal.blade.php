<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>CMS | Principal</title>
		<!-- Jquery -->
		<script type="text/javascript" src="{{ asset('bootstrap/js/jquery-3.3.1.js')}}"></script>
		<!-- Bootstrap -->
		<link rel="stylesheet" href="{{ asset('bootstrap/css/main.css')}}"/>
		<link rel="stylesheet" href="{{ asset('bootstrap/css/navbar.css')}}"/>
		<link rel="stylesheet" href="{{ asset('bootstrap/css/simplesidebar.css')}}"/>
		<link rel="stylesheet" href="{{ asset('bootstrap/font-awesome/css/all.css')}}"/>
		<link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.css')}}"/>
		<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.js')}}"></script>
		<!-- CDN  Data Tables-->
		<link rel="stylesheet" href="{{ asset('DataTables/datatables.min.css')}}"/>
		<script type="text/javascript" src="{{ asset('DataTables/datatables.min.js')}}"></script>
		<script type="text/javascript" src="{{ asset('DataTables/adminDashboardManageDP.js')}}"></script>
		<!-- Sweetalert2 -->
		<script type="text/javascript" src="{{ asset('bootstrap/sweetalert-master/dist/sweetalert.min.js')}}"></script>
		<link rel="stylesheet" href="{{ asset('bootstrap/sweetalert-master/dist/sweetalert.css')}}"/>
		<!-- Favicon and touch icons -->
		<link rel="shortcut icon" href="{{ asset ('CMSlogin/ico/favicon-loginlogo.png')}}">
	</head>
	<body>
	<!-- Header && SweetAlert2 -->
	@include('sweetalert::alert')
	@include('components.principalHeader')
	<div class="container">
		<div class="row">
			<div class="col-3">
				<div id="wrapper">
				<!-- Side Navbar -->
					<div id="sidebar-wrapper">
						<ul class="sidebar-nav">
							<!-- .sidebar-brand  for sidebar header-->
							<li class="dropdown drop-down-right" style="margin-top:30px;">
								<button class="dropdown-btn dashboard">Programs <i class="fas fa-angle-down" style="margin-left:15px;"></i></button>
								<div class="dropdown-container">
									{{-- <a class="dropdown-item-move" href="{{route('_divisionPersonnelDashboardManageSPC')}}"><i class="fas fa-edit" style="padding-right:10px;"></i>Manage SPC</a> --}}
                                    @foreach($programs as $data1)
                                        <a class="dropdown-item-move" href="/cms/principal/dashboard/reports/{{$data1['program']}}"><i class="fas fa-copy" style="padding-right:10px;"></i> {{$data1['program']}}</a>
                                    @endforeach 
                                </div>
							</li>
							{{-- <li class="dropdown drop-down-right">
								<button class="dropdown-btn dashboard">Post / Reports <i class="fas fa-angle-down" style="margin-left:15px;"></i></button>
								<div class="dropdown-container">
                                    foreach($dpcprograms as $data1)
										<a class="dropdown-item-move" href="/cms/dp/dashboard/reports/{{$data1}}"><i class="fas fa-copy" style="padding-right:10px;"></i> {{$data1}}</a>
									endforeach 
								</div> --}}
							</li>
						</ul>
					</div>
				<!-- end of side navbar -->
				</div>
			</div>
			<div class="col-9" style="margin-left:250px; margin-top:75px;">
			<!-- Body's Content -->
				@yield('content')
			</div>
		</div>
	</div>
		<script>
			/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
			var dropdown = document.getElementsByClassName("dropdown-btn");
			var i;

			for (i = 0; i < dropdown.length; i++) {
			dropdown[i].addEventListener("click", function() {
			this.classList.toggle("activebg");
			var dropdownContent = this.nextElementSibling;
			if (dropdownContent.style.display === "block") {
			dropdownContent.style.display = "none";
			} else {
			dropdownContent.style.display = "block";
			}
			});
			}
			var dropdown1 = document.getElementsByClassName("dropdown-btn1");
			var i1;

			for (i1 = 0; i1 < dropdown1.length; i1++) {
			dropdown1[i1].addEventListener("click", function() {
			this.classList.toggle("activebg");
			var dropdownContent = this.nextElementSibling;
			if (dropdownContent.style.display === "block") {
			dropdownContent.style.display = "none";
			} else {
			dropdownContent.style.display = "block";
			}
			});
			}
			/* Toggle Menu 
			$(".toggle").click(function(e) {
				e.preventDefault();
				$("#wrapper").toggleClass("toggled");
			});*/
			//Browser Support Code AJAX
			function ajaxFunction() {
				var ajaxRequest;  // The variable that makes Ajax possible!

				try {
				// Opera 8.0+, Firefox, Safari 
				ajaxRequest = new XMLHttpRequest();
				} catch (e) {

				// Internet Explorer Browsers
				try {
					ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
					
					try {
						ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {

						// Something went wrong
						alert("Your browser broke!");
						return false;
						}
					}
				}
			}
		</script>
	</body>
</html>