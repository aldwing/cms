<!-- Professors and Students' Login -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CMS | Login</title>
        <!-- Sweetalert2 -->
        <script type="text/javascript" src="{{ asset('bootstrap/sweetalert-master/dist/sweetalert.min.js')}}"></script>
        <link rel="stylesheet" href="{{ asset('bootstrap/sweetalert-master/dist/sweetalert.css')}}"/>
        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('CMSlogin/css/fontgoogleapisroboto.css')}}">
        <link rel="stylesheet" href="{{ asset('CMSlogin/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{ asset('CMSlogin/font-awesome/css/font-awesome.min.css')}}">
		<link rel="stylesheet" href="{{ asset('CMSlogin/css/form-elements.css')}}">
        <link rel="stylesheet" href="{{ asset('CMSlogin/css/style.css')}}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="{{ asset ('CMSlogin/ico/favicon-loginlogo.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('CMSlogin/ico/apple-touch-icon-144-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('CMSlogin/ico/apple-touch-icon-114-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72"   href="{{ asset('CMSlogin/ico/apple-touch-icon-72-precomposed.png')}}">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('CMSlogin/ico/apple-touch-icon-57-precomposed.png')}}">
    </head>
    <body>
    @include('sweetalert::alert')
        <!-- Top content -->
        <div class="top-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3><b>SIGN IN</b></h3>
                        		</div>
                        		<div class="form-top-right">
                                    <img id="CMSlogo" src="{{asset('CMSlogin/img/loginLogo.png')}}"/>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="{{ route('_loginAdminConfirm')}}" method="post" class="login-form">
                                {!! csrf_field() !!}
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Email</label>
			                        	<input type="email" name="form-username" placeholder="Email" class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="form-password" placeholder="Password" class="form-password form-control" id="form-password">
                                    </div>
                                    <!--<div class="row">
                                        <div class="col-md-12 text-center">
                                            <div class="g-recaptcha" id="recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_KEY') }}" style="transform:scale(0.80);-webkit-transform:scale(0.80);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                                        </div>
                                    </div>-->
                                    <button type="submit" class="btn">Sign in</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Javascript -->
        <script>
            var assetBaseUrl = "{{ asset('') }}";
            window.onload = function() {
                document.getElementById('form-username').value = '';
	            document.getElementById('form-password').value = '';
	        };
        </script>
        <script src="{{ asset('CMSlogin/js/jquery-1.11.1.min.js')}}"></script>
        <script src="{{ asset('CMSlogin/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('CMSlogin/js/jquery.backstretch.min.js')}}"></script>
        <script src="{{ asset('CMSlogin/js/scripts.js')}}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
    </body>
</html>