<?php $__env->startSection('content'); ?>
<style>
.reportdescription {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis; 
}
</style>
<div class="modal-header">
    <!-- h5 class="modal-title" id="exampleModalCenterTitle">Add</h5 -->
    <!-- Links for Modal Content -->
    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-dpcActiveReports-tab" data-toggle="pill" href="#pills-dpcActiveReports" role="tab" aria-controls="pills-dpcActiveReports" aria-selected="true">Active Reports</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-dpcCompliedReports-tab" data-toggle="pill" href="#pills-dpcCompliedReports" role="tab" aria-controls="pills-dpcCompliedReports" aria-selected="false">Complied Reports</a>
        </li>
    </ul>
    <!-- end of Links for Modal Content -->
    <div>
        <a href="/cms/dp/dashboard/reports/<?php echo e(session('dcpprogramcompose')); ?>/compose" id="dpc-SPC-PostReport"><button class="btn btn-primary" style="margin-bottom:10px"><i class="fas fa-plus-square"></i> Compose</button></a>
    </div>
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- Active Reports -->
            <div class="tab-pane fade show active" id="pills-dpcActiveReports" role="tabpanel" aria-labelledby="pills-dpcActiveReports-tab">
               <img src="<?php echo e(asset('CMSlogin/img/loadingGif.gif')); ?>" id="loadingGif" style="display:none;margin:auto;height:121px;width:121px;">
                <table class="table table-striped table-border" id="dataTableActiveReport">
                    <thead>
                        <tr>
                            <th>Mode of Reply</th>
                            <th>Compliance</th>
                            <th>Title</th>
                            <th>Attachment</th>
                            <th>Description</th>
                            <th>Date Posted</th>
                            <th>Deadline</th>
                            <th>File Quantity</th>
                            <th>Level</th>
                            <th>Date Updated</th>
                            <th>Show/Hide</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody id="activeReport">

                    </tbody>
                </table>
            </div>
            <!-- Complied Reports -->
            <div class="tab-pane fade" id="pills-dpcCompliedReports" role="tabpanel" aria-labelledby="pills-dpcCompliedReports-tab">
                <table class="table table-striped table-border adminDashboardManageDP">
                    <thead>
                        <tr>
                            <th>Mode of Reply</th>
                            <th>File Quantity</th>
                            <th>Title</th>
                            <th>Attachment</th>
                            <th class="reportdescription">Description</th>
                            <th>Date Posted</th>
                            <th>Deadline</th>
                            <th>Compliance</th>
                        </tr>
                    </thead>
                    <tbody id="compliedReport">
                        <tr>
                            <td>File</td>
                            <td>3</td>
                            <td><a href="#" style="color:blue;">What is Lorem Ipsum?</a></td>
                            <td></td>
                            <td>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                            <td>2019-03-11</td>
                            <td>2019-04-11</td>
                            <td>0/32</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<!-- Active-Modal ShowHide -->
<div class="modal fade" id="dpc-ActiveReport-ShowHide" tabindex="-1" role="dialog" aria-labelledby="dpc-ActiveReport-ShowHide" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" id="modal-headerShowHide">
                <h5 class="modal-title" id="exampleModalLongTitle">Choose School Level:</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="<?php echo e(route('_divisionPersonnelShowHideReport')); ?>" method="post">
            <?php echo csrf_field(); ?>

                <div id="showradiotext" style="font-weight:bold;font-size:18px;">
                    Are you sure?
                </div>
                <div id="hideallradio">
                    <div>
                        <input type="radio" name="dpcRadioLevel" value="Both" required> Both
                    </div>
                    <div>
                        <input type="radio" name="dpcRadioLevel" value="Primary" required> Primary
                    </div>
                    <div>
                        <input type="radio" name="dpcRadioLevel" value="Secondary" required> Secondary
                    </div>
                </div>
                <input type="hidden" name="new-DPC-ShowHideLevel" class="form-username form-control" id="dpc-SPC-ShowHide">
                <input type="hidden" name="new-DPC-ShowHideID" id="dpc-SPC-ShowHideID">
                <input type="hidden" name="new-DPC-ShowHideStatusShownOrHidden" id="dpc-SPC-ShowHideStatusShownOrHidden">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Submit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<!-- Active-Modal Delete -->
<div class="modal fade" id="dpc-ActiveReport-Delete" tabindex="-1" role="dialog" aria-labelledby="dpc-ActiveReport-Delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="<?php echo e(route('_dpcDeleteActiveReport')); ?>" method="post">
            <?php echo csrf_field(); ?>

                <input type="hidden" name="new-DPC-DeleteID" id="dpc-SPC-DeleteID">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Submit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
function _getActiveReport(){
    $("#loadingGif").css("display","block"); 
    $("#dataTableActiveReport").css("display","none");
    $("#dataTableActiveReport").DataTable().destroy();
    $("#activeReport").html("");
    setTimeout(function(){$.ajax({
            type:"POST",
            url:"<?php echo e(route('_divisionPersonnelGetActiveReport')); ?>",
            data:{_token:"<?php echo csrf_token(); ?>"},
            success:function(data){
                for (i=0; i < data.length; i++){
                    $("#activeReport").append('<tr><td>'
                                              + data[i]["modeofreply"] +'</td><td id="uniqueID_'+i+'"></td><td id="uniqueID1_'+i+'"></td><td>'
                                              + data[i]["attachment"] +'</td><td>'
                                              + data[i]["description"] +'</td><td>'
                                              + data[i]["date_posted"] +'</td><td>'
                                              + data[i]["date_deadline"] +'</td><td>'
                                              + data[i]["filequantity"] +'</td><td>'
                                              + data[i]["level"] +'</td><td>'
                                              + data[i]["date_updated"] +'</td>'
                                              +'<td id="uniqueID3_'+i+'"></td>'
                                              +'<td id="uniqueID2_'+i+'"></td>'
                                              +'<td><a href="#" id="dpc-ActiveReport-ModalDelete" data-dpcdeleteactivereportid="'+data[i]["id"]+'" data-toggle="modal" data-target="#dpc-ActiveReport-Delete"><button class="btn"><i class="fas fa-trash-alt" style="color:red;"></i></button></a></td></tr>');
                                              _getCountSchoolsCompliance("uniqueID_"+i,data[i]["level"]);
                                              _getLevelChangeTitle("uniqueID1_"+i,data[i]["level"],data[i]["title"]);
                                              _getEditEnableDisable("uniqueID2_"+i,data[i]["level"],data[i]["id"]);
                                              _getLevelChangeIconHideShow("uniqueID3_"+i,data[i]["level"],data[i]["id"],data[i]["status_shownorhidden"]);
                }
                $("#loadingGif").css("display","none");
                $("#dataTableActiveReport").css("display","block");
                $("#dataTableActiveReport").DataTable({
                    retrieve: true,
                    responsive: true,
                    ordering: false,
                });
            }
        });},3025)
}
function _getCountSchoolsCompliance(id,level){
    //alert(id+"    "+level);
    var selected_level = level;
        $.ajax({   
            type:"POST",
            url:"<?php echo e(route('_divisionPersonnelShowCountSchoolsCompliance')); ?>",
            data:{_token:"<?php echo csrf_token(); ?>",newDPCLevel:selected_level},
            success:function(data){
                if(level==""){
                    $("#"+id).html("<i class='fas fa-eye-slash' style='color:gray;'></i>");
                }
                else{
                    $("#"+id).html("0 / " + data[0]["count"]);
                }
            }
        });
}
function _getLevelChangeTitle(id,level,title){
    // alert(id+"    "+level+"    "+title);
    if(level==""){
        $("#"+id).html("<i class='fas fa-circle' style='color:gray;font-size:10px;'></i> "+title);
    }
    else{
        $("#"+id).html("<i class='fas fa-circle' style='color:#31B131;font-size:10px;'></i> "+title);
    }
}
function _getEditEnableDisable(id,level,dataid){
    if(level==""){
        $("#"+id).html('<a href="'+"/cms/dp/dashboard/reports/<?php echo e(session('dcpprogramcompose')); ?>/edit/"+dataid+""+'"><button class="btn"><i class="fas fa-user-edit"></i></button></a>');
    }
    else{
        $("#"+id).html('<i class="fas fa-user-edit" style="color:gray;"></i>');
    }
}
function _getLevelChangeIconHideShow(id,level,dataid,status){
    if(level==""){
        $("#"+id).html('<a href="#" id="dpc-ActiveReport-ModalShowHide" data-dpcshowhideactivereportid="'+ dataid +'" data-dpcshowhideactivereportstatusshownorhidden="'+ status +'" data-toggle="modal" data-target="#dpc-ActiveReport-ShowHide"><button class="btn reportShowHide"><i class="fas fa-toggle-off" style="color:#17A2B8;font-size:21px;"></i></button></a>');
    }
    else{
        $("#"+id).html('<a href="#" id="dpc-ActiveReport-ModalShowHide" data-dpcshowhideactivereportid="'+ dataid +'" data-dpcshowhideactivereportstatusshownorhidden="'+ status +'" data-toggle="modal" data-target="#dpc-ActiveReport-ShowHide"><button class="btn reportShowHide"><i class="fas fa-toggle-on" style="color:#17A2B8;font-size:21px;"></i></button></a>');
    }
}
$(function(){
    _getActiveReport();
    $("#pills-dpcActiveReports-tab").on('click',_getActiveReport);
    $("#dataTableActiveReport tbody").on('click','#dpc-ActiveReport-ModalDelete',function(){
        $("#dpc-SPC-DeleteID").val($(this).data("dpcdeleteactivereportid"));
    });
    $("#dataTableActiveReport tbody").on('click','#dpc-ActiveReport-ModalShowHide',function(){
        $("#dpc-SPC-ShowHideID").val($(this).data("dpcshowhideactivereportid"));
        $("#dpc-SPC-ShowHideStatusShownOrHidden").val($(this).data("dpcshowhideactivereportstatusshownorhidden"));
        //if report is shown
        if($("#dpc-SPC-ShowHideStatusShownOrHidden").val()=="Shown"){
            $("#modal-headerShowHide").css("display","none");
            $("#hideallradio").css("display","none");
            $("input[name=dpcRadioLevel]").prop("disabled",true);
        }
        else if($("#dpc-SPC-ShowHideStatusShownOrHidden").val()=="Hidden"){
            $("#hideallradio").css("display","block");
            $("input[name=dpcRadioLevel]").change(function(){
                if($("input[name=dpcRadioLevel]:checked").val() == "Both"){
                    $("#dpc-SPC-ShowHide").val('Primary, Secondary');
                }
                else if($("input[name=dpcRadioLevel]:checked").val() == "Primary"){
                    $("#dpc-SPC-ShowHide").val('Primary');
                }
                else if($("input[name=dpcRadioLevel]:checked").val() == "Secondary"){
                    $("#dpc-SPC-ShowHide").val('Secondary');
                }
            });
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.divisionpersonnel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>