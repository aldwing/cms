<?php $__env->startSection('title'); ?>
CMS | Admin
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<button class="btn btn-primary admin-Office-ModalAdd" style="margin-bottom:10px" data-toggle="modal" data-target="#admin-Office-Add"><i class="fas fa-plus-square"></i> Add</button>
<a href="<?php echo e(route('_adminDashboardManageOfficeArchive')); ?>" id="admin-Office-Archive"><button class="btn btn-info" style="margin-bottom:10px"><i class="fas fa-user-times"></i> View Archive</button></a>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Office</th>
            <th>Edit</th>
            <th>Drop</th>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $adminmiscoffice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($data['office']); ?></td>
            <!-- Button trigger modal for edit-->
            <td><button class="btn admin-Office-ModalEdit" data-officeeditoffice="<?php echo e($data['office']); ?>" data-officeeditid="<?php echo e($data['id']); ?>" data-toggle="modal" data-target="#admin-Office-Edit"><i class="fas fa-user-edit"></i></button></td>
            <td><a href="#" class="admin-Office-ModalDrop" data-officedropid="<?php echo e($data['id']); ?>" data-toggle="modal" data-target="#admin-Office-Drop"><button class="btn"><i class="fas fa-trash-alt" style="color:red;"></i></button></a></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<!-- Modal for add ? -->
<div class="modal fade" id="admin-Office-Add" tabindex="-1" role="dialog" aria-labelledby="admin-Office-Add" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Add Office
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="<?php echo e(route('_adminInsertOffice')); ?>" method="post">
                                <?php echo csrf_field(); ?>

                                <b>Office:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Office</label>
                                    <input type="text" name="new-Office-OfficeName" placeholder="Office" class="form-username form-control" id="admin-Office-OfficeName" pattern="[a-zA-Z0-9\s(),']+" required>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-Office-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Modal for edit ? -->
<div class="modal fade" id="admin-Office-Edit" tabindex="-1" role="dialog" aria-labelledby="admin-Office-Edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Edit Office
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="<?php echo e(route('_adminUpdateOffice')); ?>" method="post">
                                <?php echo csrf_field(); ?>

                                <b>Office:</b> <b id="admin-Office-BoldEdit"></b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Office</label>
                                    <input type="text" name="new-Office-OfficeNameEdit" placeholder="Office" class="form-username form-control" id="admin-Office-OfficeNameEdit" required>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="new-Office-IDEdit" placeholder="Contact Number" class="form-username form-control" id="admin-Office-IDEdit" required>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-Office-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Modal Drop -->
<div class="modal fade" id="admin-Office-Drop" tabindex="-1" role="dialog" aria-labelledby="admin-Office-Drop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="<?php echo e(route('_adminDropOffice')); ?>" method="post">
            <?php echo csrf_field(); ?>

                The Office will be sent to archive.
                <input type="hidden" name="new-Office-DropID" class="form-username form-control" id="admin-Office-DropID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Drop</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".admin-Office-ModalAdd").click(function(){
    $("#admin-Office-OfficeName").val("");
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Office-ModalEdit', function () {
    $("#admin-Office-OfficeNameEdit").val($(this).data("officeeditoffice"));
    $("#admin-Office-IDEdit").val($(this).data("officeeditid"));
    $("#admin-Office-BoldEdit").text('( '+$(this).data("officeeditoffice")+' )');
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Office-ModalDrop', function () {
    $("#admin-Office-DropID").val($(this).data("officedropid"));
})
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>