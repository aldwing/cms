<?php $__env->startSection('title'); ?>
CMS | Admin
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- New Personnel/New Office/ New Program -->
<!-- MODALS -->
<!-- Modal for add ? -->
<div class="modal-header" style="font-weight:bold;">
    Add Division Personnel
    <!-- end of Links for Modal Content -->
</div>
<div class="modal-body">
    <div class="form-bottom">
        <!-- Modal Content -->
        <div class="tab-content" id="pills-tabContent">
            <!-- New Personnel -->
            <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                <form role="form" action="<?php echo e(route('_adminInsertDivisionPersonnel')); ?>" method="post">
                    <?php echo csrf_field(); ?>

                    <b>First Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">First Name</label>
                        <input type="text" name="new-DP-FirstName" placeholder="First Name" class="form-username form-control" id="admin-DP-FirstName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Middle Name:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Middle Name</label>
                        <input type="text" name="new-DP-MiddleName" placeholder="Middle Name" class="form-username form-control" id="admin-DP-MiddleName" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Surname:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Surname</label>
                        <input type="text" name="new-DP-Surname" placeholder="Surname" class="form-username form-control" id="admin-DP-Surname" pattern="[a-zA-Z0-9\s(),']+" required>
                    </div>
                    <b>Email:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Email</label>
                        <input type="email" name="new-DP-Email" placeholder="Email" class="form-username form-control" id="admin-DP-Email" pattern="[a-zA-Z0-9\s(),'@.]+" required>
                    </div>
                    <div id="adminNoOffice" class="alert alert-warning" role="alert">
                        There are no offices, please create an office first. Click the link below!</p>
                        <a href="<?php echo e(route('_adminDashboardManageMiscOffice')); ?>" style="color:blue;"><i class="fas fa-arrow-right"></i> Add new</a>
                    </div>
                    <b>Office:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Office</label>
                        <select name="new-DP-Office" placeholder="Office" class="form-username form-control" id="admin-DP-GetOfficeAssignments">
                            <?php $__currentLoopData = $adminnewoffice; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option id="admin-DP-GetOfficeOption"><?php echo e($data['office']); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                    </div>
                    <!-- Program -->
                    <b>Program:</b>
                    <div class="container">
                        <div class="card">
                            <div class="card-body">
                                <div id="adminNoProgram" class="alert alert-warning" role="alert">
                                    There are no programs, please create a program first. Click the link below!</p>
                                    <a href="<?php echo e(route('_adminDashboardManageMiscProgram')); ?>" style="color:blue;"><i class="fas fa-arrow-right"></i> Add new</a>
                                </div>
                                Active Program/s:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="form-username" placeholder="Program" class="form-username form-control" id="admin-DP-GetPrograms" size="7">
                                        <?php $__currentLoopData = $adminnewprogram; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option id="admin-DP-GetProgramsOption"><?php echo e($data['program']); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div style="text-align:center;padding-bottom:15px;">
                                        <button type="button" class="btn btn-info" id="admin-DP-GetPrograms-Button1">Add</button>
                                </div>
                                Program/s to be added:
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program</label>
                                    <select name="new-DP-Program" placeholder="Program" class="form-username form-control" id="admin-DP-GetPrograms-Transferred" size="7">
                                    </select>
                                </div>
                                <!-- Hidden input -->
                                <div class="form-group">
                                    <input name="new-DP-Programs" type="hidden" id="admin-DP-GetPrograms-Hidden">
                                </div>
                                <!-- end of hidden input -->
                                <div style="text-align:center;">
                                    <button type="button" class="btn btn-danger" id="admin-DP-GetPrograms-Button2">Remove All</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End of Program -->
                    <b>Contact Number:</b>
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Contact Number</label>
                        <input type="text" onkeydown="return event.keyCode !== 69" name="new-DP-ContactNumber" placeholder="Contact Number" class="form-username form-control" id="admin-DP-ContactNumber" pattern="[0-9\s()]+" required>
                    </div>
                    <button type="submit" class="btn btn-primary float-right" id="admin-DP-ApplyA">Submit</button>
                </form>
            </div>
        </div>
        <!-- End of Modal Content -->
    </div>
    <!-- FOOTER
    <div class="modal-footer">
                <button type="button" class="btn btn-primary">ApplyA</button>
    </div> -->
</div>
<!-- end of Modal add -->
<script>
$(function(){
    if($("#admin-DP-GetOfficeOption").html()==""){
        $("#adminNoOffice").show();
        $("#admin-DP-GetOfficeAssignments").attr('disabled', 'disabled');
        $("#admin-DP-ApplyA").attr('disabled', 'disabled');
    }
    else{
        $("#adminNoOffice").hide();
    }
    if($("#admin-DP-GetProgramsOption").html()==""){
        $("#adminNoProgram").show();
        $("#admin-DP-GetPrograms").attr('disabled', 'disabled');
        $("#admin-DP-GetPrograms-Transferred").attr('disabled', 'disabled');
        $("#admin-DP-GetPrograms-Button1").attr('disabled', 'disabled');
        $("#admin-DP-GetPrograms-Button2").attr('disabled', 'disabled');
        $("#admin-DP-ApplyA").attr('disabled', 'disabled');
    }
    else{
        $("#adminNoProgram").hide();
    }
})
/* ADD */
    $("#admin-DP-GetPrograms-Button1").click(function(){
        $("#admin-DP-GetPrograms > option:selected").each(function(){
            $(this).remove().appendTo("#admin-DP-GetPrograms-Transferred");
        });
        /* Separator */
        var selectedval = $("#admin-DP-GetPrograms-Hidden").val();
        selectedval += $("#admin-DP-GetPrograms-Transferred").val() + ", ";
        $("#admin-DP-GetPrograms-Hidden").val(selectedval);
        /* end of separator */
    });
    $("#admin-DP-GetPrograms-Button2").click(function(){
        $('#admin-DP-GetPrograms-Transferred > option').remove().appendTo("#admin-DP-GetPrograms");
        $('#admin-DP-GetPrograms-Hidden').val('');
    });
    /* Remove comma */
    $("#admin-DP-ApplyA").on("click", function(){
        var removedComma = $("#admin-DP-GetPrograms-Hidden").val();
        $('#admin-DP-GetPrograms-Hidden').val(removedComma.substring(0,removedComma.length - 2));
    });
    /* end of remove comma */
/* ADD */
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>