<?php $__env->startSection('title'); ?>
CMS | Admin
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<button class="btn btn-primary admin-Program-ModalAdd" style="margin-bottom:10px" data-toggle="modal" data-target="#admin-Program-Add"><i class="fas fa-plus-square"></i> Add</button>
<a href="<?php echo e(route('_adminDashboardManageProgramArchive')); ?>" id="admin-Program-Archive"><button class="btn btn-info" style="margin-bottom:10px"><i class="fas fa-user-times"></i> View Archive</button></a>
<table class="table table-striped table-border adminDashboardManageDP">
    <thead>
        <tr>
            <th>Program</th>
            <th>Description</th>
            <th>Edit</th>
            <th>Drop</th>
        </tr>
    </thead>
    <tbody>
        <?php $__currentLoopData = $adminmiscprogram; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
            <td><?php echo e($data['program']); ?></td>
            <td><?php echo e($data['description']); ?></td>
            <!-- Button trigger modal for edit-->
            <td><button class="btn admin-Program-ModalEdit" data-programeditprogram="<?php echo e($data['program']); ?>" data-programeditdescription="<?php echo e($data['description']); ?>" data-programeditid="<?php echo e($data['id']); ?>" data-toggle="modal" data-target="#admin-Program-Edit"><i class="fas fa-user-edit"></i></button></td>
            <td><a href="#" class="admin-Program-ModalDrop" data-programdropid="<?php echo e($data['id']); ?>" data-toggle="modal" data-target="#admin-Program-Drop"><button class="btn"><i class="fas fa-trash-alt" style="color:red;"></i></button></a></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<!-- Modal for add ? -->
<div class="modal fade" id="admin-Program-Add" tabindex="-1" role="dialog" aria-labelledby="admin-Program-Add" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Add Program
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="<?php echo e(route('_adminInsertProgram')); ?>" method="post">
                                <?php echo csrf_field(); ?>

                                <b>Program Title:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program Title</label>
                                    <input type="text" name="new-Program-ProgramName" placeholder="Program Title" class="form-username form-control" id="admin-Program-ProgramName" required>
                                </div>
                                <b>Program Description:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program Description</label>
                                    <textarea name="new-Program-ProgramDescription" placeholder="Program Description" class="form-username form-control" id="admin-Program-ProgramDescription" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-Program-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Modal for edit ? -->
<div class="modal fade" id="admin-Program-Edit" tabindex="-1" role="dialog" aria-labelledby="admin-Program-Edit" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="font-weight:bold;">
                Edit Program
                <!-- end of Links for Modal Content -->
            </div>
            <div class="modal-body">
                <div class="form-bottom">
                    <!-- Modal Content -->
                    <div class="tab-content" id="pills-tabContent">
                        <!-- New Personnel -->
                        <div class="tab-pane fade show active" id="pills-adminNewPersonnel" role="tabpanel" aria-labelledby="pills-adminNewPersonnel-tab">
                            <form role="form" action="<?php echo e(route('_adminUpdateProgram')); ?>" method="post">
                                <?php echo csrf_field(); ?>

                                <b>Program Title:</b> <b id="admin-Program-BoldEdit"></b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program Title</label>
                                    <input type="text" name="new-Program-ProgramNameEdit" placeholder="Program" class="form-username form-control" id="admin-Program-ProgramNameEdit" required>
                                </div>
                                <b>Program Description:</b>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Program Description</label>
                                    <textarea name="new-Program-ProgramDescriptionEdit" placeholder="Program Description" class="form-username form-control" id="admin-Program-ProgramDescriptionEdit" required></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="new-Program-IDEdit" placeholder="Contact Number" class="form-username form-control" id="admin-Program-IDEdit" required>
                                </div>
                                <button type="submit" class="btn btn-primary float-right" id="admin-Program-ApplyA">Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- End of Modal Content -->
                </div>
                <!-- FOOTER
                <div class="modal-footer">
                            <button type="button" class="btn btn-primary">ApplyA</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- Modal Drop -->
<div class="modal fade" id="admin-Program-Drop" tabindex="-1" role="dialog" aria-labelledby="admin-Program-Drop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Are you sure?</h5>
            </div>
            <div class="modal-body">
            <form role="form" action="<?php echo e(route('_adminDropProgram')); ?>" method="post">
            <?php echo csrf_field(); ?>

                The Program will be sent to archive.
                <input type="hidden" name="new-Program-DropID" class="form-username form-control" id="admin-Program-DropID" required>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning">Drop</button>
            </form>
            </div>
        </div>
    </div>
</div>
<script>
$(".admin-Program-ModalAdd").click(function(){
    $("#admin-Program-ProgramName").val("");
    $("#admin-Program-ProgramDescription").val("");
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Program-ModalEdit', function () {
    $("#admin-Program-ProgramNameEdit").val($(this).data("programeditprogram"));
    $("#admin-Program-ProgramDescriptionEdit").val($(this).data("programeditdescription"));
    $("#admin-Program-IDEdit").val($(this).data("programeditid"));
    $("#admin-Program-BoldEdit").text('( '+$(this).data("programeditprogram")+' )');
})
$(".adminDashboardManageDP tbody").on('click', '.admin-Program-ModalDrop', function () {
    $("#admin-Program-DropID").val($(this).data("programdropid"));
})
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>