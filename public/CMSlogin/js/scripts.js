
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    $.backstretch([
        assetBaseUrl + "CMSlogin/img/backgrounds/bglaptop.jpg"
    ]);
    
    /*
        Form validation
    */
    $('.login-form input[type="email"], .login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('.login-form').on('submit', function(e) {
    	
    	$(this).find('input[type="email"], input[type="text"], input[type="password"], textarea').each(function(){
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });

    $(".login-form").submit(function(event) {

        var recaptcha = $("#g-recaptcha-response").val();
        if (recaptcha === "") {
           event.preventDefault();
           swal({type:'error',
           title:'Error',
           text:'Please check the reCAPTCHA'});
        }
     });
    
    
});

$(function() {
    $('.login-form input[type="email"], .login-form input[type="text"], .login-form input[type="password"]').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });
});
