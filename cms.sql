-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2019 at 11:58 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `office`
--

CREATE TABLE `office` (
  `id` int(11) NOT NULL,
  `office` varchar(255) NOT NULL,
  `office_status` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office`
--

INSERT INTO `office` (`id`, `office`, `office_status`, `timestamp`) VALUES
(1, 'Schools Division Office', 'Active', '2019-03-11 03:09:26'),
(10, 'asdasdasd asdasd', 'Inactive', '2019-04-11 03:37:52'),
(11, 'asdasda123123123', 'Inactive', '2019-04-11 03:38:27'),
(12, 'asdadasasdasdasd \'\'\'\'asdasdasd()(),,,,,,\'\'\'\'\'\'\'\'\'\'\'\'\'\'\'', 'Inactive', '2019-04-11 03:38:52');

-- --------------------------------------------------------

--
-- Table structure for table `postreport`
--

CREATE TABLE `postreport` (
  `id` int(11) NOT NULL,
  `dpc_name` varchar(255) NOT NULL,
  `modeofreply` varchar(255) NOT NULL,
  `filequantity` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_deadline` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `compliance` varchar(255) NOT NULL,
  `program` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL,
  `status_activeorcomplied` varchar(255) NOT NULL,
  `status_shownorhidden` varchar(255) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `postreport`
--

INSERT INTO `postreport` (`id`, `dpc_name`, `modeofreply`, `filequantity`, `title`, `attachment`, `description`, `date_posted`, `date_deadline`, `compliance`, `program`, `level`, `status_activeorcomplied`, `status_shownorhidden`, `date_updated`) VALUES
(2, 'Claire Redfield', 'File', '3', 'samplesample', 'DPC-20190424162510.docx', '123123', '2019-04-24 08:25:23', '2019-04-23 16:00:00', '', 'ICT', 'Primary', 'Active', 'Shown', '0000-00-00 00:00:00'),
(3, 'Claire Redfield', 'Text', 'Empty', 'Xamppfall', 'DPC-20190426133122.docx', 'Sample', '2019-04-26 05:48:19', '2019-04-25 16:00:00', '', 'ICT', 'Primary, Secondary', 'Active', 'Shown', '0000-00-00 00:00:00'),
(4, 'Claire Redfield', 'Url', 'Empty', 'TestPost', 'Empty', 'This is a test.', '2019-05-02 01:24:15', '2019-05-02 16:00:00', '', 'ICT', 'Primary, Secondary', 'Active', 'Shown', '0000-00-00 00:00:00'),
(5, 'Claire Redfield', 'File', '1', 'Updated Title1', 'DPC-20190506130805.pptx', 'Updated Description1', '2019-05-06 05:09:50', '2019-05-08 16:00:00', '', 'ICT', 'Primary', 'Active', 'Shown', '2019-05-09 20:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `postreport_remark`
--

CREATE TABLE `postreport_remark` (
  `id` int(11) NOT NULL,
  `postreport_id` int(11) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `date_posted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `program` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `program_status` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `program`, `description`, `program_status`, `timestamp`) VALUES
(10, 'Sample Program', 'Sample', 'Inactive', '2019-04-11 00:31:50'),
(11, 'Brigada', 'Brigada Description', 'Inactive', '2019-04-11 00:32:52'),
(12, 'ICT', 'Information and Communications Technology', 'Active', '2019-04-11 02:11:07'),
(13, 'ALS', 'Alternative Learning System', 'Active', '2019-04-11 02:11:53'),
(14, 'SBM', 'School-Based Management', 'Active', '2019-04-11 02:15:35'),
(15, 'Sample', 'Sample Description', 'Active', '2019-05-06 04:54:12');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `contact_number` varchar(155) NOT NULL,
  `level` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`id`, `name`, `address`, `contact_number`, `level`, `status`) VALUES
(1, 'Barangka National High School', '119 A. Bonifacio Ave, Marikina, 1800 Metro Manila', '(02) 621 5831', 'Secondary', 'Active'),
(3, 'Santa Elena High School', '226 E dela Paz St, Marikina, 1800 Metro Manila', '(02) 646 9793', 'Secondary', 'Active'),
(4, 'sampleschool1', 'sampleschool 123123 address1', '(02) 123 12341', 'Primary', 'Inactive'),
(5, 'Barangka Elementary School', 'General Julian Cruz St, Marikina, 1803 Metro Manila', '(02) 942 0069', 'Primary', 'Active'),
(6, 'Leodegario Victorino Elementary School', '332 A. Bonifacio Ave, Marikina, 1804 Metro Manila', '(02) 642 0019', 'Primary', 'Active'),
(7, 'Sample SchoolName', 'Sample Address', '(123456789)', 'Secondary', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `sysreport`
--

CREATE TABLE `sysreport` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `actiontype` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sysreport`
--

INSERT INTO `sysreport` (`id`, `email`, `actiontype`, `timestamp`) VALUES
(1, 'aldwincpagkatipunan@yahoo.com', 'Logged in', '2019-04-16 06:32:37'),
(2, 'aldwincpagkatipunan@yahoo.com', 'Logged out', '2019-04-16 06:32:53'),
(3, 'claireredfield@yahoo.com', 'Logged in', '2019-04-16 06:33:02'),
(4, 'claireredfield@yahoo.com', 'Logged out', '2019-04-16 08:57:51'),
(5, 'claireredfield@yahoo.com', 'Logged in', '2019-04-20 13:03:05'),
(6, 'claireredfield@yahoo.com', 'Logged out', '2019-04-20 14:28:41'),
(7, 'aldwincpagkatipunan@yahoo.com', 'Logged in', '2019-04-20 14:28:51'),
(8, 'aldwincpagkatipunan@yahoo.com', 'Logged out', '2019-04-20 14:29:18'),
(9, 'claireredfield@yahoo.com', 'Logged in', '2019-04-20 14:29:27'),
(10, 'claireredfield@yahoo.com', 'Logged out', '2019-04-20 14:50:12'),
(11, 'leonskennedy@yahoo.com', 'Logged in', '2019-04-20 14:50:22'),
(12, 'leonskennedy@yahoo.com', 'Logged out', '2019-04-20 14:51:47'),
(13, 'claireredfield@yahoo.com', 'Logged in', '2019-04-20 14:51:55'),
(14, 'claireredfield@yahoo.com', 'Logged in', '2019-04-22 00:08:32'),
(15, 'claireredfield@yahoo.com', 'Logged in', '2019-04-22 03:42:00'),
(16, 'claireredfield@yahoo.com', 'Logged in', '2019-04-22 07:10:34'),
(17, 'claireredfield@yahoo.com', 'Logged in', '2019-04-24 00:26:44'),
(18, 'claireredfield@yahoo.com', 'Logged in', '2019-04-24 10:23:45'),
(19, 'claireredfield@yahoo.com', 'Logged in', '2019-04-26 01:07:41'),
(20, 'claireredfield@yahoo.com', 'Logged out', '2019-04-26 02:21:25'),
(21, 'claireredfield@yahoo.com', 'Logged in', '2019-04-26 02:21:34'),
(22, 'claireredfield@yahoo.com', 'Logged in', '2019-04-26 13:04:07'),
(23, 'claireredfield@yahoo.com', 'Logged in', '2019-04-30 00:54:26'),
(24, 'samplesp@yahoo.com', 'Logged in', '2019-04-30 01:52:20'),
(25, 'claireredfield@yahoo.com', 'Restored a School Personnel', '2019-04-30 04:10:16'),
(26, 'claireredfield@yahoo.com', 'Updated a School Personnel', '2019-04-30 04:21:25'),
(27, 'claireredfield@yahoo.com', 'Updated a School Personnel', '2019-04-30 04:21:46'),
(28, 'aldwincpagkatipunan@yahoo.com', 'Logged in', '2019-04-30 04:58:11'),
(29, 'aldwincpagkatipunan@yahoo.com', 'Reset a Division Personnel', '2019-04-30 04:58:17'),
(30, 'aldwincpagkatipunan@yahoo.com', 'Logged out', '2019-04-30 04:58:21'),
(31, 'adawong1@yahoo.com', 'Logged in', '2019-04-30 04:58:26'),
(32, 'claireredfield@yahoo.com', 'Dropped a School Personnel', '2019-04-30 06:42:01'),
(33, 'claireredfield@yahoo.com', 'Logged in', '2019-04-30 08:06:20'),
(34, 'claireredfield@yahoo.com', 'Logged in', '2019-04-30 23:49:01'),
(35, 'claireredfield@yahoo.com', 'Logged in', '2019-05-02 00:38:38'),
(36, 'claireredfield@yahoo.com', 'Created a Post/Report', '2019-05-02 01:24:15'),
(37, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-02 01:24:42'),
(38, 'claireredfield@yahoo.com', 'Inserted a School Personnel', '2019-05-02 01:31:56'),
(39, 'claireredfield@yahoo.com', 'Inserted a School Personnel', '2019-05-02 01:47:04'),
(40, 'leonskennedy@yahoo.com', 'Logged in', '2019-05-02 01:51:02'),
(41, 'claireredfield@yahoo.com', 'Logged in', '2019-05-02 04:08:36'),
(42, 'claireredfield@yahoo.com', 'Logged in', '2019-05-02 04:22:50'),
(43, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 02:25:30'),
(44, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 02:28:02'),
(45, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 04:55:07'),
(46, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 04:57:05'),
(47, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 05:48:48'),
(48, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 05:49:54'),
(49, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 05:50:42'),
(50, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 05:51:15'),
(51, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 05:51:45'),
(52, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 05:52:16'),
(53, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 06:16:44'),
(54, 'leonskennedy@yahoo.com', 'Logged in', '2019-05-03 06:22:55'),
(55, 'leonskennedy@yahoo.com', 'Logged out', '2019-05-03 06:23:41'),
(56, 'adawong1@yahoo.com', 'Logged in', '2019-05-03 06:23:44'),
(57, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 06:30:30'),
(58, 'claireredfield@yahoo.com', 'Logged out', '2019-05-03 06:30:37'),
(59, 'leonskennedy@yahoo.com', 'Logged in', '2019-05-03 06:30:48'),
(60, 'leonskennedy@yahoo.com', 'Logged out', '2019-05-03 06:30:56'),
(61, 'claireredfield@yahoo.com', 'Logged out', '2019-05-03 06:43:59'),
(62, 'claireredfield@yahoo.com', 'Logged in', '2019-05-03 06:44:07'),
(63, 'claireredfield@yahoo.com', 'Dropped a School Personnel', '2019-05-03 07:14:26'),
(64, 'claireredfield@yahoo.com', 'Restored a School Personnel', '2019-05-03 07:14:36'),
(65, 'claireredfield@yahoo.com', 'Dropped a School Personnel', '2019-05-03 07:18:08'),
(66, 'claireredfield@yahoo.com', 'Restored a School Personnel', '2019-05-03 07:19:10'),
(67, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 00:53:36'),
(68, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 01:35:36'),
(69, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 04:45:01'),
(70, 'claireredfield@yahoo.com', 'Logged out', '2019-05-06 04:52:28'),
(71, 'aldwincpagkatipunan@yahoo.com', 'Logged in', '2019-05-06 04:53:07'),
(72, 'aldwincpagkatipunan@yahoo.com', 'Inserted a Program', '2019-05-06 04:54:12'),
(73, 'aldwincpagkatipunan@yahoo.com', 'Inserted a School', '2019-05-06 04:58:01'),
(74, 'aldwincpagkatipunan@yahoo.com', 'Dropped a Principal', '2019-05-06 04:59:31'),
(75, 'aldwincpagkatipunan@yahoo.com', 'Restored a Principal', '2019-05-06 05:00:02'),
(76, 'aldwincpagkatipunan@yahoo.com', 'Inserted a Division Personnel', '2019-05-06 05:02:10'),
(77, 'leonskennedy@yahoo.com', 'Logged in', '2019-05-06 05:03:14'),
(78, 'aldwincpagkatipunan@yahoo.com', 'Logged out', '2019-05-06 05:03:18'),
(79, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 05:03:23'),
(80, 'claireredfield@yahoo.com', 'Inserted a School Personnel', '2019-05-06 05:04:51'),
(81, 'claireredfield@yahoo.com', 'Created a Post/Report', '2019-05-06 05:09:50'),
(82, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-06 05:11:43'),
(83, 'claireredfield@yahoo.com', 'Hidden an Active Report', '2019-05-06 05:12:26'),
(84, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-06 05:12:47'),
(85, 'leonskennedy@yahoo.com', 'Logged out', '2019-05-06 05:16:46'),
(86, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 05:17:00'),
(87, 'claireredfield@yahoo.com', 'Logged out', '2019-05-06 05:17:11'),
(88, 'claireredfield@yahoo.com', 'Logged out', '2019-05-06 05:17:20'),
(89, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 05:17:42'),
(90, 'claireredfield@yahoo.com', 'Logged out', '2019-05-06 05:42:23'),
(91, 'samplesp@yahoo.com', 'Logged in', '2019-05-06 05:42:39'),
(92, 'samplesp@yahoo.com', 'Logged out', '2019-05-06 05:45:32'),
(93, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 05:45:36'),
(94, 'claireredfield@yahoo.com', 'Logged out', '2019-05-06 05:45:41'),
(95, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 05:48:43'),
(96, 'claireredfield@yahoo.com', 'Logged in', '2019-05-06 06:40:42'),
(97, 'claireredfield@yahoo.com', 'Logged in', '2019-05-07 02:42:11'),
(98, 'claireredfield@yahoo.com', 'Logged in', '2019-05-07 03:09:06'),
(99, 'leonskennedy@yahoo.com', 'Logged in', '2019-05-07 03:20:14'),
(100, 'claireredfield@yahoo.com', 'Logged in', '2019-05-07 10:58:09'),
(101, 'claireredfield@yahoo.com', 'Logged in', '2019-05-08 01:38:18'),
(102, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-08 01:38:51'),
(103, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-08 01:39:08'),
(104, 'claireredfield@yahoo.com', 'Hidden an Active Report', '2019-05-08 01:43:12'),
(105, 'claireredfield@yahoo.com', 'Hidden an Active Report', '2019-05-08 05:23:12'),
(106, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-08 05:23:26'),
(107, 'claireredfield@yahoo.com', 'Deleted a Post/Report', '2019-05-08 07:04:45'),
(108, 'claireredfield@yahoo.com', 'Logged out', '2019-05-08 07:28:02'),
(109, 'claireredfield@yahoo.com', 'Logged in', '2019-05-08 07:35:55'),
(110, 'claireredfield@yahoo.com', 'Logged out', '2019-05-08 08:27:26'),
(111, 'claireredfield@yahoo.com', 'Logged in', '2019-05-08 13:23:01'),
(112, 'claireredfield@yahoo.com', 'Logged in', '2019-05-08 21:28:58'),
(113, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-08 21:43:57'),
(114, 'claireredfield@yahoo.com', 'Hidden an Active Report', '2019-05-08 21:45:10'),
(115, 'claireredfield@yahoo.com', 'Logged out', '2019-05-08 22:25:28'),
(116, 'claireredfield@yahoo.com', 'Logged in', '2019-05-09 07:06:54'),
(117, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-09 09:04:46'),
(118, 'claireredfield@yahoo.com', 'Logged in', '2019-05-09 12:20:48'),
(119, 'claireredfield@yahoo.com', 'Hidden an Active Report', '2019-05-09 12:31:27'),
(120, 'claireredfield@yahoo.com', 'Logged in', '2019-05-09 18:55:52'),
(121, 'claireredfield@yahoo.com', 'Updated a Post/Report', '2019-05-09 20:25:11'),
(122, 'claireredfield@yahoo.com', 'Updated a Post/Report', '2019-05-09 20:32:47'),
(123, 'claireredfield@yahoo.com', 'Updated a Post/Report', '2019-05-09 20:36:37'),
(124, 'claireredfield@yahoo.com', 'Logged in', '2019-05-09 23:37:23'),
(125, 'claireredfield@yahoo.com', 'Shown an Active Report', '2019-05-09 23:38:36'),
(126, 'claireredfield@yahoo.com', 'Logged out', '2019-05-09 23:39:36'),
(127, 'samplemarkezuckerberg@yahoo.com', 'Logged in', '2019-05-09 23:42:14'),
(128, 'samplemarkezuckerberg@yahoo.com', 'Logged out', '2019-05-10 00:04:46'),
(129, 'sample1@yahoo.com', 'Logged in', '2019-05-10 00:14:47'),
(130, 'sample1@yahoo.com', 'Logged out', '2019-05-10 00:15:01'),
(131, 'sample1@yahoo.com', 'Logged in', '2019-05-10 01:45:07');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `office` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `program` varchar(255) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `middle_name`, `surname`, `email`, `password`, `office`, `role`, `program`, `contact_number`, `status`) VALUES
(1, 'Aldwin', 'C.', 'Pagkatipunan', 'aldwincpagkatipunan@yahoo.com', '277f58a6a50728bd8c3c26286023a61fe803890d', 'Schools Division Office', 'Admin', 'ICT', '01234567890', 'Active'),
(7, 'SampleSP', 'SP', 'SPSample', 'samplesp@yahoo.com', '10e3151b0f08f63a597fab1c6175861aee584bab', 'Barangka National High School', 'School Personnel', 'ICT', '01233456789', 'Active'),
(8, 'Claire', 'CR', 'Redfield', 'claireredfield@yahoo.com', 'c30d8f53f80d4cd44dedead58eaf5b03c1425d62', 'Schools Division Office', 'Division Personnel', 'ICT, ALS', '01223456789', 'Active'),
(9, 'Leon', 'S', 'Kennedy', 'leonskennedy@yahoo.com', '0c15a01f194c8efabd9e3578d44e341b0574f440', 'Schools Division Office', 'Division Personnel', 'ICT, SBM', '01234556789', 'Active'),
(64, 'Ada1', 'AW1', 'Wong1', 'adawong1@yahoo.com', '7ff39ea02568c79c319bd00af6a460d8b5e368dc', 'Schools Division Office', 'Division Personnel', 'ICT', '+631234567889', 'Active'),
(69, 'Steve', 'P', 'Jobs', 'samplestevepjobs@yahoo.com', 'eaba088b143c7cc6249cc3d5526b91a78de00bea', 'Barangka National High School', 'Principal', '', '01233456789', 'Active'),
(73, 'samplespc', 'spc', 'samplespc', 'samplespc@yahoo.com', 'e932ead2929f17103d7b178f076026606502c891', 'Barangka National High School', 'School Personnel', 'ALS', '1234123455', 'Active'),
(74, 'Mark', 'E', 'Zuckerberg', 'samplemarkezuckerberg@yahoo.com', '5259b91cfdf84544067ddb392554cd9e07a8e97a', 'Leodegario Victorino Elementary School', 'Principal', '', '+63123456789', 'Active'),
(76, 'Test', 'T', 'Test', 'testttest@yahoo.com', '20c40b72b2e878988ef900e48483fd6fe2bcfcb7', 'Barangka National High School', 'School Personnel', 'ALS, SBM', '1234567890', 'Inactive'),
(77, 'Sample1', 'Sample1', 'Sample1', 'sample1@yahoo.com', 'b8a06ed7954bfcac85b40567e56ba09ce0605001', 'Santa Elena High School', 'School Personnel', 'ICT, ALS, SBM', '123456789', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postreport`
--
ALTER TABLE `postreport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `postreport_remark`
--
ALTER TABLE `postreport_remark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `sysreport`
--
ALTER TABLE `sysreport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `office`
--
ALTER TABLE `office`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `postreport`
--
ALTER TABLE `postreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `postreport_remark`
--
ALTER TABLE `postreport_remark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sysreport`
--
ALTER TABLE `sysreport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
