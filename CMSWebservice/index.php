<?php
    include 'includes/adminCMS.php';
    include 'includes/dpcCMS.php';
    include 'includes/principalCMS.php';
    include 'includes/spcCMS.php';
    if(isset($_POST['tag']) && $_POST['tag']!=''){
        $tag=$_POST['tag'];
        $response=array("tag"=>$tag, "success"=>0, "error"=>0);
        $dbadminCMS = new adminCMS;
        $dbadminCMSSysReport = new adminCMSSysReport;
        $dbdpcCMS = new dpcCMS;
        $dbdpcCMSSysReport = new dpcCMSSysReport;
        $dbprincipalCMS = new principalCMS;
        $dbspcCMS = new spcCMS;
        /* ADMIN */
        if($tag=="getlogin"){
            $email=$_POST['email'];
            $password=$_POST['password'];
            $result=$dbadminCMS->_cmsLogin($email, $password);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                $response["email"]=$email;
                $arrayobj = array_merge($response,$result);
                $result1=$dbadminCMSSysReport->_getLoginSysReport($email);
                echo json_encode($arrayobj);
            }else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        /* Manage Division Personnel */
        else if($tag=="getlogout"){
            $email=$_POST['email'];
            $result=$dbadminCMSSysReport->_getLogoutSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted";
                $response["email"]=$email;
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["msg"]="Error Inserting";
                echo json_encode($response);
            }
        }
        else if($tag=="getdp"){
            $result=$dbadminCMS->_cmsGetDP();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getdroppeddp"){
            $result=$dbadminCMS->_cmsGetDroppedDP();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getspecificdp"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsGetSpecificDP($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="deletedp"){
            $email=$_POST["email"];
            $result=$dbadminCMS->_cmsDeleteDP($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Deleted";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Deleted";
                echo json_encode($response);
            }
        }
        else if($tag=="deletedivisionpersonnelsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardDeleteSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoredp"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsRestoreDP($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Restored";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Restoring";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoredivisionpersonnelsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardRestoreSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getofficeassignments"){
            $result=$dbadminCMS->_cmsGetOfficeAssignments();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["office"]="";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getprograms"){
            $result=$dbadminCMS->_cmsGetPrograms();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["program"]="";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertdivisionpersonnel"){
            $first_name=$_POST["first_name"];
            $middle_name=$_POST["middle_name"];
            $surname=$_POST["surname"];
            $email=$_POST["email"];
            $office=$_POST["office"];
            $program=$_POST["program"];
            $contact_number=$_POST["contact_number"];
            $result=$dbadminCMS->_cmsInsertDivisionPersonnel($first_name,$middle_name,$surname,$email,$office,$program,$contact_number);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertdivisionpersonnelsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardInsertSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updatedivisionpersonnel"){
            $first_name=$_POST["first_name"];
            $middle_name=$_POST["middle_name"];
            $surname=$_POST["surname"];
            $email=$_POST["email"];
            $office=$_POST["office"];
            $program=$_POST["program"];
            $contact_number=$_POST["contact_number"];
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsUpdateDivisionPersonnel($first_name,$middle_name,$surname,$email,$office,$program,$contact_number,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Updated";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Updating";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updatedivisionpersonnelsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardUpdateSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="resetdp"){
            $email=$_POST["email"];
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsResetDivisionPersonnel($email,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Reset";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Reset";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="resetdivisionpersonnelsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardResetSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropdivisionpersonnel"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsDropDivisionPersonnel($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Dropped";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Dropping";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropdivisionpersonnelsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardDropSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        /* End of managed division personnel */
        /* Manage School */
        else if($tag=="getschool"){
            $result=$dbadminCMS->_cmsGetSchool();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertschool"){
            $name=$_POST["name"];
            $address=$_POST["address"];
            $contact_number=$_POST["contact_number"];
            $level=$_POST["level"];
            $result=$dbadminCMS->_cmsInsertSchool($name,$address,$contact_number,$level);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertschoolsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardSchoolInsertSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateschool"){
            $name=$_POST["name"];
            $address=$_POST["address"];
            $contact_number=$_POST["contact_number"];
            $level=$_POST["level"];
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsUpdateSchool($name,$address,$contact_number,$level,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Updated";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Updating";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateschoolsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardSchoolUpdateSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropschool"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsDropSchool($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Dropped";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Dropping";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropschoolsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardSchoolDropSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getdroppedschool"){
            $result=$dbadminCMS->_cmsGetDroppedSchool();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreschool"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsRestoreSchool($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Restored";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Restoring";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreschoolsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardSchoolRestoreSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="deleteschool"){
            $name=$_POST["name"];
            $result=$dbadminCMS->_cmsDeleteSchool($name);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Deleted";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Deleted";
                echo json_encode($response);
            }
        }
        else if($tag=="deleteschoolsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardSchoolDeleteSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        /* End of manage school */
        /* Manage Principal */
        else if($tag=="getprincipal"){
            $result=$dbadminCMS->_cmsGetPrincipal();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getprincipalschool"){
            $result=$dbadminCMS->_cmsGetPrincipalSchool();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["name"]="";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertprincipal"){
            $first_name=$_POST["first_name"];
            $middle_name=$_POST["middle_name"];
            $surname=$_POST["surname"];
            $email=$_POST["email"];
            $office=$_POST["office"];
            $contact_number=$_POST["contact_number"];
            $result=$dbadminCMS->_cmsInsertPrincipal($first_name,$middle_name,$surname,$email,$office,$contact_number);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertprincipalsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardPrincipalInsertSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateprincipal"){
            $first_name=$_POST["first_name"];
            $middle_name=$_POST["middle_name"];
            $surname=$_POST["surname"];
            $email=$_POST["email"];
            $office=$_POST["office"];
            $contact_number=$_POST["contact_number"];
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsUpdatePrincipal($first_name,$middle_name,$surname,$email,$office,$contact_number,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Updated";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Updating";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateprincipalsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardPrincipalUpdateSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="resetprincipal"){
            $email=$_POST["email"];
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsResetPrincipal($email,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Reset";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Reset";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="resetprincipalsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardPrincipalResetSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropprincipal"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsDropPrincipal($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Dropped";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Dropping";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropprincipalsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardPrincipalDropSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getdroppedprincipal"){
            $result=$dbadminCMS->_cmsGetDroppedPrincipal();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreprincipal"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsRestorePrincipal($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Restored";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Restoring";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreprincipalsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardPrincipalRestoreSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="deleteprincipal"){
            $email=$_POST["email"];
            $result=$dbadminCMS->_cmsDeletePrincipal($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Deleted";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Deleted";
                echo json_encode($response);
            }
        }
        else if($tag=="deleteprincipalsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardPrincipalDeleteSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        /* End of manage principal */
        /* ------------------------------ Miscellaneous ------------------------------ */
        /* ---------- Manage Office ---------- */
        else if($tag=="getmiscoffice"){
            $result=$dbadminCMS->_cmsGetMiscOffice();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["office_status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertoffice"){
            $office=$_POST["office"];
            $result=$dbadminCMS->_cmsInsertOffice($office);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertofficesysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardOfficeInsertSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateoffice"){
            $office=$_POST["office"];
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsUpdateOffice($office,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Updated";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Updating";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateofficesysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardOfficeUpdateSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropoffice"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsDropOffice($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Dropped";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Dropping";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropofficesysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardOfficeDropSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getdroppedoffice"){
            $result=$dbadminCMS->_cmsGetDroppedOffice();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["office_status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreoffice"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsRestoreOffice($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Restored";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Restoring";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreofficesysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardOfficeRestoreSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="deleteoffice"){
            $office=$_POST["office"];
            $result=$dbadminCMS->_cmsDeleteOffice($office);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Deleted";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Deleted";
                echo json_encode($response);
            }
        }
        else if($tag=="deleteofficesysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardOfficeDeleteSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        /* ---------- End of manage office ---------- */
        /* ---------- Manage Program ---------- */
        else if($tag=="getmiscprogram"){
            $result=$dbadminCMS->_cmsGetMiscProgram();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["program_status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertprogram"){
            $program=$_POST["program"];
            $description=$_POST["description"];
            $result=$dbadminCMS->_cmsInsertProgram($program,$description);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertprogramsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardProgramInsertSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateprogram"){
            $program=$_POST["program"];
            $description=$_POST["description"];
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsUpdateProgram($program,$description,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Updated";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Updating";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updateprogramsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardProgramUpdateSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropprogram"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsDropProgram($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Dropped";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Dropping";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropprogramsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardProgramDropSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getdroppedprogram"){
            $result=$dbadminCMS->_cmsGetDroppedProgram();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["program_status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreprogram"){
            $id=$_POST["id"];
            $result=$dbadminCMS->_cmsRestoreProgram($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Restored";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Restoring";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restoreprogramsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardProgramRestoreSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="deleteprogram"){
            $program=$_POST["program"];
            $result=$dbadminCMS->_cmsDeleteProgram($program);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Deleted";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Deleted";
                echo json_encode($response);
            }
        }
        else if($tag=="deleteprogramsysreport"){
            $email=$_POST["email"];
            $result=$dbadminCMSSysReport->_getAdminDashboardProgramDeleteSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        /* ---------- End of manage program ---------- */
        /* ------------------------------ End of miscellaneous ------------------------------ */
        /* ADMIN */
        /* DPC */
        else if($tag=="dpcgetspc"){
            $email=$_POST["email"];
            $result=$dbdpcCMS->_dpcGetSPC($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getcompareprogramdpc"){
            $email=$_POST["email"];
            $office=$_POST["office"];
            $result=$dbdpcCMS->_dpcGetCompareProgramSPC($email,$office);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        else if($tag=="insertspc"){
            $first_name=$_POST["first_name"];
            $middle_name=$_POST["middle_name"];
            $surname=$_POST["surname"];
            $email=$_POST["email"];
            $office=$_POST["office"];
            $program=$_POST["program"];
            $contact_number=$_POST["contact_number"];
            $result=$dbdpcCMS->_dpcInsertSPC($first_name,$middle_name,$surname,$email,$office,$program,$contact_number);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="insertspcsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCInsertSPCSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dpcgetspecificspc"){
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcGetSpecificSPC($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updatespc"){
            $first_name=$_POST["first_name"];
            $middle_name=$_POST["middle_name"];
            $surname=$_POST["surname"];
            $email=$_POST["email"];
            $office=$_POST["office"];
            $program=$_POST["program"];
            $contact_number=$_POST["contact_number"];
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcUpdateSPC($first_name,$middle_name,$surname,$email,$office,$program,$contact_number,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Updated";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Updating";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="updatespcsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCUpdateSPCSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="resetspc"){
            $email=$_POST["email"];
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcResetSPC($email,$id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Reset";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Reset";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="resetspcsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCResetSPCSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropspc"){
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcDropSPC($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Dropped";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Dropping";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dropspcsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCDropSPCSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getdroppedspc"){
            $email=$_POST["email"];
            $result=$dbdpcCMS->_dpcGetDroppedSPC($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="deletespc"){
            $email=$_POST["email"];
            $result=$dbdpcCMS->_dpcDeleteSPC($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Deleted";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Deleted";
                echo json_encode($response);
            }
        }
        else if($tag=="deletespcsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCDeleteSPCSysReport($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restorespc"){
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcRestoreSPC($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Restored";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["status"]="Error Restoring";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="restorespcsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCRestoreSPCSysReport($email);
            if($result){ 
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getprogramdpc"){
            $email=$_POST["email"];
            $result=$dbdpcCMS->_dpcGetProgramSPC($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selected";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Selecting";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="dpccreatepostreport"){
            $dpc_name=$_POST["dpc_name"];
            $modeofreply=$_POST["modeofreply"];
            $filequantity=$_POST["filequantity"];
            $title=$_POST["title"];
            $attachment=$_POST["attachment"];
            $description=$_POST["description"];
            $date_deadline=$_POST["date_deadline"];
            $program=$_POST["program"];
            $result=$dbdpcCMS->_dpcCreatePostReport($dpc_name,$modeofreply,$filequantity,$title,$attachment,$description,$date_deadline,$program);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Inserted";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["msg"]="Error Inserting";
                echo json_encode($response);
            }
        }
        else if($tag=="createpostreportsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCCreatePostReportSysReport($email);
            if($result){ 
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getactivereport"){
            $program=$_POST["program"];
            $result=$dbdpcCMS->_dpcGetActiveReport($program);
            if($result){ 
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        else if($tag=="dpcshowhidereport"){
            $id=$_POST["id"];
            $level=$_POST["level"];
            $result=$dbdpcCMS->_dpcShowHideReport($id,$level);
            if($result){ 
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        else if($tag=="showhidesysreport"){
            $email=$_POST["email"];
            $sor=$_POST["actiontype"];
            $result=$dbdpcCMSSysReport->_getDPCShowHideSysReport($email,$sor);
            if($result){ 
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="showcountschoolscompliance"){
            $level=$_POST["level"];
            $result=$dbdpcCMS->_dpcShowCountSchoolsCompliance($level);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }
            else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        else if($tag=="deleteactivereport"){
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcDeleteActiveReport($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }
            else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        else if($tag=="deleteactivereportsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCDeleteActiveReportSysReport($email);
            if($result){ 
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        else if($tag=="getspecificreport"){
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcGetSpecificReport($id);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }
            else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        else if($tag=="editpostreport"){
            $name=$_POST["dpc_name"];
            $mor=$_POST["modeofreply"];
            $quantity=$_POST["filequantity"];
            $title=$_POST["title"];
            $desc=$_POST["description"];
            $deadline=$_POST["date_deadline"];
            $updated=$_POST["date_updated"];
            $id=$_POST["id"];
            $result=$dbdpcCMS->_dpcEditPostReport($id,$name,$mor,$quantity,$title,$desc,$deadline,$updated);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Updated";
                echo json_encode($response);
            }else{
                $response["error"]=1;
                $response["msg"]="Error Updated";
                echo json_encode($response);
            }
        }
        else if($tag=="editactivereportsysreport"){
            $email=$_POST["email"];
            $result=$dbdpcCMSSysReport->_getDPCUpdateActiveReportSysReport($email);
            if($result){ 
                $response["success"]=1;
                $response["msg"]="Record Inserted Sysreport";
                echo json_encode($result);
            }else{
                $response["error"]=1;
                $response["status"]="Error Inserting Sysreport";
                $index["0"] = $response;
                echo json_encode($index);
            }
        }
        /* DPC */
        /* PRINCIPAL */
        else if($tag=="principalgetprograms"){
            $result=$dbprincipalCMS->_principalGetPrograms();
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }
            else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        /* PRINCIPAL */
        /* SPC */
        else if($tag=="spcgetprograms"){
            $email=$_POST["email"];
            $result=$dbspcCMS->_spcGetPrograms($email);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }
            else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        else if($tag=="spcprogramgetfilteredreport"){
            $email=$_POST["email"];
            $program=$_POST["program"];
            $result=$dbspcCMS->_spcGetActiveReport($email,$program);
            if($result){
                $response["success"]=1;
                $response["msg"]="Record Selecting";
                echo json_encode($result);
            }
            else{
                $response["error"]=1;
                $response["msg"]="Error Selecting";
                echo json_encode($response);
            }
        }
        /* SPC */
    }else{
        echo "Invalid tag keyword!";
    }
?>