<?php
    Class spcCMS{
        //Get Specific Programs
        public function _spcGetPrograms($email){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT group_concat(program.program separator ', ') as program1 from program 
            WHERE program.program_status='Active'");
            $query1->execute();
            $row1=$query1->fetchAll(PDO::FETCH_ASSOC);
            $groupprogram1 = $row1[0]["program1"];
            $exploded1 = explode(', ',$groupprogram1);
            $query=$db->PREPARE("SELECT user.program as program, user.status FROM user 
            WHERE user.role='School Personnel' AND user.email = :email AND user.status='Active'");
            $query->bindParam(":email",$email);
            if($query->execute()){
                $row=$query->fetchAll(PDO::FETCH_ASSOC);
                $groupprogram = $row[0]["program"];
                $exploded = explode(', ',$groupprogram);
                $arraycompared = array_intersect($exploded1, $exploded);
                return $arraycompared;
            }else{
                return false;
            }
        }
        /* Get All Active Filtered Report */
        public function _spcGetActiveReport($email,$program){
            include('dbconnect.php');
            //1st Query - office
            $query1=$db->PREPARE('SELECT office FROM user WHERE email=:email');
            $query1->bindParam('email', $email);
            $query1->execute();
            $row1=$query1->fetchAll(PDO::FETCH_ASSOC);
            $office=$row1[0]["office"];
            //2nd Query - level
            $query2=$db->PREPARE('SELECT level FROM school WHERE name="'.$office.'"');
            $query2->execute();
            $row2=$query2->fetchAll(PDO::FETCH_ASSOC);
            $level=$row2[0]["level"];
            $explodeLevel= explode(', ',$level);
            //3rd Query - explode + nested for loop
            for($i=0; $i < count($explodeLevel); $i++){
                $query=$db->PREPARE("SELECT id,title,attachment,description,date_posted,date(date_deadline)as date_deadline FROM postreport 
                                            WHERE status_activeorcomplied='Active' 
                                            and program=:program 
                                            and status_shownorhidden='Shown' 
                                            and level like '%".$explodeLevel[$i]."%'
                                            ORDER BY date_posted 
                                            DESC");
                $query->bindParam(":program", $program);
                $query->execute();
                $row=$query->fetchAll(PDO::FETCH_ASSOC);
                for($o=0; $o < count($row); $o++){
                    $htmlTable[] =array("id"=>$row[$o]["id"],
                                         "title"=>$row[$o]["title"],
                                         "attachment"=>$row[$o]["attachment"],
                                         "description"=>$row[$o]["description"],
                                         "date_posted"=>$row[$o]["date_posted"],
                                         "date_deadline"=>$row[$o]["date_deadline"]);
                }
            }
            return $htmlTable;
        }
}
?>