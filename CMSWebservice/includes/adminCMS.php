<?php
    Class adminCMS{
        /* Login for all roles*/
        public function _cmsLogin($email, $password){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT * FROM user WHERE user.email=:email
            AND user.password=SHA1(:password) AND user.status='Active'");
            $query->bindParam(":email",$email);
            $query->bindParam(":password",$password);
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* End of Login */
    /* ---------- Manage Division Personnel ---------- */
        /* Get all Division Personnel */
        public function _cmsGetDP(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT user.id, CONCAT(user.first_name,' ',user.middle_name,' ',user.surname) as Name, 
            user.email, user.office, user.role, user.program, user.contact_number, user.status FROM user 
            WHERE user.role='Division Personnel' AND user.status='Active'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Get all Dropped Division Personnel */
        public function _cmsGetDroppedDP(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT user.id, CONCAT(user.first_name,' ',user.middle_name,' ',user.surname) as Name, 
            user.email, user.office, user.role, user.program, user.contact_number, user.status FROM user 
            WHERE user.role='Division Personnel' AND user.status='Inactive'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        public function _cmsGetSpecificDP($id){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT user.id, user.first_name, user.middle_name, user.surname, user.email, user.office, user.role, user.program, user.contact_number, user.status FROM user 
            WHERE user.role='Division Personnel' AND user.status='Active' and user.id = :id");
            $query->bindParam(":id",$id);
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        public function _cmsDeleteDP($email){
            include('dbconnect.php');
            $query=$db->PREPARE("DELETE FROM user 
            WHERE user.role='Division Personnel' AND user.status='Inactive' and user.email = :email");
            $query->bindParam(":email",$email);
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _cmsRestoreDP($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.status = 'Active' where user.role = 'Division Personnel' and user.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* End of get all Division Personnel */
        /* Get all Office Assignments ? */
        public function _cmsGetOfficeAssignments(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT * from office where office.office_status='Active'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* End of get all Office Assignments */
        /* Get all Programs ? */
        public function _cmsGetPrograms(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT * from program where program.program_status='Active'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* End of get all Programs */
        /* Insert Division Personnel*/
        public function _cmsInsertDivisionPersonnel($first_name,$middle_name,$surname,$email,$office,$program,$contact_number){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT count(user.email) FROM user WHERE user.email = :email");
            $query1->bindParam(":email",$email);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                foreach($row as $value){
                    $count=$value['count(user.email)'];
                }if($count>0){
                    $response["count"] = $count;
                    return $response;
            }else{
                $query=$db->PREPARE("INSERT INTO user(first_name,middle_name,surname,email,password,office,role,program,contact_number,status) 
                VALUES (:first_name,:middle_name,:surname,:email,SHA1(:email),:office,'Division Personnel',:program,:contact_number,'Active')");
                $query->bindParam(":first_name",$first_name);
                $query->bindParam(":middle_name",$middle_name);
                $query->bindParam(":surname",$surname);
                $query->bindParam(":email",$email);
                $query->bindParam(":office",$office);
                $query->bindParam(":program",$program);
                $query->bindParam(":contact_number",$contact_number);
                if($email==""){
                    return false;
                }
                else{
                    $query->execute();
                    return true;
                    }
                }
            }
        }
        /* End of insert division personnel */
        /* Update Division Personnel */
        public function _cmsUpdateDivisionPersonnel($first_name,$middle_name,$surname,$email,$office,$program,$contact_number,$id){
            include('dbconnect.php');
            $query2=$db->PREPARE("SELECT user.email as email FROM user WHERE user.role='Division Personnel' AND user.status='Active' and user.id = :id");
            $query2->bindParam(":id",$id);
            $query1=$db->PREPARE("SELECT count(user.email) as count FROM user WHERE user.email = :email");
            $query1->bindParam(":email",$email);
            $query2->execute();
            $row2=$query2->fetchAll(PDO::FETCH_ASSOC);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                $count = $row[0]["count"];
                $email1 = $row2[0]["email"];
            if($count>0 and $email1 != $email){
                    $response["success"]=0;
                    $response["count"] = $count;
                    $response["email1"] = $email1;
                    $response["email"] = $email;
                    return $response;
            }else{
            $query=$db->PREPARE("UPDATE user set first_name = :first_name, middle_name = :middle_name, surname = :surname, email = :email, 
            office = :office, program = :program, contact_number = :contact_number where user.id = :id");
            $query->bindParam(":first_name",$first_name);
            $query->bindParam(":middle_name",$middle_name);
            $query->bindParam(":surname",$surname);
            $query->bindParam(":email",$email);
            $query->bindParam(":office",$office);
            $query->bindParam(":program",$program);
            $query->bindParam(":contact_number",$contact_number);
            $query->bindParam(":id",$id);
            if($email==""){
                return false;
            }
            else{
                $query->execute();
                $response["success"]=1;
                $response["msg"]="Record Updated";
                return $response;
                    }
                }
            }
        }
        /* End of update division personnel*/
        /* Reset Division Personnel */
        public function _cmsResetDivisionPersonnel($email,$id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.password = SHA1(:email) where user.role = 'Division Personnel' and user.id = :id");
            $query->bindParam(":email",$email);
            $query->bindParam(":id",$id);
            if($email==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Drop Division Personnel*/
        public function _cmsDropDivisionPersonnel($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.status = 'Inactive' where user.role = 'Division Personnel' and user.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
    /* ---------- End of manage division personnel ---------- */
    /* ---------- Manage School ---------- */
        /* Get all School */
        public function _cmsGetSchool(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT school.id, school.name, school.address, school.contact_number, school.level, school.status FROM school 
            WHERE school.status='Active'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Insert School*/
        public function _cmsInsertSchool($name,$address,$contact_number,$level){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT count(school.name) FROM school WHERE school.name = :name");
            $query1->bindParam(":name",$name);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                foreach($row as $value){
                    $count=$value['count(school.name)'];
                }if($count>0){
                    $response["count"] = $count;
                    return $response;
            }else{
                $query=$db->PREPARE("INSERT INTO school(name,address,contact_number,level,status) 
                VALUES (:name,:address,:contact_number,:level,'Active')");
                $query->bindParam(":name",$name);
                $query->bindParam(":address",$address);
                $query->bindParam(":contact_number",$contact_number);
                $query->bindParam(":level",$level);
                if($name==""){
                    return false;
                }
                else{
                    $query->execute();
                    return true;
                    }
                }
            }
        }
        /* Update School*/
        public function _cmsUpdateSchool($name,$address,$contact_number,$level,$id){
            include('dbconnect.php');
            $query2=$db->PREPARE("SELECT school.name as name FROM school WHERE school.status='Active' and school.id = :id");
            $query2->bindParam(":id",$id);
            $query1=$db->PREPARE("SELECT count(school.name) as count FROM school WHERE school.name = :name");
            $query1->bindParam(":name",$name);
            $query2->execute();
            $row2=$query2->fetchAll(PDO::FETCH_ASSOC);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                $count = $row[0]["count"];
                $name1 = $row2[0]["name"];
            if($count>0 and $name1 != $name){
                    $response["success"]=0;
                    $response["count"] = $count;
                    $response["name1"] = $name1;
                    $response["name"] = $name;
                    return $response;
            }else{
            $query=$db->PREPARE("UPDATE school set name = :name, address = :address ,contact_number = :contact_number, level = :level where school.id = :id");
            $query->bindParam(":name",$name);
            $query->bindParam(":address",$address);
            $query->bindParam(":contact_number",$contact_number);
            $query->bindParam(":level",$level);
            $query->bindParam(":id",$id);
            if($name==""){
                return false;
            }
            else{
                $query->execute();
                $response["success"]=1;
                $response["msg"]="Record Updated";
                return $response;
                    }
                }
            }
        }
        /* Drop School*/
        public function _cmsDropSchool($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE school set school.status = 'Inactive' where school.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Get all Dropped School */
        public function _cmsGetDroppedSchool(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT school.id, school.name, school.address, school.contact_number, school.level, school.status FROM school 
            WHERE school.status='Inactive'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        public function _cmsRestoreSchool($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE school set school.status = 'Active' where school.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        public function _cmsDeleteSchool($name){
            include('dbconnect.php');
            $query=$db->PREPARE("DELETE FROM school 
            WHERE school.status='Inactive' and school.name = :name");
            $query->bindParam(":name",$name);
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
    /* ---------- End of manage school ---------- */
    /* ---------- Manage Principal ---------- */
        public function _cmsGetPrincipal(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT user.id, user.first_name, user.middle_name, user.surname, CONCAT(user.first_name,' ',user.middle_name,' ',user.surname) as Name, 
            user.email, user.office, user.role, user.program, user.contact_number, user.status FROM user 
            WHERE user.role='Principal' AND user.status='Active'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Get all Office Assignments ? */
        public function _cmsGetPrincipalSchool(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT * from school where school.status='Active' ");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* End of get all Office Assignments */
        /* Insert Principal */
        public function _cmsInsertPrincipal($first_name,$middle_name,$surname,$email,$office,$contact_number){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT count(user.email) FROM user WHERE user.email = :email");
            $query1->bindParam(":email",$email);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                foreach($row as $value){
                    $count=$value['count(user.email)'];
                }if($count>0){
                    $response["count"] = $count;
                    return $response;
            }else{
                $query=$db->PREPARE("INSERT INTO user(first_name,middle_name,surname,email,password,office,role,contact_number,status) 
                VALUES (:first_name,:middle_name,:surname,:email,SHA1(:email),:office,'Principal',:contact_number,'Active')");
                $query->bindParam(":first_name",$first_name);
                $query->bindParam(":middle_name",$middle_name);
                $query->bindParam(":surname",$surname);
                $query->bindParam(":email",$email);
                $query->bindParam(":office",$office);
                $query->bindParam(":contact_number",$contact_number);
                if($email==""){
                    return false;
                }
                else{
                    $query->execute();
                    return true;
                    }
                }
            }
        }
        /* End of insert principal */
        public function _cmsUpdatePrincipal($first_name,$middle_name,$surname,$email,$office,$contact_number,$id){
            include('dbconnect.php');
            $query2=$db->PREPARE("SELECT user.email as email FROM user WHERE user.role='Principal' AND user.status='Active' and user.id = :id");
            $query2->bindParam(":id",$id);
            $query1=$db->PREPARE("SELECT count(user.email) as count FROM user WHERE user.email = :email");
            $query1->bindParam(":email",$email);
            $query2->execute();
            $row2=$query2->fetchAll(PDO::FETCH_ASSOC);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                $count = $row[0]["count"];
                $email1 = $row2[0]["email"];
            if($count>0 and $email1 != $email){
                    $response["success"]=0;
                    $response["count"] = $count;
                    $response["email1"] = $email1;
                    $response["email"] = $email;
                    return $response;
            }else{
            $query=$db->PREPARE("UPDATE user set first_name = :first_name, middle_name = :middle_name, surname = :surname, email = :email, 
            office = :office, contact_number = :contact_number where user.id = :id");
            $query->bindParam(":first_name",$first_name);
            $query->bindParam(":middle_name",$middle_name);
            $query->bindParam(":surname",$surname);
            $query->bindParam(":email",$email);
            $query->bindParam(":office",$office);
            $query->bindParam(":contact_number",$contact_number);
            $query->bindParam(":id",$id);
            if($email==""){
                return false;
            }
            else{
                $query->execute();
                $response["success"]=1;
                $response["msg"]="Record Updated";
                return $response;
                    }
                }
            }
        }
        /* Reset Principal */
        public function _cmsResetPrincipal($email,$id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.password = SHA1(:email) where user.role = 'Principal' and user.id = :id");
            $query->bindParam(":email",$email);
            $query->bindParam(":id",$id);
            if($email==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Drop Principal*/
        public function _cmsDropPrincipal($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.status = 'Inactive' where user.role = 'Principal' and user.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Get all Dropped Principal */
        public function _cmsGetDroppedPrincipal(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT user.id, CONCAT(user.first_name,' ',user.middle_name,' ',user.surname) as Name, 
            user.email, user.office, user.role, user.contact_number, user.status FROM user 
            WHERE user.role='Principal' AND user.status='Inactive'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Restore Principal */
        public function _cmsRestorePrincipal($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.status = 'Active' where user.role = 'Principal' and user.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Delete Principal */
        public function _cmsDeletePrincipal($email){
            include('dbconnect.php');
            $query=$db->PREPARE("DELETE FROM user 
            WHERE user.role='Principal' AND user.status='Inactive' and user.email = :email");
            $query->bindParam(":email",$email);
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        /* ---------- End of manage principal ---------- */
        /* ------------------------------ Miscellaneous ------------------------------ */
        /* ---------- Manage Office ---------- */
        public function _cmsGetMiscOffice(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT office.id, office.office, office.office_status FROM office 
            WHERE office.office_status='Active'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Insert Office*/
        public function _cmsInsertOffice($office){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT count(office.office) FROM office WHERE office.office = :office");
            $query1->bindParam(":office",$office);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                foreach($row as $value){
                    $count=$value['count(office.office)'];
                }if($count>0){
                    $response["count"] = $count;
                    return $response;
            }else{
                $query=$db->PREPARE("INSERT INTO office(office,office_status) VALUES (:office,'Active')");
                $query->bindParam(":office",$office);
                if($office==""){
                    return false;
                }
                else{
                    $query->execute();
                    return true;
                    }
                }
            }
        }
        public function _cmsUpdateOffice($office,$id){
            include('dbconnect.php');
            $query2=$db->PREPARE("SELECT office.office as office FROM office WHERE office.office_status='Active' and office.id = :id");
            $query2->bindParam(":id",$id);
            $query1=$db->PREPARE("SELECT count(office.office) as count FROM office WHERE office.office = :office");
            $query1->bindParam(":office",$office);
            $query2->execute();
            $row2=$query2->fetchAll(PDO::FETCH_ASSOC);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                $count = $row[0]["count"];
                $office1 = $row2[0]["office"];
            if($count>0 and $office1 != $office){
                    $response["success"]=0;
                    $response["count"] = $count;
                    $response["office1"] = $office1;
                    $response["office"] = $office;
                    return $response;
            }else{
            $query=$db->PREPARE("UPDATE office set office = :office where office.id = :id");
            $query->bindParam(":office",$office);
            $query->bindParam(":id",$id);
            if($office==""){
                return false;
            }
            else{
                $query->execute();
                $response["success"]=1;
                $response["msg"]="Record Updated";
                return $response;
                    }
                }
            }
        }
        /* Drop Office*/
        public function _cmsDropOffice($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE office set office.office_status = 'Inactive' where office.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Get all Dropped Office */
        public function _cmsGetDroppedOffice(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT office.id, office.office, office.office_status FROM office WHERE office.office_status='Inactive'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Restore an Office */
        public function _cmsRestoreOffice($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE office set office.office_status = 'Active' where office.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Delete an Office */
        public function _cmsDeleteOffice($office){
            include('dbconnect.php');
            $query=$db->PREPARE("DELETE FROM office WHERE office.office_status='Inactive' and office.office = :office");
            $query->bindParam(":office",$office);
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        /* ---------- End of manage office ---------- */
        /* ---------- Manage Program ---------- */
        public function _cmsGetMiscProgram(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT program.id, program.program, program.description, program.program_status FROM program 
            WHERE program.program_status='Active'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Insert Program*/
        public function _cmsInsertProgram($program,$description){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT count(program.program) FROM program WHERE program.program = :program");
            $query1->bindParam(":program",$program);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                foreach($row as $value){
                    $count=$value['count(program.program)'];
                }if($count>0){
                    $response["count"] = $count;
                    return $response;
            }else{
                $query=$db->PREPARE("INSERT INTO program(program,description,program_status) VALUES (:program,:description,'Active')");
                $query->bindParam(":program",$program);
                $query->bindParam(":description",$description);
                if($program==""){
                    return false;
                }
                else{
                    $query->execute();
                    return true;
                    }
                }
            }
        }
        public function _cmsUpdateProgram($program,$description,$id){
            include('dbconnect.php');
            $query2=$db->PREPARE("SELECT program.program as program FROM program WHERE program.program_status='Active' and program.id = :id");
            $query2->bindParam(":id",$id);
            $query1=$db->PREPARE("SELECT count(program.program) as count FROM program WHERE program.program = :program");
            $query1->bindParam(":program",$program);
            $query2->execute();
            $row2=$query2->fetchAll(PDO::FETCH_ASSOC);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                $count = $row[0]["count"];
                $program1 = $row2[0]["program"];
            if($count>0 and $program1 != $program){
                    $response["success"]=0;
                    $response["count"] = $count;
                    $response["program1"] = $program1;
                    $response["program"] = $program;
                    return $response;
            }else{
            $query=$db->PREPARE("UPDATE program set program = :program, description = :description where program.id = :id");
            $query->bindParam(":program",$program);
            $query->bindParam(":description",$description);
            $query->bindParam(":id",$id);
            if($program==""){
                return false;
            }
            else{
                $query->execute();
                $response["success"]=1;
                $response["msg"]="Record Updated";
                return $response;
                    }
                }
            }
        }
        /* Drop Program*/
        public function _cmsDropProgram($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE program set program.program_status = 'Inactive' where program.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Get all Dropped Program */
        public function _cmsGetDroppedProgram(){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT program.id, program.program, program.description, program.program_status FROM program WHERE program.program_status='Inactive'");
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Restore an Program */
        public function _cmsRestoreProgram($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE program set program.program_status = 'Active' where program.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Delete an Program */
        public function _cmsDeleteProgram($program){
            include('dbconnect.php');
            $query=$db->PREPARE("DELETE FROM program WHERE program.program_status='Inactive' and program.program = :program");
            $query->bindParam(":program",$program);
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
    /* ---------- End of manage program ---------- */
    /* ------------------------------ End of miscellaneous ------------------------------ */
    }
    /* End of Class CMS{} */
    /* ---------------------------------------- For System reports ----------------------------------------*/
    Class adminCMSSysReport{
        public function _getLoginSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Logged in' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getLogoutSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Logged out' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
    /* ---------- Manage Division Personnel ---------- */
        public function _getAdminDashboardInsertSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Inserted a Division Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getAdminDashboardUpdateSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Updated a Division Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getAdminDashboardResetSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Reset a Division Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getAdminDashboardDropSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Dropped a Division Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getAdminDashboardRestoreSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Restored a Division Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getAdminDashboardDeleteSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Deleted a Division Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
    /* ---------- End of manage division personnel ---------- */
    /* ---------- Manage School ----------- */
    public function _getAdminDashboardSchoolInsertSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Inserted a School' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardSchoolUpdateSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Updated a School' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardSchoolDropSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Dropped a School' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardSchoolRestoreSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Restored a School' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardSchoolDeleteSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Deleted a School' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    /* ---------- End of manage school ---------- */
    /* ---------- Manage Principal ---------- */
    public function _getAdminDashboardPrincipalInsertSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Inserted a Principal' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardPrincipalUpdateSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Updated a Principal' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardPrincipalResetSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Reset a Principal' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardPrincipalDropSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Dropped a Principal' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardPrincipalRestoreSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Restored a Principal' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardPrincipalDeleteSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Deleted a Principal' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    /* ---------- End of manage principal ---------- */
    /* ------------------------------ Miscellaneous ------------------------------ */
    /* ---------- Manage Office ---------- */
    public function _getAdminDashboardOfficeInsertSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Inserted an Office' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardOfficeUpdateSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Updated an Office' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardOfficeDropSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Dropped an Office' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardOfficeRestoreSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Restored an Office' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardOfficeDeleteSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Deleted an Office' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    /* ---------- End of manage office ---------- */
    /* ---------- Manage Program ---------- */
    public function _getAdminDashboardProgramInsertSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Inserted a Program' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardProgramUpdateSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Updated a Program' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardProgramDropSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Dropped a Program' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardProgramRestoreSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Restored a Program' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    public function _getAdminDashboardProgramDeleteSysReport($email){
        include('dbconnect.php');
        $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
        SELECT email, 'Deleted a Program' FROM user 
        WHERE email = :email AND status = 'Active'");
        $query->bindParam(":email",$email);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    /* ---------- End of manage program ---------- */
    /* ------------------------------ End of miscellaneous ------------------------------ */
    }
    /* End of Class CMSSysReport{} */
?>