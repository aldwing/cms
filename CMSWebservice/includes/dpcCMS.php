<?php
    Class dpcCMS{
    /* Manage SPC */
        /* Get all SPC */
        public function _dpcGetSPC($email){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT user.program as program1 FROM user WHERE email=:email");
            $query1->bindParam(":email", $email);
            $query1->execute();
            $row1=$query1->fetchAll(PDO::FETCH_ASSOC);
            $groupprogram1 = $row1[0]["program1"];
            $row1Exploded= explode(', ', $groupprogram1);
            //2nd query
            //$htmlTable= "";
            for($i=0; $i < count($row1Exploded); $i++){
                $query=$db->PREPARE("SELECT user.id, CONCAT(user.first_name,' ',user.middle_name,' ',user.surname) as Name, 
                user.email, user.office, user.role, user.program, user.contact_number, user.status FROM user 
                WHERE user.role='School Personnel' AND user.status='Active' AND user.program LIKE '%".$row1Exploded[$i]."%'");
                $query->execute();
                $row=$query->fetchAll(PDO::FETCH_ASSOC);
                for($o=0; $o < count($row); $o++){
                    //$htmlTable .="<tr><td>".$row[$o]["Name"]."</td><td>".$row[$o]["email"]."</td><td>".$row[$o]["office"]."</td><td>".$row[$o]["role"]."</td><td>".$row[$o]["program"]."</td><td>".$row[$o]["contact_number"]."</td><td>".$row[$o]["status"]."</td></tr>";
                    $htmlTable1[] =array("id"=>$row[$o]["id"],
                                         "Name"=>$row[$o]["Name"],
                                         "email"=>$row[$o]["email"],
                                         "office"=>$row[$o]["office"],
                                         "role"=>$row[$o]["role"],
                                         "program"=>$row[$o]["program"],
                                         "contact_number"=>$row[$o]["contact_number"],
                                         "status"=>$row[$o]["status"]);
                }
            }
            return $htmlTable1;
        }
        /* Get and compare specific DPC's and SPC's program/s */
        public function _dpcGetCompareProgramSPC($email,$office){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT group_concat(program separator ', ') as program1 FROM user 
            WHERE user.office = :office AND user.role='School Personnel' AND user.status='Active'");
            $query1->bindParam(":office",$office);
            $query1->execute();
            $row1=$query1->fetchAll(PDO::FETCH_ASSOC);
            $groupprogram1 = $row1[0]["program1"];
            $exploded1 = explode(', ',$groupprogram1);
            $query=$db->PREPARE("SELECT user.program as program, user.status FROM user 
            WHERE user.role='Division Personnel' AND user.email = :email AND user.status='Active'");
            $query->bindParam(":email",$email);
            if($query->execute()){
                $row=$query->fetchAll(PDO::FETCH_ASSOC);
                $groupprogram = $row[0]["program"];
                $exploded = explode(', ',$groupprogram);
                $arraycompared = array_values(array_diff($exploded, $exploded1));
                    return $arraycompared;
            }else{
                return false;
            }
        }
        /* Insert SPC*/
        public function _dpcInsertSPC($first_name,$middle_name,$surname,$email,$office,$program,$contact_number){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT count(user.email) as count1 FROM user WHERE user.email = :email");
            $query1->bindParam(":email",$email);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                $count = $row[0]['count1'];
            if($count>0){
                $response["count"] = $count;
                return $response;
            }else{
                $query=$db->PREPARE("INSERT INTO user(first_name,middle_name,surname,email,password,office,role,program,contact_number,status) 
                VALUES (:first_name,:middle_name,:surname,:email,SHA1(:email),:office,'School Personnel',:program,:contact_number,'Active')");
                $query->bindParam(":first_name",$first_name);
                $query->bindParam(":middle_name",$middle_name);
                $query->bindParam(":surname",$surname);
                $query->bindParam(":email",$email);
                $query->bindParam(":office",$office);
                $query->bindParam(":program",$program);
                $query->bindParam(":contact_number",$contact_number);
                if($email==""){
                    return false;
                }
                else{
                    $query->execute();
                    return true;
                }
            }
        }
    }
        /* Get specific SPC */
        public function _dpcGetSpecificSPC($id){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT user.id, user.first_name, user.middle_name, user.surname, user.email, user.office, user.role, user.program, user.contact_number, user.status FROM user 
            WHERE user.role='School Personnel' AND user.status='Active' and user.id = :id");
            $query->bindParam(":id",$id);
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Update SPC */
        public function _dpcUpdateSPC($first_name,$middle_name,$surname,$email,$office,$program,$contact_number,$id){
            include('dbconnect.php');
            $query2=$db->PREPARE("SELECT user.email as email FROM user WHERE user.role='School Personnel' AND user.status='Active' and user.id = :id");
            $query2->bindParam(":id",$id);
            $query1=$db->PREPARE("SELECT count(user.email) as count FROM user WHERE user.email = :email");
            $query1->bindParam(":email",$email);
            $query2->execute();
            $row2=$query2->fetchAll(PDO::FETCH_ASSOC);
            if($query1->execute()){
                $row=$query1->fetchAll(PDO::FETCH_ASSOC);
                $count = $row[0]["count"];
                $email1 = $row2[0]["email"];
            if($count>0 and $email1 != $email){
                    $response["success"]=0;
                    $response["count"] = $count;
                    $response["email1"] = $email1;
                    $response["email"] = $email;
                    return $response;
            }else{
            $query=$db->PREPARE("UPDATE user set first_name = :first_name, middle_name = :middle_name, surname = :surname, email = :email, 
            office = :office, program = :program, contact_number = :contact_number where user.id = :id");
            $query->bindParam(":first_name",$first_name);
            $query->bindParam(":middle_name",$middle_name);
            $query->bindParam(":surname",$surname);
            $query->bindParam(":email",$email);
            $query->bindParam(":office",$office);
            $query->bindParam(":program",$program);
            $query->bindParam(":contact_number",$contact_number);
            $query->bindParam(":id",$id);
            if($email==""){
                return false;
            }
            else{
                $query->execute();
                $response["success"]=1;
                $response["msg"]="Record Updated";
                return $response;
                    }
                }
            }
        }
        /* Reset SPC */
        public function _dpcResetSPC($email,$id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.password = SHA1(:email) where user.role = 'School Personnel' and user.id = :id");
            $query->bindParam(":email",$email);
            $query->bindParam(":id",$id);
            if($email==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Drop SPC*/
        public function _dpcDropSPC($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.status = 'Inactive' where user.role = 'School Personnel' and user.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Get all Dropped SPC */
        public function _dpcGetDroppedSPC($email){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT user.program as program1 FROM user WHERE email=:email");
            $query1->bindParam(":email", $email);
            $query1->execute();
            $row1=$query1->fetchAll(PDO::FETCH_ASSOC);
            $groupprogram1 = $row1[0]["program1"];
            $row1Exploded= explode(', ', $groupprogram1);
            //2nd query
            $htmlTable= "";
            for($i=0; $i < count($row1Exploded); $i++){
                $query=$db->PREPARE("SELECT user.id, CONCAT(user.first_name,' ',user.middle_name,' ',user.surname) as Name, 
                user.email, user.office, user.role, user.program, user.contact_number, user.status FROM user 
                WHERE user.role='School Personnel' AND user.status='Inactive' AND user.program LIKE '%".$row1Exploded[$i]."%'");
                $query->execute();
                $row=$query->fetchAll(PDO::FETCH_ASSOC);
                for($o=0; $o < count($row); $o++){
                    //$htmlTable .="<tr><td>".$row[$o]["Name"]."</td><td>".$row[$o]["email"]."</td><td>".$row[$o]["office"]."</td><td>".$row[$o]["role"]."</td><td>".$row[$o]["program"]."</td><td>".$row[$o]["contact_number"]."</td><td>".$row[$o]["status"]."</td></tr>";
                    $htmlTable1[] =array("id"=>$row[$o]["id"],
                                         "Name"=>$row[$o]["Name"],
                                         "email"=>$row[$o]["email"],
                                         "office"=>$row[$o]["office"],
                                         "role"=>$row[$o]["role"],
                                         "program"=>$row[$o]["program"],
                                         "contact_number"=>$row[$o]["contact_number"],
                                         "status"=>$row[$o]["status"]);
                }
            }
            return $htmlTable1;
        }
        public function _dpcDeleteSPC($email){
            include('dbconnect.php');
            $query=$db->PREPARE("DELETE FROM user 
            WHERE user.role='School Personnel' AND user.status='Inactive' and user.email = :email");
            $query->bindParam(":email",$email);
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _dpcRestoreSPC($id){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE user set user.status = 'Active' where user.role = 'School Personnel' and user.id = :id");
            $query->bindParam(":id",$id);
            if($id==""){
                return false;
            }
            else{
                $query->execute();
                return true;
            }
        }
        /* Get all DPC's Program/s */
        public function _dpcGetProgramSPC($email){
            include('dbconnect.php');
            $query1=$db->PREPARE("SELECT group_concat(program.program separator ', ') as program1 from program 
            WHERE program.program_status='Active'");
            $query1->execute();
            $row1=$query1->fetchAll(PDO::FETCH_ASSOC);
            $groupprogram1 = $row1[0]["program1"];
            $exploded1 = explode(', ',$groupprogram1);
            $query=$db->PREPARE("SELECT user.program as program, user.status FROM user 
            WHERE user.role='Division Personnel' AND user.email = :email AND user.status='Active'");
            $query->bindParam(":email",$email);
            if($query->execute()){
                $row=$query->fetchAll(PDO::FETCH_ASSOC);
                $groupprogram = $row[0]["program"];
                $exploded = explode(', ',$groupprogram);
                $arraycompared = array_intersect($exploded1, $exploded);
                return $arraycompared;
            }else{
                return false;
            }
        }
        /* Create a Post/Report */
        public function _dpcCreatePostReport($dpc_name,$modeofreply,$filequantity,$title,$attachment,$description,$date_deadline,$program){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO postreport (dpc_name,
                                                         modeofreply,
                                                         filequantity,
                                                         title,
                                                         attachment,
                                                         description,
                                                         date_deadline,
                                                         program,
                                                         status_activeorcomplied,
                                                         status_shownorhidden)
                                                         VALUES
                                                        (:dpc_name,
                                                         :modeofreply,
                                                         :filequantity,
                                                         :title,
                                                         :attachment,
                                                         :description,
                                                         :date_deadline,
                                                         :program,
                                                         'Active',
                                                         'Hidden')");
            $query->bindParam(":dpc_name",$dpc_name);
            $query->bindParam(":modeofreply",$modeofreply);
            $query->bindParam(":filequantity",$filequantity);
            $query->bindParam(":title",$title);
            $query->bindParam(":attachment",$attachment);
            $query->bindParam(":description",$description);
            $query->bindParam(":date_deadline",$date_deadline);
            $query->bindParam(":program",$program);
            if($query->execute()){
                return true;
            }
            else{
                return false;
            }
        }
        /* Get All Active Report */
        public function _dpcGetActiveReport($program){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT * FROM postreport 
                                          WHERE (status_activeorcomplied='Active' 
                                          and program=:program 
                                          and (status_shownorhidden='Hidden' or status_shownorhidden='Shown')) 
                                          ORDER BY date_posted 
                                          DESC");
            $query->bindParam(":program", $program);
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        /* Show/Hide Report filter->Level */
        public function _dpcShowHideReport($id,$level){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT status_shownorhidden FROM postreport WHERE postreport.id=:id");
            $query->bindParam(":id", $id);
            if($query->execute()){
                $row=$query->fetchAll(PDO::FETCH_ASSOC);
                $status=array('Status'=>$row[0]['status_shownorhidden']);
                if($status['Status']=="Hidden"){
                    $query1=$db->PREPARE("UPDATE postreport SET postreport.status_shownorhidden='Shown',postreport.level=:level 
                                          WHERE postreport.id=:id AND postreport.status_activeorcomplied='Active'");
                    $query1->bindParam(":id", $id);
                    $query1->bindParam(":level", $level);
                    if($query1->execute()){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
                else{
                    $query2=$db->PREPARE("UPDATE postreport SET postreport.status_shownorhidden='Hidden',postreport.level=:level 
                                          WHERE postreport.id=:id AND postreport.status_activeorcomplied='Active'");
                    $query2->bindParam(":id", $id);
                    $query2->bindParam(":level", $level);
                    if($query2->execute()){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
            }else{
                return false;
            }
        }
        /* Show Count of Schools for Compliance */
        public function _dpcShowCountSchoolsCompliance($level){
            include('dbconnect.php');
            if($level=="Primary, Secondary"){
                $query=$db->PREPARE("SELECT count(id) as count FROM school 
                                     WHERE school.status='Active'");
                if($query->execute()){
                    return $query->fetchAll(PDO::FETCH_ASSOC);
                }
                else{
                    return false;
                }
            }
            else if($level=="Primary"){
                $query1=$db->PREPARE("SELECT count(id) as count FROM school 
                                     WHERE school.status='Active' AND school.level='Primary'");
                if($query1->execute()){
                    return $query1->fetchAll(PDO::FETCH_ASSOC);
                }
                else{
                    return false;
                }
            }
            else if($level=="Secondary"){
                $query2=$db->PREPARE("SELECT count(id) as count FROM school 
                WHERE school.status='Active' AND school.level='Secondary'");
                if($query2->execute()){
                    return $query2->fetchAll(PDO::FETCH_ASSOC);
                }
                else{
                    return false;
                }
            }
            // else{
            //     return false;
            // }
        }
        public function _dpcDeleteActiveReport($id){
            include('dbconnect.php');
            $query=$db->PREPARE("DELETE FROM postreport WHERE postreport.id=:id AND postreport.status_activeorcomplied='Active'");
            $query->bindParam(":id",$id);
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        /* Get specific Report */
        public function _dpcGetSpecificReport($id){
            include('dbconnect.php');
            $query=$db->PREPARE("SELECT id,title,description,date(date_deadline)as date_deadline,modeofreply,filequantity FROM postreport WHERE postreport.status_activeorcomplied='Active' AND postreport.id = :id");
            $query->bindParam(":id",$id);
            if($query->execute()){
                return $query->fetchAll(PDO::FETCH_ASSOC);
            }else{
                return false;
            }
        }
        public function _dpcEditPostReport($id,$name,$mor,$quantity,$title,$desc,$deadline,$updated){
            include('dbconnect.php');
            $query=$db->PREPARE("UPDATE postreport set dpc_name=:name, modeofreply=:mor, filequantity=:quantity, title=:title,
                                 description=:desc, date_deadline=:deadline, date_updated=:updated WHERE id=:id");
            $query->bindParam(":name",$name);
            $query->bindParam(":mor",$mor);
            $query->bindParam(":quantity",$quantity);
            $query->bindParam(":title",$title);
            $query->bindParam(":desc",$desc);
            $query->bindParam(":deadline",$deadline);
            $query->bindParam(":updated",$updated);
            $query->bindParam(":id",$id);
            if($query->execute()){
                return true;
            }
            else{
                return false;
            }
        }
    /* End of manage spc */
    /* End of Class dpcCMS */
    }
    /* ---------------------------------------- For System reports ----------------------------------------*/
    Class dpcCMSSysReport{
    /* Manage SPC */
        public function _getDPCInsertSPCSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Inserted a School Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCUpdateSPCSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Updated a School Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCResetSPCSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Reset a School Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCDropSPCSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Dropped a School Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCDeleteSPCSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Deleted a School Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCRestoreSPCSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Restored a School Personnel' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCCreatePostReportSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Created a Post/Report' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCShowHideSysReport($email, $sor){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, :sor FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            $query->bindParam(":sor",$sor);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCDeleteActiveReportSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Deleted a Post/Report' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
        public function _getDPCUpdateActiveReportSysReport($email){
            include('dbconnect.php');
            $query=$db->PREPARE("INSERT INTO sysreport(email, actiontype) 
            SELECT email, 'Updated a Post/Report' FROM user 
            WHERE email = :email AND status = 'Active'");
            $query->bindParam(":email",$email);  
            if($query->execute()){
                return true;
            }else{
                return false;
            }
        }
    /* End of manage spc */   
    }
?>