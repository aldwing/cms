<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use RealRashid\SweetAlert\Facades\Alert;

class adminController extends Controller{
    /* Manage School */
    public function _adminDashboardManageSchool(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getschool',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["status"]=='Active'){
            return view('roles/admin/adminDashboardManageSchool',['getschool'=>$data]);
        }
        else{
            alert()->warning('Error','No Data, add a school first');
            return view('roles/admin/adminDashboardManageSchoolAdd');
        }
    }
    public function _adminDashboardManageSchoolAdd(){
        return view('roles/admin/adminDashboardManageSchoolAdd');
    }
    public function _adminInsertSchool(Request $request){
        $name=$request["new-School-Name"];
        $address=$request["new-School-Address"];
        $contact_number=$request["new-School-ContactNumber"];
        $level=$request["new-School-SchoolLevel"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'insertschool',
                'name'=>$name,
                'address'=>$address,
                'contact_number'=>$contact_number,
                'level'=>$level,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* insertdpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'insertschoolsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["count"]<=0){
            alert()->success('Successully', 'Inserted a School');
            return redirect()->route('_adminDashboardManageSchool');
        }
        else{
            alert()->warning('Error','School already exists');
            return redirect()->route('_adminDashboardManageSchool');
        }
    }
    public function _adminUpdateSchool(Request $request){
        $name=$request["new-School-NameEdit"];
        $address=$request["new-School-AddressEdit"];
        $contact_number=$request["new-School-ContactNumberEdit"];
        $level=$request["new-School-SchoolLevelEdit"];
        $id=$request["new-School-IDEdit"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'updateschool',
                'name'=>$name,
                'address'=>$address,
                'contact_number'=>$contact_number,
                'level'=>$level,
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* updatedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'updateschoolsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["success"]==1){
            alert()->success('Successully', 'Updated a School');
            return redirect()->route('_adminDashboardManageSchool');
        }
        else{
            alert()->warning('Error','School Name already exists');
            return redirect()->route('_adminDashboardManageSchool');
        }
    }
    public function _adminDropSchool(Request $request){
        $id=$request["new-School-DropID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'dropschool',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* droppsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'dropschoolsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Dropped a School');
        return redirect()->route('_adminDashboardManageSchool');
    }
    public function _adminDashboardManageSchoolArchive(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getdroppedschool',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["status"]=='Inactive'){  
            return view('roles/admin/adminDashboardManageSchoolArchive',['getschoolarchive'=>$data]);
        }
        else{
            alert()->warning('Error','No Data');
            return redirect()->route('_adminDashboardManageSchool');
        }
    }
    public function _adminDashboardManageSchoolRestore(Request $request){
        $id=$request["new-School-ArchiveRestoreID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'restoreschool',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* restoredpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'restoreschoolsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Restored a School');
        return redirect()->route('_adminDashboardManageSchoolArchive');

    }
    public function _adminDashboardManageSchoolDelete(Request $request){
        $compareName=$request["admin-School-ArchiveHiddenName"];
        $name=$request["new-School-ArchiveName"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'deleteschool',
                'name'=>$name,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* deletedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'deleteschoolsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Deleted a School');
        return redirect()->route('_adminDashboardManageSchoolArchive');

    }
    /* End of manage school */
    /* ---------- Manage Division Personnel ---------- */
    public function _adminDashboardManageDP(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getdp',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* getofficeassignments */
        $client2=new \GuzzleHttp\Client();
        $url2="http://localhost/CMSWebservice/";
        $response2=$client2->request('POST', $url2, [
            'form_params'=>[
                'tag'=>'getofficeassignments',
            ]
        ]);
        $data2=json_decode($response2->getBody()->getContents(), true);
        /* get programs */
        $client3=new \GuzzleHttp\Client();
        $url3="http://localhost/CMSWebservice/";
        $response3=$client3->request('POST', $url3, [
            'form_params'=>[
                'tag'=>'getprograms',
            ]
        ]);
        $data3=json_decode($response3->getBody()->getContents(), true);
        if($data[0]["status"]=='Active'){
            return view('roles/admin/adminDashboardManageDP',['getdp'=>$data]);
        }
        else{
            alert()->warning('Error','No Data');
            return view('roles/admin/adminDashboardManageDPAdd',['adminnewoffice'=>$data2,'adminnewprogram'=>$data3]);
        }
    }
    public function _adminDashboardManageDPAdd(){
        /* getofficeassignments */
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getofficeassignments',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* get programs */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'getprograms',
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        return view('roles/admin/adminDashboardManageDPAdd',['adminnewoffice'=>$data,'adminnewprogram'=>$data1]);
    }
    public function _adminResetDivisionPersonnel(Request $request){
        $email=$request["new-DP-ResetEmail"];
        $id=$request["new-DP-ResetID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'resetdp',
                'email'=>$email,
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* resetdpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'resetdivisionpersonnelsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Reset a Division Personnel');
        return redirect()->route('_adminDashboardManageDP');
    }
    public function _adminDashboardManageDPArchive(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getdroppeddp',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["status"]=='Inactive'){  
            return view('roles/admin/adminDashboardManageDPArchive',['getdparchive'=>$data]);
        }
        else{
            alert()->warning('Error','No Data');
            return redirect()->route('_adminDashboardManageDP');
        }
    }
    public function _adminDashboardManageDPDelete(Request $request){
        $compareEmail=$request["admin-DP-ArchiveHiddenEmail"];
        $email=$request["new-DP-ArchiveEmail"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'deletedp',
                'email'=>$email,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* deletedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'deletedivisionpersonnelsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Deleted a Division Personnel');
        return redirect()->route('_adminDashboardManageDPArchive');

    }
    public function _adminDashboardManageDPRestore(Request $request){
        $id=$request["new-DP-ArchiveRestoreID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'restoredp',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* restoredpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'restoredivisionpersonnelsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Restored a Division Personnel');
        return redirect()->route('_adminDashboardManageDPArchive');

    }
    public function _adminDashboardManageDPEdit($id){
        /* edit division personnel */
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getspecificdp',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        session(['admindpid'=>$data[0]['id']]);  
        session(['admindpemail'=>$data[0]['email']]);
        /* getofficeassignments */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'getofficeassignments',
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        /* get programs */
        $client2=new \GuzzleHttp\Client();
        $url2="http://localhost/CMSWebservice/";
        $response2=$client2->request('POST', $url2, [
            'form_params'=>[
                'tag'=>'getprograms',
            ]
        ]);
        $data2=json_decode($response2->getBody()->getContents(), true);
        return view('roles/admin/adminDashboardManageDPEdit',['admineditdp'=>$data,'adminnewoffice'=>$data1,'adminnewprogram'=>$data2]);
    }
    public function _adminInsertDivisionPersonnel(Request $request){
        $first_name=$request["new-DP-FirstName"];
        $middle_name=$request["new-DP-MiddleName"];
        $surname=$request["new-DP-Surname"];
        $email=$request["new-DP-Email"];
        $office=$request["new-DP-Office"];
        $program=$request["new-DP-Programs"];
        $contact_number=$request["new-DP-ContactNumber"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'insertdivisionpersonnel',
                'first_name'=>$first_name,
                'middle_name'=>$middle_name,
                'surname'=>$surname,
                'email'=>$email,
                'office'=>$office,
                'program'=>$program,
                'contact_number'=>$contact_number,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* insertdpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'insertdivisionpersonnelsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["count"]<=0){
            alert()->success('Successully', 'Inserted a Division Personnel');
            return redirect()->route('_adminDashboardManageDP');
        }
        else{
            alert()->warning('Error','Email already exists');
            return redirect()->route('_adminDashboardManageDPAdd');
        }
    }
    public function _adminDropDivisionPersonnel(Request $request){
        $id=$request["new-DP-DropID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'dropdivisionpersonnel',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* droppsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'dropdivisionpersonnelsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Dropped a Division Personnel');
        return redirect()->route('_adminDashboardManageDP');
    }
    public function _adminUpdateDivisionPersonnel(Request $request){
        $first_name=$request["new-DP-FirstName-edit"];
        $middle_name=$request["new-DP-MiddleName-edit"];
        $surname=$request["new-DP-Surname-edit"];
        $email=$request["new-DP-Email-edit"];
        $office=$request["new-DP-Office-edit"];
        $program=$request["new-DP-Programs-edit"];
        $contact_number=$request["new-DP-ContactNumber-edit"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'updatedivisionpersonnel',
                'first_name'=>$first_name,
                'middle_name'=>$middle_name,
                'surname'=>$surname,
                'email'=>$email,
                'office'=>$office,
                'program'=>$program,
                'contact_number'=>$contact_number,
                'id'=>session('admindpid'),
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* updatedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'updatedivisionpersonnelsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["success"]==1){
            alert()->success('Successully', 'Updated a Division Personnel');
            return redirect()->route('_adminDashboardManageDP');
        }
        else{
            alert()->warning('Error','Email already exists');
            return redirect()->route('_adminDashboardManageDPEdit',['id'=> session('admindpid')]);
        }
    }
    /* ---------- End of manage division personnel ---------- */
    /* ---------- Manage Principal ---------- */
    public function _adminDashboardManagePrincipal(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getprincipal',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'getprincipalschool',
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data[0]["status"]=='Active'){
            return view('roles/admin/adminDashboardManagePrincipal',['getprincipal'=>$data]);
        }
        else{
            alert()->warning('Error','No Data, add a principal first');
            return view('roles/admin/adminDashboardManagePrincipalAdd',['getprincipal1'=>$data1]);
        }
    }
    public function _adminDashboardPrincipalGetSchool(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getprincipalschool',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        return $data;
    }
    public function _adminInsertPrincipal(Request $request){
        $first_name=$request["new-Principal-FirstName"];
        $middle_name=$request["new-Principal-MiddleName"];
        $surname=$request["new-Principal-Surname"];
        $email=$request["new-Principal-Email"];
        $office=$request["new-Principal-Office"];
        $contact_number=$request["new-Principal-ContactNumber"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'insertprincipal',
                'first_name'=>$first_name,
                'middle_name'=>$middle_name,
                'surname'=>$surname,
                'email'=>$email,
                'office'=>$office,
                'contact_number'=>$contact_number,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* insertdpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'insertprincipalsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["count"]<=0){
            alert()->success('Successully', 'Inserted a Principal');
            return redirect()->route('_adminDashboardManagePrincipal');
        }
        else{
            alert()->warning('Error','Email already exists');
            return redirect()->route('_adminDashboardManagePrincipal');
        }
    }
    public function _adminUpdatePrincipal(Request $request){
        $first_name=$request["new-Principal-FirstNameEdit"];
        $middle_name=$request["new-Principal-MiddleNameEdit"];
        $surname=$request["new-Principal-SurnameEdit"];
        $email=$request["new-Principal-EmailEdit"];
        $office=$request["new-Principal-OfficeEdit"];
        $contact_number=$request["new-Principal-ContactNumberEdit"];
        $id=$request["new-Principal-IDEdit"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'updateprincipal',
                'first_name'=>$first_name,
                'middle_name'=>$middle_name,
                'surname'=>$surname,
                'email'=>$email,
                'office'=>$office,
                'contact_number'=>$contact_number,
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* updatedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'updateprincipalsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["success"]==1){
            alert()->success('Successully', 'Updated a Division Personnel');
            return redirect()->route('_adminDashboardManagePrincipal');
        }
        else{
            alert()->warning('Error','Email already exists');
            return redirect()->route('_adminDashboardManagePrincipal');
        }
    }
    public function _adminResetPrincipal(Request $request){
        $email=$request["new-Principal-ResetEmail"];
        $id=$request["new-Principal-ResetID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'resetprincipal',
                'email'=>$email,
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* resetdpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'resetprincipalsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Reset a Principal');
        return redirect()->route('_adminDashboardManagePrincipal');
    }
    public function _adminDropPrincipal(Request $request){
        $id=$request["new-Principal-DropID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'dropprincipal',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* droppsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'dropprincipalsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Dropped a Principal');
        return redirect()->route('_adminDashboardManagePrincipal');
    }
    public function _adminDashboardManagePrincipalArchive(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getdroppedprincipal',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["status"]=='Inactive'){  
            return view('roles/admin/adminDashboardManagePrincipalArchive',['getprincipalarchive'=>$data]);
        }
        else{
            alert()->warning('Error','No Data');
            return redirect()->route('_adminDashboardManagePrincipal');
        }
    }
    public function _adminDashboardManagePrincipalRestore(Request $request){
        $id=$request["new-Principal-ArchiveRestoreID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'restoreprincipal',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* restoredpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'restoreprincipalsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Restored a Principal');
        return redirect()->route('_adminDashboardManagePrincipalArchive');
    }
    public function _adminDashboardManagePrincipalDelete(Request $request){
        $compareName=$request["admin-Principal-ArchiveHiddenName"];
        $email=$request["new-Principal-ArchiveEmail"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'deleteprincipal',
                'email'=>$email,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* deletedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'deleteprincipalsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Deleted a Principal');
        return redirect()->route('_adminDashboardManagePrincipalArchive');
    }
    /* ---------- End of manage principal ---------- */
    /* ------------------------------ Miscellaneous ------------------------------ */
    /* Manage Office */
    public function _adminDashboardManageMiscOffice(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getmiscoffice',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["office_status"]=='Active'){
            return view('roles/admin/adminDashboardManageMiscOffice',['adminmiscoffice'=>$data]);
        }
        else{
            alert()->warning('Error','No Data, add a office first');
            return view('roles/admin/adminDashboardManageMiscOfficeAdd');
        }
    }
    public function _adminInsertOffice(Request $request){
        $office=$request["new-Office-OfficeName"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'insertoffice',
                'office'=>$office,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'insertofficesysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["count"]<=0){
            alert()->success('Successully', 'Inserted a Office');
            return redirect()->route('_adminDashboardManageMiscOffice');
        }
        else{
            alert()->warning('Error','Office already exists');
            return redirect()->route('_adminDashboardManageMiscOffice');
        }
    }
    public function _adminUpdateOffice(Request $request){
        $office=$request["new-Office-OfficeNameEdit"];
        $id=$request["new-Office-IDEdit"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'updateoffice',
                'office'=>$office,
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* updatedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'updateofficesysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["success"]==1){
            alert()->success('Successully', 'Updated an Office');
            return redirect()->route('_adminDashboardManageMiscOffice');
        }
        else{
            alert()->warning('Error','Office Name already exists');
            return redirect()->route('_adminDashboardManageMiscOffice');
        }
    }
    public function _adminDropOffice(Request $request){
        $id=$request["new-Office-DropID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'dropoffice',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* droppsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'dropofficesysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Dropped a Office');
        return redirect()->route('_adminDashboardManageMiscOffice');
    }
    public function _adminDashboardManageOfficeArchive(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getdroppedoffice',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["office_status"]=='Inactive'){  
            return view('roles/admin/adminDashboardManageMiscOfficeArchive',['getofficearchive'=>$data]);
        }
        else{
            alert()->warning('Error','No Data');
            return redirect()->route('_adminDashboardManageMiscOffice');
        }
    }
    public function _adminDashboardManageOfficeRestore(Request $request){
        $id=$request["new-Office-ArchiveRestoreID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'restoreoffice',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* restoredpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'restoreofficesysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Restored an Office');
        return redirect()->route('_adminDashboardManageOfficeArchive');
    }
    public function _adminDashboardManageOfficeDelete(Request $request){
        $compareName=$request["admin-Office-ArchiveHiddenName"];
        $office=$request["new-Office-ArchiveName"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'deleteoffice',
                'office'=>$office,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* deletedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'deleteofficesysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Deleted an Office');
        return redirect()->route('_adminDashboardManageOfficeArchive');
    }
    /* End of manage office */
    /* Manage program */
    public function _adminDashboardManageMiscProgram(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getmiscprogram',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["program_status"]=='Active'){
            return view('roles/admin/adminDashboardManageMiscProgram',['adminmiscprogram'=>$data]);
        }
        else{
            alert()->warning('Error','No Data, add a program first');
            return view('roles/admin/adminDashboardManageMiscProgramAdd');
        }
    }
    public function _adminInsertProgram(Request $request){
        $program=$request["new-Program-ProgramName"];
        $description=$request["new-Program-ProgramDescription"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'insertprogram',
                'program'=>$program,
                'description'=>$description,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'insertprogramsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["count"]<=0){
            $filesdirNewDir = public_path().'/filesdir/'.$program;
            mkdir($filesdirNewDir, 0777);
            alert()->success('Successully', 'Inserted a Program');
            return redirect()->route('_adminDashboardManageMiscProgram');
        }
        else{
            alert()->warning('Error','Program already exists');
            return redirect()->route('_adminDashboardManageMiscProgram');
        }
    }
    public function _adminUpdateProgram(Request $request){
        $program=$request["new-Program-ProgramNameEdit"];
        $description=$request["new-Program-ProgramDescriptionEdit"];
        $id=$request["new-Program-IDEdit"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'updateprogram',
                'program'=>$program,
                'description'=>$description,
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* updatedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'updateprogramsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        if($data["success"]==1){
            alert()->success('Successully', 'Updated an Program');
            return redirect()->route('_adminDashboardManageMiscProgram');
        }
        else{
            alert()->warning('Error','Program Name already exists');
            return redirect()->route('_adminDashboardManageMiscProgram');
        }
    }
    public function _adminDropProgram(Request $request){
        $id=$request["new-Program-DropID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'dropprogram',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* droppsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'dropprogramsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Dropped a Program');
        return redirect()->route('_adminDashboardManageMiscProgram');
    }
    public function _adminDashboardManageProgramArchive(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getdroppedprogram',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data[0]["program_status"]=='Inactive'){  
            return view('roles/admin/adminDashboardManageMiscProgramArchive',['getprogramarchive'=>$data]);
        }
        else{
            alert()->warning('Error','No Data');
            return redirect()->route('_adminDashboardManageMiscProgram');
        }
    }
    public function _adminDashboardManageProgramRestore(Request $request){
        $id=$request["new-Program-ArchiveRestoreID"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'restoreprogram',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* restoredpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'restoreprogramsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Restored an Program');
        return redirect()->route('_adminDashboardManageProgramArchive');
    }
    public function _adminDashboardManageProgramDelete(Request $request){
        $compareName=$request["admin-Program-ArchiveHiddenName"];
        $program=$request["new-Program-ArchiveName"];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'deleteprogram',
                'program'=>$program,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        /* deletedpsysreport */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'deleteprogramsysreport',
                'email'=>session('email'),
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        alert()->success('Successully', 'Deleted an Program');
        return redirect()->route('_adminDashboardManageProgramArchive');
    }
    /* End of manage program */
    /* ------------------------------ End of miscellaneous ------------------------------ */
}
?>