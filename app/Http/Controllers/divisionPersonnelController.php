<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use RealRashid\SweetAlert\Facades\Alert;

class divisionPersonnelController extends Controller{
        /* ---------- Manage SPC ---------- */
        public function _divisionPersonnelDashboardManageSPC(){
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'dpcgetspc',
                    'email'=>session('email')
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* getschool */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'getprincipalschool',
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            /* get programs */
            $client2=new \GuzzleHttp\Client();
            $url2="http://localhost/CMSWebservice/";
            $response2=$client2->request('POST', $url2, [
                'form_params'=>[
                    'tag'=>'getprograms',
                ]
            ]);
            $data2=json_decode($response2->getBody()->getContents(), true);
            /* get dpc program/s */
            $client3=new \GuzzleHttp\Client();
            $url3="http://localhost/CMSWebservice/";
            $response3=$client3->request('POST', $url3, [
                'form_params'=>[
                    'tag'=>'getprogramdpc',
                    'email'=>session('email'),
                ]
            ]);
            $data3=json_decode($response3->getBody()->getContents(), true);
            if($data[0]["status"]=='Active'){
                return view('roles/divisionpersonnel/divisionPersonnelDashboardManageSPC',['dpcgetspc'=>$data,'dpcprograms'=>$data3]);
            }
            else{
                return redirect()->route('_divisionPersonnelDashboardManageSPCAdd',['dpcschool'=>$data1,'dpcprogram'=>$data2]);
            }
        }
        public function _divisionPersonnelDashboardManageSPCAdd(){
            /* getschool */
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'getprincipalschool',
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* get programs */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'getprograms',
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            /* get dpc program/s */
            $client2=new \GuzzleHttp\Client();
            $url2="http://localhost/CMSWebservice/";
            $response2=$client2->request('POST', $url2, [
                'form_params'=>[
                    'tag'=>'getprogramdpc',
                    'email'=>session('email'),
                ]
            ]);
            $data2=json_decode($response2->getBody()->getContents(), true);
            return view('roles/divisionpersonnel/divisionPersonnelDashboardManageSPCAdd',['dpcschool'=>$data,'dpcprogram'=>$data1,'dpcprograms'=>$data2]);
        }
        public function _divisionPersonnelGetCompareProgramSPC(Request $request){
            $office=$request["newDPCSchool"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'getcompareprogramdpc',
                    'email'=>session('email'),
                    'office'=>$office
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            return $data;
        }
        public function _divisionPersonnelInsertSPC(Request $request){
            $first_name=$request["new-DPC-FirstName"];
            $middle_name=$request["new-DPC-MiddleName"];
            $surname=$request["new-DPC-Surname"];
            $email=$request["new-DPC-Email"];
            $school=$request["new-DPC-School"];
            $program=$request["new-DPC-Program"];
            $contact_number=$request["new-DPC-ContactNumber"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'insertspc',
                    'first_name'=>$first_name,
                    'middle_name'=>$middle_name,
                    'surname'=>$surname,
                    'email'=>$email,
                    'office'=>$school,
                    'program'=>$program,
                    'contact_number'=>$contact_number,
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* insertdpsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'insertspcsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            if($data["count"]<=0){
                alert()->success('Successully', 'Inserted a School Personnel');
                return redirect()->route('_divisionPersonnelDashboardManageSPC');
            }
            else{
                alert()->warning('Error','Email already exists');
                return redirect()->route('_divisionPersonnelDashboardManageSPCAdd');
            }
        }
        public function _divisionPersonnelDashboardManageSPCEdit($id){
        /* edit division personnel */
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'dpcgetspecificspc',
                'id'=>$id,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        session(['dpcidspc'=>$data[0]['id']]);  
        session(['dpcemailspc'=>$data[0]['email']]);
        /* getschool */
        $client1=new \GuzzleHttp\Client();
        $url1="http://localhost/CMSWebservice/";
        $response1=$client1->request('POST', $url1, [
            'form_params'=>[
                'tag'=>'getprincipalschool',
            ]
        ]);
        $data1=json_decode($response1->getBody()->getContents(), true);
        /* get programs */
        $client2=new \GuzzleHttp\Client();
        $url2="http://localhost/CMSWebservice/";
        $response2=$client2->request('POST', $url2, [
            'form_params'=>[
                'tag'=>'getprograms',
            ]
        ]);
        $data2=json_decode($response2->getBody()->getContents(), true);
        /* get dpc program/s */
        $client3=new \GuzzleHttp\Client();
        $url3="http://localhost/CMSWebservice/";
        $response3=$client3->request('POST', $url3, [
            'form_params'=>[
                'tag'=>'getprogramdpc',
                'email'=>session('email'),
            ]
        ]);
        $data3=json_decode($response3->getBody()->getContents(), true);
        return view('roles/divisionpersonnel/divisionPersonnelDashboardManageSPCEdit',['dpceditspc'=>$data,'dpcnewschool'=>$data1,'dpcnewprogram'=>$data2,'dpcprograms'=>$data3]);
        }
        public function _divisionPersonnelUpdateSPC(Request $request){
            $first_name=$request["new-DPC-FirstNameEdit"];
            $middle_name=$request["new-DPC-MiddleNameEdit"];
            $surname=$request["new-DPC-SurnameEdit"];
            $email=$request["new-DPC-EmailEdit"];
            $school=$request["new-DPC-SchoolEdit"];
            $program=$request["new-DPC-ProgramsEdit"];
            $contact_number=$request["new-DPC-ContactNumberEdit"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'updatespc',
                    'first_name'=>$first_name,
                    'middle_name'=>$middle_name,
                    'surname'=>$surname,
                    'email'=>$email,
                    'office'=>$school,
                    'program'=>$program,
                    'contact_number'=>$contact_number,
                    'id'=>session('dpcidspc'),
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* updatedpsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'updatespcsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            if($data["success"]==1){
                alert()->success('Successully', 'Updated a School Personnel');
                return redirect()->route('_divisionPersonnelDashboardManageSPC');
            }
            else{
                alert()->warning('Error','Email already exists');
                return redirect()->route('_divisionPersonnelDashboardManageSPCEdit',['id'=> session('dpcidspc')]);
            }
        }
        public function _divisionPersonnelResetSPC(Request $request){
            $email=$request["new-DPC-ResetEmail"];
            $id=$request["new-DPC-ResetID"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'resetspc',
                    'email'=>$email,
                    'id'=>$id,
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* resetspcsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'resetspcsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            alert()->success('Successully', 'Reset a School Personnel');
            return redirect()->route('_divisionPersonnelDashboardManageSPC');
        }
        public function _divisionPersonnelDropSPC(Request $request){
            $id=$request["new-DPC-DropID"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'dropspc',
                    'id'=>$id,
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* dropspcsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'dropspcsysreport',
                    'email'=>session('email')
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            alert()->success('Successully', 'Dropped a School Personnel');
            return redirect()->route('_divisionPersonnelDashboardManageSPC');
        }
        public function _divisionPersonnelDashboardManageSPCArchive(){
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'getdroppedspc',
                    'email'=>session('email')
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* get dpc program/s */
            $client2=new \GuzzleHttp\Client();
            $url2="http://localhost/CMSWebservice/";
            $response2=$client2->request('POST', $url2, [
                'form_params'=>[
                    'tag'=>'getprogramdpc',
                    'email'=>session('email')
                ]
            ]);
            $data2=json_decode($response2->getBody()->getContents(), true);
            if($data[0]["status"]=='Inactive'){  
                return view('roles/divisionpersonnel/divisionPersonnelDashboardManageSPCArchive',['getspcarchive'=>$data,'dpcprograms'=>$data2]);
            }
            else{
                alert()->warning('Error','No Data');
                return redirect()->route('_divisionPersonnelDashboardManageSPC');
            }
        }
        public function _divisionPersonnelDeleteSPC(Request $request){
            $compareEmail=$request["admin-DPC-ArchiveHiddenEmail"];
            $email=$request["new-DPC-ArchiveEmail"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'deletespc',
                    'email'=>$email,
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* deletedpsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'deletespcsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            alert()->success('Successully', 'Deleted a School Personnel');
            return redirect()->route('_divisionPersonnelDashboardManageSPCArchive');
    
        }
        public function _divisionPersonnelRestoreSPC(Request $request){
            $id=$request["new-DPC-ArchiveRestoreID"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'restorespc',
                    'id'=>$id,
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* restoredpsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'restorespcsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            alert()->success('Successully', 'Restored a School Personnel');
            return redirect()->route('_divisionPersonnelDashboardManageSPCArchive');
    
        }
        public function _divisionPersonnelProgramReports($program){
            /* get dpc program/s */
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'getprogramdpc',
                    'email'=>session('email'),
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            session(['dcpprogramcompose'=>$program]);  
            return view('roles/divisionpersonnel/divisionPersonnelDashboardManageReports',['dpcprograms'=>$data]);
        }
        public function _divisionPersonnelProgramReportsCompose($program){
            date_default_timezone_set('Asia/Manila');
            /* get dpc program/s */
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'getprogramdpc',
                    'email'=>session('email'),
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            return view('roles/divisionpersonnel/divisionPersonnelDashboardManageReportsCompose',['dpcprograms'=>$data]);
        }
        public function _divisionPersonnelCompose(Request $request){
            /* Hidden Program */
            $hiddenProgram=$request['new-fileUploadProgram'];
            /* Title */
            $title=$request['new-DPCPost-Title'];
            /* Description */
            $description=$request['new-DPCPost-Description'];
            /* Attachment -Optional */
                /* Hidden DateTime ->File Name */
                $hiddenDateTime=$request['new-fileUploadDateTime'];
                /* Directory */ 
                $uploadDir=public_path().'/filesdir/'.$hiddenProgram;
                /* File 1 */
                $file1Ext=pathinfo($_FILES['new-fileAttachment1']['name'], PATHINFO_EXTENSION);
                $file1=$_FILES['new-fileAttachment1']['tmp_name'];//ex: C:\xampp\tmp\php9B9F.tmp
                if(!empty($file1)){
                    $uploadFileNameExt='DPC-'.$hiddenDateTime.'.'.$file1Ext;//request
                    $uploadFile1=$uploadDir.'/'.$uploadFileNameExt;
                    move_uploaded_file($file1, $uploadFile1);
                }
                else{
                    $uploadFileNameExt='Empty';
                }
            /* Hidden Deadline */
            $hiddenDeadline=$request['new-Deadline'];
            /* Hidden Mode of Reply */
            $hiddenModeofReply=$request['new-fileUploadModeofReply'];
            /* Hidden File Quantity */
            $hiddenFileQuantity=$request['new-fileUploadQuantity'];
            /* Web Service */
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'dpccreatepostreport',
                    'dpc_name'=>session('first_name').' '.session('surname'),
                    'modeofreply'=>$hiddenModeofReply,
                    'filequantity'=>$hiddenFileQuantity,
                    'title'=>$title,
                    'attachment'=>$uploadFileNameExt,
                    'description'=>$description,
                    'date_deadline'=>$hiddenDeadline,
                    'program'=>session('dcpprogramcompose')

                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* createpostreportsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'createpostreportsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            alert()->success('Successully', 'Composed a Report');
            return redirect()->route('_divisionPersonnelProgramReports',['program'=>session('dcpprogramcompose')]);
        }
        public function _divisionPersonnelComposeEdit($program,$id){
            date_default_timezone_set('Asia/Manila');
            /* get dpc program/s */
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'getprogramdpc',
                    'email'=>session('email'),
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* get specific report */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'getspecificreport',
                    'id'=>$id
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            return view('roles/divisionpersonnel/divisionPersonnelDashboardManageReportsComposeEdit',['dpcprograms'=>$data,'dpcreport'=>$data1[0]]);
        }
        public function _divisionPersonnelEditReport(Request $request){
            /* Hidden Program */
            $hiddenProgram=$request['new-fileUploadProgramEdit'];
            /* Title */
            $title=$request['new-DPCPost-TitleEdit'];
            /* Description */
            $description=$request['new-DPCPost-DescriptionEdit'];
            /* Hidden Deadline */
            $hiddenDeadline=$request['new-DeadlineEdit'];
            /* Hidden Mode of Reply */
            $hiddenModeofReply=$request['new-fileUploadModeofReplyEdit'];
            /* Hidden File Quantity */
            $hiddenFileQuantity=$request['new-fileUploadQuantityEdit'];
            /* Hidden Datetime */
            $hiddenDateTime=$request['new-ReportDateUpdated'];
            /* Hidden ID */
            $id=$request['new-ReportIDEdit'];
            /* Web Service */
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'editpostreport',
                    'dpc_name'=>session('first_name').' '.session('surname'),
                    'modeofreply'=>$hiddenModeofReply,
                    'filequantity'=>$hiddenFileQuantity,
                    'title'=>$title,
                    'description'=>$description,
                    'date_deadline'=>$hiddenDeadline,
                    'date_updated'=>$hiddenDateTime,
                    'id'=>$id

                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* createpostreportsysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'editactivereportsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            alert()->success('Successully', 'Updated a Report');
            return redirect()->route('_divisionPersonnelProgramReports',['program'=>session('dcpprogramcompose')]);
        }
        //Get Active Report Ajax
        public function _divisionPersonnelGetActiveReport(){
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'getactivereport',
                    'program'=>session('dcpprogramcompose')
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            return $data;
        }
        //Get Compliance ex: 0/32 (the 32)
        public function _divisionPersonnelShowCountSchoolsCompliance(Request $request){
            $level=$request['newDPCLevel'];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'showcountschoolscompliance',
                    'level'=>$level
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            return $data;
        }
        public function _divisionPersonnelShowHideReport(Request $request){
            $id=$request["new-DPC-ShowHideID"];
            $level=$request["new-DPC-ShowHideLevel"];
            $statusShownOrHidden=$request["new-DPC-ShowHideStatusShownOrHidden"];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'dpcshowhidereport',
                    'id'=>$id,
                    'level'=>$level
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            if($statusShownOrHidden=='Hidden'){
                $Shown='Shown an Active Report';
                alert()->success('Successully', $Shown);
                /* showhidesysreport */
                $client1=new \GuzzleHttp\Client();
                $url1="http://localhost/CMSWebservice/";
                $response1=$client1->request('POST', $url1, [
                    'form_params'=>[
                        'tag'=>'showhidesysreport',
                        'email'=>session('email'),
                        'actiontype'=>$Shown
                    ]
                ]);
                $data1=json_decode($response1->getBody()->getContents(), true);
            }
            else if($statusShownOrHidden=='Shown'){
                $Hidden='Hidden an Active Report';
                alert()->success('Successully', $Hidden);
                /* showhidesysreport */
                $client1=new \GuzzleHttp\Client();
                $url1="http://localhost/CMSWebservice/";
                $response1=$client1->request('POST', $url1, [
                    'form_params'=>[
                        'tag'=>'showhidesysreport',
                        'email'=>session('email'),
                        'actiontype'=>$Hidden
                    ]
                ]);
                $data1=json_decode($response1->getBody()->getContents(), true);
            }
            return redirect()->route('_divisionPersonnelProgramReports',['program'=>session('dcpprogramcompose')]);
        }
        public function _dpcDeleteActiveReport(Request $request){
            $id=$request['new-DPC-DeleteID'];
            $client=new \GuzzleHttp\Client();
            $url="http://localhost/CMSWebservice/";
            $response=$client->request('POST', $url, [
                'form_params'=>[
                    'tag'=>'deleteactivereport',
                    'id'=>$id
                ]
            ]);
            $data=json_decode($response->getBody()->getContents(), true);
            /* showhidesysreport */
            $client1=new \GuzzleHttp\Client();
            $url1="http://localhost/CMSWebservice/";
            $response1=$client1->request('POST', $url1, [
                'form_params'=>[
                    'tag'=>'deleteactivereportsysreport',
                    'email'=>session('email'),
                ]
            ]);
            $data1=json_decode($response1->getBody()->getContents(), true);
            return redirect()->route('_divisionPersonnelProgramReports',['program'=>session('dcpprogramcompose')]);
        }
/* end of class */
}
?>