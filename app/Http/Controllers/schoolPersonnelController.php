<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use RealRashid\SweetAlert\Facades\Alert;

class schoolPersonnelController extends Controller{

    public function _schoolPersonnelHome(){
        /* get spc program/s */
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'spcgetprograms',
                'email'=>session('email'),
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        return view('roles/schoolpersonnel/schoolPersonnelHome', ["programs"=>$data]);
    }
    public function _schoolPersonnelGetSpecificProgram($program){
        /* get spc program/s */
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'spcgetprograms',
                'email'=>session('email'),
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        session(['spcprogram'=>$program]);
        return view('roles/schoolpersonnel/schoolPersonnelProgram', ["programs"=>$data]);
    }
    /* Ajax for getting specific program */
    public function _schoolPersonnelProgramGetFilteredReport(){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'spcprogramgetfilteredreport',
                'email'=>session('email'),
                'program'=>session('spcprogram')
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        return $data;
    }
    // /* Ajax to Controller to download attachment */
    // public function _schoolPersonnelProgramDownloadAttachment(Request $request){
    //     $attachment=$request['spc-AttachmentName'];
    //     if (file_exists(public_path().'/filesdir/'.session('spcprogram').$attachment)) {
    //         header('Content-Description: File Transfer');
    //         header('Content-Type: application/octet-stream');
    //         header('Content-Disposition: attachment; filename="'.basename($attachment).'"');
    //         header('Expires: 0');
    //         header('Cache-Control: must-revalidate');
    //         header('Pragma: public');
    //         header('Content-Length: ' . filesize($attachment));
    //         readfile($attachment);
    //         exit;
    //     }
    //     return $public_path().'/filesdir/'.session('spcprogram').$attachment;
    // }
}

?>