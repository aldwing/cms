<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use RealRashid\SweetAlert\Facades\Alert;

class loginController extends Controller{

    public function _login(){
        return view('login');
    }
    public function _loginAdminConfirm(Request $request){
        $email=$request['form-username'];
        $password=$request['form-password'];
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getlogin',
                'email'=>$email,
                'password'=>$password,
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        if($data['success']==1){
            if($data[0]['role']=="Admin"){
                session(['email'=>$data['email']]);
                session(['first_name'=>$data[0]['first_name']]);
                session(['surname'=>$data[0]['surname']]);
                return redirect()->route('_adminDashboardManageDP');
            }
            else if($data[0]['role']=="Division Personnel"){
                session(['email'=>$data['email']]);
                session(['first_name'=>$data[0]['first_name']]);
                session(['surname'=>$data[0]['surname']]);
                return redirect()->route('_divisionPersonnelDashboardManageSPC');
            }
            else if($data[0]['role']=="Principal"){
                session(['email'=>$data['email']]);
                session(['first_name'=>$data[0]['first_name']]);
                session(['surname'=>$data[0]['surname']]);
                return redirect()->route('_principalHome');
            }
            else if($data[0]['role']=="School Personnel"){
                session(['email'=>$data['email']]);
                session(['first_name'=>$data[0]['first_name']]);
                session(['surname'=>$data[0]['surname']]);
                return redirect()->route('_schoolPersonnelHome');
            }
        }else{
            alert()->warning('Error','Email/Password is invalid');
            return redirect()->route('_login');
        }
    }
//end of class
}
?>