<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use RealRashid\SweetAlert\Facades\Alert;

class logoutController extends Controller{

    public function _logout($alert){
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getlogout',
                'email'=>session('email'),
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        alert()->success('Logout', 'Successfully');
        return redirect()->route('_login');
    }   
//end of class
}
?>