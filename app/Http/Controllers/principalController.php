<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use RealRashid\SweetAlert\Facades\Alert;

class principalController extends Controller{

    public function _principalHome(){
        /* get dpc program/s */
        $client=new \GuzzleHttp\Client();
        $url="http://localhost/CMSWebservice/";
        $response=$client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'principalgetprograms',
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        return view('roles/principal/principalHome', ["programs"=>$data]);
    }
}

?>